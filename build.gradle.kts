plugins {
  id("org.springframework.boot") version "3.1.2"
  id("io.spring.dependency-management") version "1.1.2" // Note all detachedConfigurations101
  java
  jacoco
}

group = "com.itcareer"
version = "0.1.0-SNAPSHOT"

repositories {
  mavenCentral()
}

dependencies {

  // SpringBoot
  implementation("org.springframework.boot:spring-boot-starter-data-jdbc")
  implementation("org.springframework.boot:spring-boot-starter-security")
  implementation("org.springframework.boot:spring-boot-starter-thymeleaf")
  implementation("org.springframework.boot:spring-boot-starter-web")

  // Application
  implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-xml") {
    because("Sitemap generation")
  }
  implementation("org.apache.commons:commons-lang3:3.12.0")
  implementation("org.commonmark:commonmark:0.21.0") {
    because("Parsing persisted event description stored in Markdown syntax (EventController)")
  }
  implementation("com.googlecode.owasp-java-html-sanitizer:owasp-java-html-sanitizer:20220608.1") {
    because("Sanitizing input in SearchTermSanitizer")
  }
  implementation("org.hibernate:hibernate-validator:7.0.4.Final")
  implementation("org.jsoup:jsoup:1.16.1")
  implementation("jakarta.validation:jakarta.validation-api:3.0.2")
  implementation("jakarta.servlet:jakarta.servlet-api:6.0.0")
  runtimeOnly("org.postgresql:postgresql:42.5.4") {
    because("PostgreSQL Database driver")
  }

  // Testing
  testImplementation("com.github.tomakehurst:wiremock-jre8-standalone:2.35.0")
  testImplementation("org.springframework.boot:spring-boot-starter-test")
  testImplementation("org.junit.jupiter:junit-jupiter:5.9.3")
  testRuntimeOnly("org.junit.platform:junit-platform-launcher")
  testImplementation("org.springframework.security:spring-security-test")
}


java {
  toolchain {
    languageVersion = JavaLanguageVersion.of(17)
  }
}

tasks.test<Test> { // Show code completion with task type and without
  useJUnitPlatform()
}

tasks.check {
  dependsOn(tasks.named("jacocoTestCoverageVerification"))
}

tasks.jacocoTestCoverageVerification {
  violationRules {
    rule {
      limit {
        counter = "LINE"
        value = "COVEREDRATIO"
        minimum = BigDecimal("0.96")
      }
    }
  }
}
