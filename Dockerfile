FROM eclipse-temurin:17.0.7_7-jdk-alpine
COPY . /app
WORKDIR /app
RUN /bin/sh ./gradlew build -x check

EXPOSE 5000
CMD ["java", "-jar", "build/libs/itshelf-app-0.1.0-SNAPSHOT.jar"]