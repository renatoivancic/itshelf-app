# ITShelf #

This README contains all the necessary steps to get the ITShelf application up and running.

## Overview ##

* This application enables IT people/students to browse or search from different vendors by categories, tags or keywords through the collections of 
    - Courses (Udemy, Coursera, Microsoft)
    - Events (Custom entries)
	- ~~Books,~~
	- ~~Streams (Youtube, Twitter, Twitch channels)~~
	- ~~Certifications~~
	
    In this stage it is fetching information from public APIs and aggregates the information and 
  displays it to the user.

## Setup ###

* Frameworks and technologies:
    - Springboot: 3.1.2
    - Gradle (Kotlin DSL): 8.2.1
    - Java: 17
    - Thymeleaf
    - PostgreSQL: 11

### Configuration

*application.properties* is being used for the configurations.

One needs to specify Udemy API credentials in order to fetch the information for their API.

- vendor.udemy.api.id=XXX
- vendor.udemy.api.secret=XXX

### Credentials

Credentials are stored in a list in the property below. They represent 
`org.springframework.security.core.userdetails.UserDetails`  

- com.itshelf.adminusers

Admin interface is available through `/admin` path.

| Username | Password |
|----------|----------|
| admin    | test     |
| user     | test     |

### Database

By default PostgreSQL database is being used (See Docker section).

### Tests

Check task will run tests and after it jacoco code coverage.

```
./gradlew check
```

## Build

### Frontent

#### CSS is combined

purify-css npm package is used for combining css files https://www.npmjs.com/package/purify-css
Needs to be done before deployment (TODO implement CSS combination in build pipeline)

Script

```
purifycss static/css/theme.css static/css/tiny-slider.min.css static/css/itshelf.css templates/admin/adminEvent.html templates/event.html templates/events.html templates/fragments/footer/footer.html templates/fragments/header/header.html templates/fragments/header/landing-header.html templates/fragments/list/courseList.html templates/fragments/eventList.html templates/landing.html templates/search.html static/js/vendor/tiny-slider.min.js static/js/vendor/popper.min.js  --min --info --out static/css/itshelf-style.css
```

#### JS is combined

merger-js an npm package is used to combine js https://www.npmjs.com/package/merger-js
Needs to be done before deployment (TODO implement JS combination in build pipeline)

1) Install merger

```npm i merger-js -g```

2) Init merger

```merger init```

This will create a header file (containing all js files that have to be combined), and a configuration file.

3) Build JS with merger

```merger build```

### Backend

Build is being run after every commit, it is running tests and codecoverage (96% lines).

After each push the bitbucket build will check the application for code coverage.

## Docker

You can spin up application locally with `docker-compose up`. Definition of the environment is in docker-compose.yml
file in root directory. Application will be available on [localhost:80](http://localhost:80).

You can also start only `itshelf-db` service with `docker-compose up itshelf-db` command. Then you can start application from IDE with `local` profile.

To rebuild and use new app image:

Shorthand:

`docker-compose up -d --build` to rebuild all images and start again

Or only application service:

`docker stop itshelf-app-itshelf-app-1`

`docker rm itshelf-app-itshelf-app-1`

`docker rmi itshelf-app-itshelf-app`

## Contribution guidelines ###

* Read [Playbook](https://rivancic.atlassian.net/wiki/spaces/IC/pages/38895772/Playbook) for details 
* Code coverage 96 %

## Who do I talk to? ###

* @Renato Ivancic

## TODOs ##

- Clean branches / repository (local)
- Check Confluence / Documentation / README file
- Check that the app is functional even if the external resource providers are unavailable.
- Check once again all the JS, frontend framework
- Refactor readme file, leave inside only the necessary information for running this project locally.
- Prepare for the SpringBoot native build
- Remove BitBucket / AWS pipeline
- Make Coursera partners list dynamic. Otherwise for the new ones Partner name won't be able to resolve:
  .getPartnerDisplayName() | Partner name is missing for following partner id: 705
- Upgrade docker image, check vulnerabilities: https://bitbucket.org/renatoivancic/itshelf-app/addon/snyk-bbc-app/snyk-repo-page

### H2

- Make project runnable without any dependency of the concrete database.
- Create local-h2 profile that loads application with the H2 database.