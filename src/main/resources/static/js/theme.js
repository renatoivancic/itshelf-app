/**
 * Cartzilla | Bootstrap E-Commerce Template
 * Theme core scripts
 *
 * @author Createx Studio
 * @version 1.1
 */

(function ($) {
    'use strict';

    const theme = {

        /**
         * Theme's components/functions list
         * Comment out or delete the unnecessary component.
         * Some component have dependencies (plugins).
         * Do not forget to remove component plugin script from src/vendor/js
         */
        filterByFree: false,
        filterByType: {
            conference: true, onlineConference: true, webinar: true,
            allDisabled() {
                return this.conference === false && this.onlineConference === false && this.webinar === false;
            }
        },
        loggingEnabled: false,
        init: () => {
            theme.carousel();
            theme.searchSelector();
            theme.pagination();
            theme.filterFree();
            theme.filterType();
            theme.adminEventDetail();
            theme.adminEventList();
        },
        adminEventDetail: () => {
            $(document).ready(function() {

                // have to check that the element exists otherwise it is broken on main pages
                if($("#event-description") != null && $("#event-description").length) {
                    let matches = $("#event-description")[0].value.match(/\S+/g);
                    let words = 0;
                    if(matches != null) {
                        words = $("#event-description")[0].value.match(/\S+/g).length;
                    }
                    $('#event-description-count').text('Words: ' + words);
                    if(words < 150) {
                        $('#event-description-count').addClass('badge-primary');
                        $('#event-description-count').removeClass('badge-success');
                    } else {
                        $('#event-description-count').removeClass('badge-primary');
                        $('#event-description-count').addClass('badge-success');
                    }
                }

                $("#event-description").on('keyup', function() {
                    let wordsNumber = this.value.match(/\S+/g).length;
                    $('#event-description-count').text('Words: ' + wordsNumber);
                    if(wordsNumber < 150) {
                        $('#event-description-count').addClass('badge-primary');
                        $('#event-description-count').removeClass('badge-success');
                    } else {
                        $('#event-description-count').removeClass('badge-primary');
                        $('#event-description-count').addClass('badge-success');
                    }
                });

                // Check if image can be loaded on event detail page
                if($("#event-image-url").length) {
                    let imageUrl = $("#event-image-url")[0].value;
                    console.log("Image URL: " + imageUrl)
                    $("<img/>")
                        .on('load', function () {
                            console.log("image loaded correctly");
                        })
                        .on('error', function () {
                            console.log("error loading image");
                            $($("#event-image")[0]).attr("src", "/img/other/stop-sign.png");
                        })
                        .attr("src", imageUrl)
                    ;
                }

                // Enable or disable To Date
                if($("#isDateTimeToFlag").length) {
                    if($("#isDateTimeToFlag").prop("checked") == false) {
                        // disable To Date
                        $("date-time-input2").prop( "disabled", true);
                        $("timezone-offset2").prop( "disabled", true);
                    } else {
                        // enable To Date
                        $("date-time-input2").prop( "disabled", false);
                        $("timezone-offset2").prop( "disabled", false);
                    }
                }
            });

        },
        adminEventList: () => {
            $(document).ready(function() {
                let eventsWithBrokenImages = 0;

                // Check if image can be loaded on event list page
                if($(".image-url").length) {
                    $(".image-url").each(function () {
                        let imageUrlElement = $( this );
                        let imageUrl = imageUrlElement.text();
                        $("<img/>")
                            .on('load', function () {
                                $(imageUrlElement.siblings('.czi-close')[0]).addClass('d-none');
                            })
                            .on('error', function () {

                                // only count all broken images for the upcoming events
                                if (imageUrlElement.attr("upcoming") === "true") {
                                    eventsWithBrokenImages += 1;
                                    $('#events-broken-images').text(eventsWithBrokenImages);
                                }
                                $(imageUrlElement.siblings('.czi-close')[0]).addClass('d-inline');
                            })
                            .attr("src", imageUrl);
                    });

                    // initialize datatable
                    if($('.datatable').length) {
                        $('.datatable').DataTable();
                    }
                }
            });

        },
        filterType: () => {
            $(document).on('click', 'body .dropdown-menu', function (e) {
                e.stopPropagation();
            });

            let applyFilterTrigger = function (element, e, variable) {
                variable(!!element.checked);
                if (theme.filterByType.allDisabled()) {
                    variable(true);
                    element.checked = true;
                    e.stopPropagation();
                    return false;
                }
                theme.pagination();
            }
            $('#filter-type-conference').change(function (e) {
                applyFilterTrigger(this, e, value => {
                    theme.filterByType.conference = value
                });
            });
            $('#filter-type-webinar').change(function (e) {
                applyFilterTrigger(this, e, value => {
                    theme.filterByType.webinar = value
                });
            });
            $('#filter-type-online-conference').change(function (e) {
                applyFilterTrigger(this, e, value => {
                    theme.filterByType.onlineConference = value
                });
            });
        },
        filterFree: () => {
            let filterButton = $('#filter-free').first();

            filterButton.click(function () {
                theme.filterByFree = !theme.filterByFree;
                if (theme.filterByFree) {
                    filterButton.removeClass('btn-outline-info');
                    filterButton.addClass('btn-info');
                    theme.pagination();
                } else {
                    filterButton.removeClass('btn-info');
                    filterButton.addClass('btn-outline-info');
                    theme.pagination();
                }
            });
        },

        pagination: () => {
            let itemsPerPage = 12;

            // populate cards with attribute values and filter them based on that.
            let items = [];

            let filter = function (element) {

                let freeFilter = function () {
                    let isEventFree = element.find(".event-data").first().data("eventFree");
                    return !!isEventFree;
                }

                let typeFilter = function () {
                    let test = false;
                    let eventType = element.find(".event-data").first().data("eventType");
                    if (theme.filterByType.conference === true && eventType === "Conference") {
                        test = true;
                    }
                    if (theme.filterByType.webinar === true && eventType === "Webinar") {
                        test = true;
                    }
                    if (theme.filterByType.onlineConference === true && eventType === "Online Conference") {
                        test = true;
                    }
                    return test;
                }

                let filters = [];
                if (theme.filterByFree) {
                    filters.push(freeFilter)
                }
                filters.push(typeFilter);

                return filters.every(oneFilter => oneFilter());
            }

            // take filter type into consideration
            $('.card.product-card').each(function (index, linkElement) {
                if (filter($(linkElement))) {
                    items.push(linkElement);
                }
            });
            let numberOfItems = items.length;
            let numberOfPages = Math.ceil(numberOfItems / itemsPerPage);
            let currentPage = 1;
            let pageLink = $('.page-link.page-link-static').first();
            let firstArrow = $(".pagination:first");
            let lastArrow = $(".pagination:last");

            const showItems = function (pageIndex) {
                currentPage = pageIndex;
                if (theme.loggingEnabled) {
                    console.log("Showing elements of page: " + pageIndex);
                }
                let indexOfItems = 0;


                // First hide all elements
                $('.card.product-card').each(function (index, linkElement) {
                    $(linkElement).addClass(["d-none"]);
                });

                // Show only filtered an paginated elements
                items.forEach(function (linkElement) {
                    if (((pageIndex - 1) * itemsPerPage) < (indexOfItems + 1)
                        && (indexOfItems + 1) <= (pageIndex * itemsPerPage)

                    ) {
                        if (filter($(linkElement))) {
                            $(linkElement).removeClass(["d-none"]);
                            indexOfItems++;
                        } else {
                            $(linkElement).addClass(["d-none"]);
                        }
                    } else {
                        $(linkElement).addClass(["d-none"]);
                        indexOfItems++;
                    }
                });

                // Select current page in pagination
                $('.page-item.d-sm-block').each(function (index, linkElement) {
                    if (pageIndex === (index + 1)) {
                        $(linkElement).addClass('active');
                    } else {
                        $(linkElement).removeClass('active');
                    }
                });

                // Show number of sites
                pageLink.text(pageIndex + " / " + numberOfPages);
                if (pageIndex <= 1) {
                    firstArrow.addClass("invisible");
                } else {
                    firstArrow.removeClass("invisible");
                }
                if (pageIndex === numberOfPages) {
                    lastArrow.addClass("invisible");
                } else {
                    lastArrow.removeClass("invisible");
                }
            };

            showItems(1);

            // Show or remove pagination
            if (numberOfPages > 1) {
                let navigation = $('.navigation').first();
                navigation.removeClass("d-none");
                navigation.addClass("d-flex");
            }

            let pageLinkNumbers = $('.page-item.d-none.d-sm-block').first();

            // Clear any previous pagination
            $(".page-item.d-sm-block:not(:first)").remove();

            let pageLinkNumber = pageLinkNumbers.clone(true, false);
            pageLinkNumber.removeClass('active')
            let i;
            for (i = 0; i < numberOfPages; i++) {
                if (i === 0) {

                    // Select first page
                    // pageLinkNumbers.removeClass('d-none');
                    pageLinkNumbers.addClass("active");
                    pageLinkNumbers.click(function (linkElement) {
                        showItems(parseInt(linkElement.target.text));
                    });
                } else {

                    // Generate other pages
                    pageLinkNumber = pageLinkNumber.clone(true, false);
                    pageLinkNumber.insertAfter(".page-item.d-sm-block:last");
                    pageLinkNumber.children().text(i + 1);

                    // Do not stack events. As otherwise new pagination elements will have multiple
                    // events bound to it and will call rendering of items multiple times
                    pageLinkNumber.off();
                    pageLinkNumber.click(function (linkElement) {
                        showItems(parseInt(linkElement.target.text));
                    });
                }
            }

            // Make arrows actionable
            firstArrow.click(function () {
                showItems(currentPage - 1);
            });
            lastArrow.click(function () {
                showItems(currentPage + 1);
            });

            if (theme.loggingEnabled) {
                console.log("There are : " + numberOfItems + " items, Number of pages: " + numberOfPages);
            }
        },


        /**
         * Content carousel with extensive options to control behaviour and appearance
         * @memberof theme
         * @method carousel
         * @requires https://github.com/ganlanyuan/tiny-slider
         */
        carousel: () => {

            // forEach function
            let forEach = function (array, callback, scope) {
                for (let i = 0; i < array.length; i++) {
                    callback.call(scope, i, array[i]); // passes back stuff we need
                }
            };

            // Carousel initialisation
            let carousels = document.querySelectorAll('.cz-carousel .cz-carousel-inner');
            forEach(carousels, function (index, value) {
                let defaults = {
                    container: value,
                    controlsText: ['<i class="czi-arrow-left"></i>', '<i class="czi-arrow-right"></i>'],
                    navPosition: 'bottom',
                    mouseDrag: true,
                    speed: 500,
                    autoplayHoverPause: true,
                    autoplayButtonOutput: false
                };
                let userOptions;
                if (value.dataset.carouselOptions != undefined) userOptions = JSON.parse(value.dataset.carouselOptions);
                let options = {...defaults, ...userOptions};
                tns(options);
            });
        },

        /**
         * Map course / event selector in the search input
         *
         * If courses is selected one has to be navigated to courses page
         * If events is selected one has to be navigated to the events page
         */
        searchSelector: () => {
            $("#searchType").change(function () {
                var selectedSearchType = $(this).children("option:selected").val();
                $('#searchForm').attr('action', selectedSearchType);
                $("#searchInput").focus();
            });
        }
    }

    /**
     * Init theme core
     */

    theme.init();

})(jQuery);
