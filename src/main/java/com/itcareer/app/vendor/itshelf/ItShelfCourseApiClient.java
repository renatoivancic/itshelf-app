package com.itcareer.app.vendor.itshelf;

import com.itcareer.app.domain.Course;
import com.itcareer.app.domain.Vendor;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

/**
 * Concrete client for requesting ItShelf courses information.
 * <p>
 * Responses are mocked and hardcoded in the class itself.
 */
@Service
public class ItShelfCourseApiClient {

    private static final String AUTHOR = "Renato Ivancic";
    public static final String ITSHELF_LOGO = "/img/logo/itshelf-logo.png";
    private static final Vendor IT_SHELF = new Vendor(
            "ItShelf",
            ITSHELF_LOGO,
            "http://www.itshelf.com"
    );

    /**
     * Mocked method for tests.
     */
    public Stream<Course> findCoursesBy(String searchTerm) {
        return Stream.of(
                new Course(
                        searchTerm + " Java 11",
                        IT_SHELF,
                        ITSHELF_LOGO,
                        "http://www.itshelf.com/course/java11",
                        "Java 11 OCP certificate",
                        AUTHOR),
                new Course(
                        searchTerm + "Java 12",
                        IT_SHELF,
                        ITSHELF_LOGO,
                        "http://www.itshelf.com/course/java12",
                        "Java 12 the future is here",
                        AUTHOR),
                new Course(searchTerm + "JUnit5",
                        IT_SHELF,
                        ITSHELF_LOGO,
                        "http://www.itshelf.com/course/junit5",
                        "Using it for ages",
                        AUTHOR));
    }
}
