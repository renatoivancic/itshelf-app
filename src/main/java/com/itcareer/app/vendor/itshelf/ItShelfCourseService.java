package com.itcareer.app.vendor.itshelf;

import com.itcareer.app.core.CourseService;
import com.itcareer.app.domain.Course;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

/**
 * ItShelf course vendor for testing purposes. It was used at the beginning of the development.
 * It can be used for testing new development features or specific edge-cases.
 *
 * It is turned off in production.
 */
@Service
@ConditionalOnProperty(
        value="vendor.itshelf.enabled",
        havingValue = "true",
        matchIfMissing = false)
public class ItShelfCourseService implements CourseService {

    private final ItShelfCourseApiClient itShelfCourseApiService;

    public ItShelfCourseService(ItShelfCourseApiClient itShelfCourseApiService) {
        this.itShelfCourseApiService = itShelfCourseApiService;
    }

    @Override
    public Stream<Course> findCoursesBy(String searchTerm) {
        return itShelfCourseApiService.findCoursesBy(searchTerm);
    }

    @Override
    public String toString() {
        return "ItShelfCourseService";
    }
}
