package com.itcareer.app.vendor.microsoft;

/**
 * Thrown if the Microsoft courses couldn't be loaded at the application runtime.
 */
public class MicrosoftCoursesInitializationException extends RuntimeException{

    private static final String MESSAGE = "Microsoft courses couldn't be loaded (Fatal error, application will break). ";
    public MicrosoftCoursesInitializationException(String s) {
        super(MESSAGE + s);
    }
}
