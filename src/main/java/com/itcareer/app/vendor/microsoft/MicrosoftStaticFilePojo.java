package com.itcareer.app.vendor.microsoft;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * https://codebeautify.org/json-to-java-converter
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MicrosoftStaticFilePojo {
    private final List<Module> modules;

    public MicrosoftStaticFilePojo(@JsonProperty("modules") List<Module> modules) {

        this.modules = modules;
    }

    public List<Module> getModules() {
        return modules;
    }

    @Override
    public String toString() {
        return "MicrosoftStaticFilePojo{" +
                "modules=" + modules +
                '}';
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
class Module {
    private final String summary;
    private final String uid;
    private final String type;
    private final String title;
    private final int durationInMinutes;
    private final String iconUrl;
    private final String locale;
    private final String lastModified;
    private final String url;

    /**
     * Sonar disables as class has so many properties that have to be injected at initialization.
     */
    @SuppressWarnings("java:S107")
    public Module(@JsonProperty("summary") String summary,
                  @JsonProperty("uid") String uid,
                  @JsonProperty("type") String type,
                  @JsonProperty("title") String title,
                  @JsonProperty("duration_in_minutes") int durationInMinutes,
                  @JsonProperty("icon_url") String iconUrl,
                  @JsonProperty("locale") String locale,
                  @JsonProperty("last_modified") String lastModified,
                  @JsonProperty("url") String url) {
        this.summary = summary;
        this.uid = uid;
        this.type = type;
        this.title = title;
        this.durationInMinutes = durationInMinutes;
        this.iconUrl = iconUrl;
        this.locale = locale;
        this.lastModified = lastModified;
        this.url = url;
    }

    public String getUid() {
        return uid;
    }

    public String getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public int getDurationInMinutes() {
        return durationInMinutes;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public String getLocale() {
        return locale;
    }

    public String getLastModified() {
        return lastModified;
    }

    public String getUrl() {
        return url;
    }

    public String getSummary() {
        return summary;
    }

    @Override
    public String toString() {
        return "Module{" +
                "summary='" + summary + '\'' +
                ", uid='" + uid + '\'' +
                ", type='" + type + '\'' +
                ", title='" + title + '\'' +
                ", durationInMinutes=" + durationInMinutes +
                ", iconUrl='" + iconUrl + '\'' +
                ", locale='" + locale + '\'' +
                ", lastModified='" + lastModified + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
