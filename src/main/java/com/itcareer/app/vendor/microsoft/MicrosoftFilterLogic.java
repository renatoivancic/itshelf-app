package com.itcareer.app.vendor.microsoft;

import com.itcareer.app.domain.Course;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MicrosoftFilterLogic {

    private MicrosoftFilterLogic() {

    }

    /**
     * For each course and each word in search term try to find matching. For covering special terms we map csharp and sharp to #.
     * As in Microsoft headline CSharp courses are sometimes defined as C # C# or C Sharp.
     *
     * @param searchTerm       by which user searches for courses. Can contain multiple words
     * @param microsoftCourses list of all of the Microsoft courses
     * @return filtered result with Microsoft courses that comply with the search term and searching algorithm
     */
    public static Stream<Course> findCoursesBy(String searchTerm, List<Course> microsoftCourses) {
        List<String> searchWords = splitSearchTermWords(searchTerm);


        return microsoftCourses
                .stream()
                .filter(microsoftCourse ->
                        searchWords
                                .stream()
                                .map(searchWord -> new CourseXSearchWords(microsoftCourse, searchWord))
                                .anyMatch(stringPredicate)
                );
    }

    static Predicate<CourseXSearchWords> stringPredicate = courseXSearchWords -> {
        boolean cSharp = false;
        if (courseXSearchWords.searchWord.contains("sharp") || courseXSearchWords.searchWord.contains("csharp")) {
            cSharp = true;
        }
        return courseXSearchWords.course.getHeadline().toLowerCase().contains(courseXSearchWords.searchWord)
                || courseXSearchWords.course.getName().toLowerCase().contains(courseXSearchWords.searchWord)
                || (cSharp && courseXSearchWords.course.getName().contains("#"));
    };

    public static List<String> splitSearchTermWords(String searchTerm) {
        return Arrays
                .stream(searchTerm.split(" "))
                .filter(word -> word.length() > 2)
                .map(String::toLowerCase)
                .collect(Collectors.toList());
    }
}

class CourseXSearchWords {
    Course course;
    String searchWord;

    public CourseXSearchWords(Course microsoftCourse, String searchWord) {
        this.course = microsoftCourse;
        this.searchWord = searchWord;
    }
}