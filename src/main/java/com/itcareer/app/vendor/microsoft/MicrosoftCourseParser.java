package com.itcareer.app.vendor.microsoft;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itcareer.app.domain.Course;
import com.itcareer.app.domain.Vendor;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class only responsibility is to parse static JSON file the is preloaded from the MicrosoftLearn API and return the list of
 * {@link com.itcareer.app.domain.Course}.
 * <p>
 * course.name -> modules[].title
 * course.vendor.name -> Microsoft
 * course.vendor.url -> https://docs.microsoft.com/en-us/learn/
 * course.vendor.logoUrl -> /static/img/vendors/microsoft/microsoft-logo.png
 * course.url -> modules[].url
 * course.imageUrl -> modules[].icon_url
 * course.headline -> modules[].summary (Parsed, stripped of tags)
 * course.author -> / Microsoft does not have authors
 * <p>
 * Headline is stripped of HTML tags with Jsoup library.
 */
@Component
public class MicrosoftCourseParser {

    private static String microsoftCoursesFilePath = "classpath:vendors/microsoft/microsoft-courses.json";
    private static final Logger logger =
            LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private static final Vendor MICROSOFT_VENDOR = new Vendor("Microsoft Learn", "/img/vendors/microsoft/microsoft-logo.png", "https://docs.microsoft.com/en-us/learn/");

    public List<Course> getMicrosoftCourses() {
        return microsoftCourses;
    }

    private final List<Course> microsoftCourses;

    public MicrosoftCourseParser(ResourceLoader resourceLoader) {
        try {
            MicrosoftStaticFilePojo microsoftStaticFilePojo = readMicrosoftCoursesFromFile(resourceLoader);

            microsoftCourses = convertToCourses(microsoftStaticFilePojo);

            logger.debug("Loaded Microsoft courses, size: {}", microsoftCourses.size());
            logger.trace("Loaded Microsoft courses, pojo: {}", microsoftCourses);
        } catch (Exception exception) {
            throw new MicrosoftCoursesInitializationException(exception.getMessage());
        }
    }

    private MicrosoftStaticFilePojo readMicrosoftCoursesFromFile(ResourceLoader resourceLoader) throws IOException {
        InputStream partnersInputStream = resourceLoader.getResource(microsoftCoursesFilePath).getInputStream();
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(partnersInputStream, MicrosoftStaticFilePojo.class);
    }

    private List<Course> convertToCourses(MicrosoftStaticFilePojo microsoftStaticFilePojo) {
        return microsoftStaticFilePojo.getModules().stream().map(
                coursePojo -> new Course(
                        coursePojo.getTitle(),
                        MICROSOFT_VENDOR,
                        coursePojo.getIconUrl(),
                        coursePojo.getUrl(),
                        Jsoup.parse(coursePojo.getSummary()).text(),
                        null
                )).collect(Collectors.toList());
    }
}