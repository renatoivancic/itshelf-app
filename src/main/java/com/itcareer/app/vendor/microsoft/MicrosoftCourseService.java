package com.itcareer.app.vendor.microsoft;

import com.itcareer.app.core.CourseService;
import com.itcareer.app.domain.Course;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

@Service
public class MicrosoftCourseService implements CourseService {

    private final MicrosoftCourseParser microsoftCourseParser;

    public MicrosoftCourseService(MicrosoftCourseParser microsoftCourseParser) {

        this.microsoftCourseParser = microsoftCourseParser;
    }

    /**
     * Search microsoft course by search term
     *
     * Really simple algorithm is implemented for searching the proper course.
     *
     *
     * 1. Search term is split up by spaces
     * 2. Only words with more than 3 characters are taken into the account
     * 3. Its being tested if one of the words in search term is matching the word in title or in the description.
     *
     * Still not perfect. For example for "ASP" the first solution is because the name contains "aspects".
     *
     * @param searchTerm by which the the vendor specific courses are being searched for
     * @return list of found courses for this particular search term
     */
    @Override
    public Stream<Course> findCoursesBy(String searchTerm) {

        if(searchTerm == null) {
            return Stream.empty();
        }

        return MicrosoftFilterLogic.findCoursesBy(searchTerm, microsoftCourseParser.getMicrosoftCourses());
    }

    @Override
    public String toString() {
        return "MicrosoftCourseService";
    }
}
