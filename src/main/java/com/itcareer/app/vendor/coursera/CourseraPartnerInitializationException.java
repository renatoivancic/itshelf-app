package com.itcareer.app.vendor.coursera;

/**
 * Thrown if the partners from Coursera could not be loaded at the application runtime.
 */
public class CourseraPartnerInitializationException extends RuntimeException{

    private static final String MESSAGE = "Coursera partners could not be loaded (Fatal error, application will break). ";

    public CourseraPartnerInitializationException(String s) {
        super(MESSAGE + s);
    }
}
