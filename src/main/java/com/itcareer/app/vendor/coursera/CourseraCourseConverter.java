package com.itcareer.app.vendor.coursera;

import com.itcareer.app.core.ITShelfStringUtils;
import com.itcareer.app.domain.Course;
import com.itcareer.app.domain.Vendor;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

@Component
public class CourseraCourseConverter {

    private CourseraPartnerService courseraPartnerService;

    public CourseraCourseConverter(CourseraPartnerService courseraPartnerService) {
        this.courseraPartnerService = courseraPartnerService;
    }

    private static final Vendor COURSERA = new Vendor("Coursera", "/img/vendors/coursera/coursera-logo.png", "https://www.coursera.org/");

    private static final Set<String> IT_DOMAINS = new HashSet<>();

    static {
        IT_DOMAINS.add("computer-science");
        IT_DOMAINS.add("information-technology");
        IT_DOMAINS.add("data-science");
    }

    public Stream<Course> extractCourses(CourseraCoursesSearchResponse listOfCoursesEntity) {

        if (listOfCoursesEntity == null) return Stream.empty();
        return listOfCoursesEntity.
                getElements().
                stream().
                filter(filterDomains).
                map(convertElementToCourse);
    }

    private final Function<Element, Course> convertElementToCourse = result -> new Course(
            result.getName(),
            COURSERA,
            result.getPhotoUrl(),
            "https://www.coursera.org/learn/" + result.getSlug(),
            ITShelfStringUtils.ellipsize(result.getDescription(),150),
            courseraPartnerService.getPartnerDisplayName(result.getPartnerIds()));

    private static final Predicate<Element> filterDomains = element ->

            element.getDomainTypes()
                    .stream()
                    .map(DomainType::getDomainId)
                    .anyMatch(IT_DOMAINS::contains);

    public Stream<Course> extractCoursesInParallel(CourseraCoursesSearchResponse listOfCoursesEntity) {
        if (listOfCoursesEntity == null) return Stream.empty();
        return listOfCoursesEntity.
                getElements().
                stream().
                parallel().
                unordered().
                filter(filterDomains).
                map(convertElementToCourse);
    }
}
