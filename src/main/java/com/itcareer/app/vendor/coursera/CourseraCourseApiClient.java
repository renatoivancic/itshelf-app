package com.itcareer.app.vendor.coursera;

import com.itcareer.app.domain.Course;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Stream;

/**
 * <p><b>Coursera Affiliate API client</b></p>
 *
 * <p>Documentation https://build.coursera.org/app-platform/catalog/</p>
 *
 * <p>Used for searching courses that are offered by Coursera</p>
 */
@Component
public class CourseraCourseApiClient {

    private final RestTemplate restTemplate;

    private final CourseraCourseConverter courseraCourseConverter;

    @Value("${vendor.coursera.api.url}")
    private String courseraApiUrl;

    /**
     * How many courses will be loaded at the same time
     */
    @Value("${vendor.coursera.api.querylimit}")
    private String courseraApiQueryLimit;

    public CourseraCourseApiClient(RestTemplate restTemplate, CourseraCourseConverter courseraCourseConverter) {
        this.courseraCourseConverter = courseraCourseConverter;
        this.restTemplate = restTemplate;
    }

    /**
     * <p>Search courses in courses endpoint with following query q=search&query=malware+underground</p>
     *
     * <p><b>From Coursera Documentation</b></p>
     *
     * <p>The course collecion has a search API. The following is an example search request:<p>
     * <p>
     * pre>curl "https://api.coursera.org/api/courses.v1?q=search&query=malware+underground"</pre>
     * <p>
     * pre>https://api.coursera.org/api/courses.v1?q=search&query=security&fields=language,shortDescription,domainTypes&limit=1000</pre>
     * <p>
     * <p>
     * <p>Top it domains, that can be used to filter out the results:</p>
     * <ul>
     *     <li>computer-science</li>
     *     <li>information-technology</li>
     *     <li>data-science</li>
     * </ul>
     *
     *  Throws runtime UnknownHostException in case that the connection to the external API Service can not be established
     *
     * @param searchTerm the term used to execute the search
     * @return filtered list of courses that are offered by Coursera
     */
    public Stream<Course> findCoursesBy(String searchTerm) {

        String searchUrl = courseraApiUrl + "?q=search&query={query}&fields=partnerIds,description,domainTypes,photoUrl&limit={limit}";

        ResponseEntity<CourseraCoursesSearchResponse> listOfCoursesEntity =
                restTemplate.exchange(
                        searchUrl,
                        HttpMethod.GET,
                        getHttpEntity(),
                        CourseraCoursesSearchResponse.class,
                        getUrlParameters(searchTerm)
                );

        return courseraCourseConverter.extractCourses(listOfCoursesEntity.getBody());
    }

    private Map<String, String> getUrlParameters(String searchTerm) {
        Map<String, String> vars = new HashMap<>();
        vars.put("query", searchTerm);
        vars.put("limit", courseraApiQueryLimit);
        return vars;
    }

    private HttpEntity<HttpHeaders> getHttpEntity() {

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json, text/plain, */*");
        return new HttpEntity<>(headers);
    }
}
