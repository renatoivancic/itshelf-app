package com.itcareer.app.vendor.coursera;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;
import java.util.Set;

/**
 * The response entity from  list courses endpoint.
 *
 * <pre>
 * {
 *   "elements": [
 *     {
 *       "courseType": "v2.ondemand",
 *       "id": "vhLVVTe9EeWnxw5wP_KHTw",
 *       "slug": "html-css-javascript-for-web-developers",
 *       "name": "HTML, CSS, and Javascript for Web Developers"
 *     },
 *     ...
 *     ]
 * }
 * </pre>
 * <p>
 * Additional annotation on record because of
 * https://github.com/FasterXML/jackson-future-ideas/issues/46
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CourseraCoursesSearchResponse {

    private List<Element> elements;

    public List<Element> getElements() {
        return elements;
    }

    public void setElements(List<Element> elements) {
        this.elements = elements;
    }

    @Override
    public String toString() {
        return "CourseraCoursesSearchResponse{" +
                "elements=" + elements +
                '}';
    }
}

class Element {
    private String courseType;
    private String id;
    private String slug;
    private String name;
    private Set<DomainType> domainTypes;
    private String photoUrl;
    private String description;
    private List<String> partnerIds;

    @Override
    public String toString() {
        return "Element{" +
                "courseType='" + courseType + '\'' +
                ", id='" + id + '\'' +
                ", slug='" + slug + '\'' +
                ", name='" + name + '\'' +
                ", domainTypes=" + domainTypes +
                ", photoUrl='" + photoUrl + '\'' +
                ", description='" + description + '\'' +
                ", partnerIds=" + partnerIds +
                '}';
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCourseType() {
        return courseType;
    }

    public void setCourseType(String courseType) {
        this.courseType = courseType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<DomainType> getDomainTypes() {
        return domainTypes;
    }

    public void setDomainTypes(Set<DomainType> domainTypes) {
        this.domainTypes = domainTypes;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public List<String> getPartnerIds() {
        return partnerIds;
    }

    public void setPartnerIds(List<String> partnerIds) {
        this.partnerIds = partnerIds;
    }
}

class DomainType {
    private String domainId;
    private String subdomainId;

    public String getDomainId() {
        return domainId;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }

    public String getSubdomainId() {
        return subdomainId;
    }

    public void setSubdomainId(String subdomainId) {
        this.subdomainId = subdomainId;
    }

    @Override
    public String toString() {
        return "DomainType{" +
                "domainId='" + domainId + '\'' +
                ", subdomainId='" + subdomainId + '\'' +
                '}';
    }
}
