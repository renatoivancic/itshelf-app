package com.itcareer.app.vendor.coursera;

import com.itcareer.app.core.CourseService;
import com.itcareer.app.core.search.SearchServiceAllCondition;
import com.itcareer.app.domain.Course;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Service;

import java.net.UnknownHostException;
import java.util.stream.Stream;

/**
 * Do not initialize this service if XSS filter is not enabled.
 * To avoid sending unfiltered requests to vendors.
 *
 * Methods will re-throw runtime exceptions. Logic in upper level has to handle them.
 */
@Service
@Conditional(SearchServiceAllCondition.class)
public class CourseraCourseService implements CourseService {

    private final CourseraCourseApiClient courseraCourseApiClient;

    public CourseraCourseService(CourseraCourseApiClient courseraCourseApiClient) {

        this.courseraCourseApiClient = courseraCourseApiClient;
    }

    /**
     *
     * @param searchTerm by which the the vendor specific courses are being searched for
     *
     * @throws UnknownHostException in case that the connection to the external API Service can not be established
     */
    @Override
    public Stream<Course> findCoursesBy(String searchTerm) {
        return courseraCourseApiClient.findCoursesBy(searchTerm);
    }

    @Override
    public String toString() {
        return "CourseraCourseService";
    }
}
