package com.itcareer.app.vendor.coursera;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This is the Coursera JSON response from the partner endpoint
 *
 * <pre>
 * "elements": [
 *
 *     {
 *         "name": "The Chinese University of Hong Kong",
 *         "id": "45",
 *         "shortName": "cuhk"
 *     },
 *     {
 *         "name": "Università di Napoli Federico II",
 *         "id": "619",
 *         "shortName": "unina"
 *     },
 *     {
 * </pre>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CourseraPartnerPojo {
    private final Map<String, String> elements;

    public CourseraPartnerPojo(
            @JsonProperty("elements") List<CourseraPartner> elements) {
        this.elements = elements.parallelStream().collect(
                Collectors.toConcurrentMap(CourseraPartner::getId, CourseraPartner::getName));
    }

    public Map<String, String> getElements() {
        return elements;
    }

    @Override
    public String toString() {
        return "CourseraPartnerPojo{" +
                "elements=" + elements +
                '}';
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
class CourseraPartner {
    private final String name;
    private final String id;

    CourseraPartner(@JsonProperty("name")String name,@JsonProperty("id") String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }
}
