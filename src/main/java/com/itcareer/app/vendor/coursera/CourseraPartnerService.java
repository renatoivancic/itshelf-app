package com.itcareer.app.vendor.coursera;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Current solution is reading all of the partners information form the static file baked
 * in the application itself.
 */
@Service
public class CourseraPartnerService {

    private static String courseraPartnersFile = "classpath:vendors/coursera/coursera-partners.json";
    private static final Logger logger =
            LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private final CourseraPartnerPojo courseraPartnerPojo;
    ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Load partners entities from file to memory.
     *
     * @throws RuntimeException if the file can not be read.
     */
    public CourseraPartnerService(ResourceLoader resourceLoader) {

        try {
            InputStream partnersInputStream = resourceLoader.getResource(courseraPartnersFile).getInputStream();
            courseraPartnerPojo = objectMapper.readValue(partnersInputStream, CourseraPartnerPojo.class);
            logger.trace("Loaded coursera partner pojo: {}", courseraPartnerPojo);
        } catch (Exception exception) {
            throw new CourseraPartnerInitializationException( exception.getMessage());
        }
    }

    public String getPartnerDisplayName(String partnerId) {
        return courseraPartnerPojo.getElements().get(partnerId);
    }

    /**
     * If partner is not found return empty string but log the message with error.
     *
     * @param partnerIds list of partner coursera partner ids by which names are resolved.
     * @return Text of all partners. Missing partners will be filtered out.
     */
    public String getPartnerDisplayName(Collection<String> partnerIds) {
        return partnerIds
                .stream()
                .map(partnerId -> {String partnerName = courseraPartnerPojo.getElements().get(partnerId);
                if(partnerName == null) {
                    logger.info(".getPartnerDisplayName() | Partner name is missing for following partner id: {}", partnerId);
                }
                return partnerName;})
                .filter(Objects::nonNull)
                .collect(Collectors.joining(""));
    }
}
