package com.itcareer.app.vendor.udemy;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UdemyCoursesSearchResponse {

    private final int count;
    private final List<Results> results;

    public UdemyCoursesSearchResponse() {
        this(0, new ArrayList<>());
    }

    public UdemyCoursesSearchResponse(int count, List<Results> results) {
        this.count = count;
        this.results = results;
    }

    public int getCount() {
        return count;
    }

    public List<Results> getResults() {
        return results;
    }
}

/**
 * <p>ID</p>
 *
 * <p>Title</p>
 *
 * <p>imageMedium</p>
 *
 * <p>Author -> Instructors:
 * results.visible_instructors.display_name</p>
 */
class Results {
    private final int id;
    private final String title;
    private final String imageMedium;
    private final String url;
    private final String headline;
    private final List<VisibleInstructors> visibleInstructors;
    private final Category category;

    public Results(@JsonProperty("id") int id,
                   @JsonProperty("title") String title,
                   @JsonProperty("image_240x135") String imageMedium,
                   @JsonProperty("url") String url,
                   @JsonProperty("headline") String headline,
                   @JsonProperty("visible_instructors") List<VisibleInstructors> visibleInstructors,
                   @JsonProperty("primary_category") Category category) {
        this.id = id;
        this.title = title;
        this.imageMedium = imageMedium;
        this.url = url;
        this.headline = headline;
        this.visibleInstructors = visibleInstructors;
        this.category = category;
    }

    public String getHeadline() {
        return headline;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getImageMedium() {
        return imageMedium;
    }

    public String getUrl() {
        return url;
    }

    public String getInstructorsAsText() {
        return visibleInstructors
                .stream()
                .map(VisibleInstructors::getDisplayName)
                .collect(Collectors.joining(","));
    }

    public Category getCategory() {
        return category;
    }
}

class VisibleInstructors {
    private final String displayName;

    VisibleInstructors(@JsonProperty("display_name") String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}

class Category {
    private final int id;

    public Category(@JsonProperty("id") int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}