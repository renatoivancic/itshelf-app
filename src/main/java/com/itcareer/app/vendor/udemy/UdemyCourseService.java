package com.itcareer.app.vendor.udemy;

import com.itcareer.app.core.CourseService;
import com.itcareer.app.core.search.SearchServiceAllCondition;
import com.itcareer.app.domain.Course;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

/**
 * Do not initialize this service if XSS filter is not enabled.
 * To avoid sending unfiltered requests to vendors.
 */
@Service
@Conditional(SearchServiceAllCondition.class)
@ConditionalOnProperty(
        value="vendor.udemy.enabled",
        havingValue = "true")
public class UdemyCourseService implements CourseService {

    private final UdemyCourseApiClient udemyCourseApiClient;

    public UdemyCourseService(UdemyCourseApiClient udemyCourseApiClient) {

        this.udemyCourseApiClient = udemyCourseApiClient;
    }

    @Override
    public Stream<Course> findCoursesBy(String searchTerm) {
        return udemyCourseApiClient.findCoursesBy(searchTerm);
    }

    @Override
    public String toString() {
        return "UdemyCourseService";
    }
}
