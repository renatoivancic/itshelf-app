package com.itcareer.app.vendor.udemy;

import com.itcareer.app.domain.Course;
import com.itcareer.app.domain.Vendor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.lang.invoke.MethodHandles;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Stream;

/**
 * Udemy Affiliate API client
 * <p>
 * Configuration: https://www.udemy.com/user/edit-api-clients/
 * <p>
 * Documentation: https://www.udemy.com/developers/affiliate/
 * <p>
 * Used for searching courses that are offered by Udemy
 *
 * Images
 *
 * "image_240x135": "https://img-a.udemycdn.com/course/240x135/1046722_cbd7_2.jpg",
 *
 * "image_480x270": "https://img-a.udemycdn.com/course/480x270/1046722_cbd7_2.jpg",
 */
@Component
public class UdemyCourseApiClient {

    private final RestTemplate restTemplate;
    private static final Vendor UDEMY = new Vendor("Udemy", "/img/vendors/udemy/udemy-logo.png", "https://www.udemy.com/");

    @Value("${vendor.udemy.api.url}")
    private String udemyApiUrl;

    @Value("${vendor.udemy.api.id}")
    private String udemyApiClientId;

    @Value("${vendor.udemy.api.secret}")
    private String udemyApiClientSecret;

    @Value("${vendor.udemy.api.queryLimit}")
    private String udemyApiClientQueryLimit;
    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    public UdemyCourseApiClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    /**
     * Search courses in following endpoint courses/?page=1&page_size=3&search=java
     *
     * @param searchTerm the term used to execute the search
     * @return filtered list of courses that are offered by Udemy
     */
    public Stream<Course> findCoursesBy(String searchTerm) {

        String searchUrl = udemyApiUrl + "/courses/?page={page}&page_size={page_size}&search={search}&fields[course]=@default,primary_category";


        logger.debug("Udemy REST API call started for: {}, limit: {}", searchTerm, udemyApiClientQueryLimit);
        long start = System.currentTimeMillis();

        ResponseEntity<UdemyCoursesSearchResponse> listOfCoursesEntity =
                restTemplate.exchange(
                        searchUrl,
                        HttpMethod.GET,
                        getHttpEntity(),
                        UdemyCoursesSearchResponse.class,
                        getUrlParameters(searchTerm)
                );

        long end = System.currentTimeMillis();
        logger.debug("Udemy REST API call started for: {}, limit: {}, times: {} milliseconds / {} seconds", searchTerm, udemyApiClientQueryLimit, end-start, (end-start)/1000);

        return extractCourses(listOfCoursesEntity);
    }

    private Stream<Course> extractCourses(ResponseEntity<UdemyCoursesSearchResponse> listOfCoursesEntity) {
        UdemyCoursesSearchResponse body = listOfCoursesEntity.getBody();
        if (body == null) return Stream.empty();
        return body
                .getResults()
                .stream()
                // It can happen that some courses are without categories.
                // Filter them out and do not use them in results
                .filter(results ->
                        results.getCategory() !=null && (results.getCategory().getId()==288 || results.getCategory().getId()==294))
                .map(result -> new Course(
                        result.getTitle(),
                        UDEMY,
                        result.getImageMedium(),
                        "https://www.udemy.com" + result.getUrl(),
                        result.getHeadline(),
                        result.getInstructorsAsText()));
    }

    private Map<String, String> getUrlParameters(String searchTerm) {
        Map<String, String> vars = new HashMap<>();
        vars.put("page", "1");
        vars.put("page_size", udemyApiClientQueryLimit);
        vars.put("search", searchTerm);
        return vars;
    }

    private HttpEntity<HttpHeaders> getHttpEntity() {

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json, text/plain, */*");
        headers.set("Authorization", getAuthBase64Header());
        headers.set("Content-Type", "application/json;charset=utf-8");
        return new HttpEntity<>(headers);
    }

    private String getAuthBase64Header() {
        String auth = udemyApiClientId + ":" + udemyApiClientSecret;
        byte[] encodedAuth = Base64.getEncoder().encode(
                auth.getBytes(StandardCharsets.US_ASCII));
        return "Basic " + new String(encodedAuth);
    }
}
