package com.itcareer.app.core.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import java.util.List;
import java.util.stream.Collectors;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {

  @Value("#{${com.itshelf.adminusers}}")
  private List<String> userList;

  @Bean
  public SecurityFilterChain webSecurity(HttpSecurity http) throws Exception {
    http.authorizeHttpRequests((authorize) -> authorize
            .requestMatchers("/admin/login").permitAll()
            .requestMatchers("/admin/**").authenticated()
            .anyRequest().permitAll())
        .formLogin(form -> form
            .loginPage("/login")
            .permitAll());
    return http.build();
  }

  @Bean
  public UserDetailsService userDetailsService() {

    List<UserDetails> users = userList.stream().map(userAsText -> {
      String[] userAsTextSplit = userAsText.split(",");
      return User
          .withUsername(userAsTextSplit[0])
          .password(userAsTextSplit[1])
          .roles(userAsTextSplit[2])
          .build();
    }).collect(Collectors.toList());

    return new InMemoryUserDetailsManager(users);
  }
}