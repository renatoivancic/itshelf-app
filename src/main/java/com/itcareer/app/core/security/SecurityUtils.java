package com.itcareer.app.core.security;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

public class SecurityUtils {

    public static final String ANONYMOUS_USER_NAME = "anonymousUser";

    private SecurityUtils() {

    }

    public static boolean isLoggedIn() {
        return !SecurityContextHolder.getContext().getAuthentication().getName().equals(ANONYMOUS_USER_NAME);
    }

    public static String getPrincipalUserName() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = "";
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else {
            username = principal.toString();
        }
        return username;
    }
}
