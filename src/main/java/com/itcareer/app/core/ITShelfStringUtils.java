package com.itcareer.app.core;

import javax.annotation.Nullable;

public class ITShelfStringUtils {

    private ITShelfStringUtils(){}

    private static final String NON_THIN = "[^iIl1.,']";

    private static int textWidth(String str) {
        return (str.length() - str.replaceAll(NON_THIN, "").length() / 2);
    }

    /**
     * Taken from https://stackoverflow.com/questions/3597550/ideal-method-to-truncate-a-string-with-ellipsis
     *
     * Ellipsize the text but doesn't break words.
     *
     * @param text text that has to be shorten to ideally max characters, can be null.
     * @param max maximum length of the text, but can be a bit longer as well
     * @return ellipsized text or null if input text was null
     */
    public static String ellipsize(@Nullable String text, int max) {

        if(text == null) return null;

        if (textWidth(text) <= max)
            return text;

        // Start by chopping off at the word before max
        // This is an over-approximation due to thin-characters...
        int end = text.lastIndexOf(' ', max - 3);

        // Just one long word. Chop it off.
        if (end == -1)
            return text.substring(0, max-3) + "...";

        // Step forward as long as textWidth allows.
        int newEnd = end;
        do {
            end = newEnd;
            newEnd = text.indexOf(' ', end + 1);

            // No more spaces.
            if (newEnd == -1)
                newEnd = text.length();

        } while (textWidth(text.substring(0, newEnd) + "...") < max);

        return text.substring(0, end) + "...";
    }
}
