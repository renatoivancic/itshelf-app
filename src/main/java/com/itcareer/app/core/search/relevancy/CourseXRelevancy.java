package com.itcareer.app.core.search.relevancy;

import com.itcareer.app.domain.Course;

/**
 * Wrapper class that adds relevancy value to the {@link Course} entity.
 * Relevancy is value that is used only for sorting purposes of a result.
 *
 * For this reason its not part of the {@link Course} domain object.
 */
public class CourseXRelevancy {
    private final Course course;
    private final int relevancy;

    public CourseXRelevancy(Course course, int relevancy) {
        this.course = course;
        this.relevancy = relevancy;
    }

    public Course getCourse() {
        return course;
    }

    public int getRelevancy() {
        return relevancy;
    }
}
