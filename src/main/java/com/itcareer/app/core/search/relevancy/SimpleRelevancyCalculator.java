package com.itcareer.app.core.search.relevancy;

import com.itcareer.app.domain.Course;

/**
 * Class responsible to calculate relevancy value for the class.
 *
 * This class in particular is taking course ordinality and appends its vendor name to it.
 * {@link Course}s are sorted by ordinality and a name
 */
public class SimpleRelevancyCalculator {

    private int internalCounter = 0;

    /**
     * Calculate actual relevancy of a course.
     *
     *
     * @param course can be used in more sophisticated algorithms of calculating relevancy
     * @return integer value representing relevancy of a course based on a searching term.
     */
    public int calculateRelevancy(Course course) { // NOSONAR
        return internalCounter++;
    }
}
