package com.itcareer.app.core.search;

import com.itcareer.app.core.CourseService;
import com.itcareer.app.core.search.relevancy.CourseXRelevancy;
import com.itcareer.app.domain.Course;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service that is responsible for delegation of information fetching to specific vendor services.
 *
 * It is also handling any exceptions that bubble up from deeper levels.
 */
@Service
public class SearchService {

    private final List<CourseService> courseServices;

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public SearchService(List<CourseService> courseServices) {

        this.courseServices = courseServices;
        logger.info("SearchService() | Initialized Search Services: {}", courseServices);
    }

    /**
     * Delegates the search for courses to concrete vendor services.
     *
     * If exception was thrown from delegated call handle it correctly at this point.
     * Skip result and log the error with context information.
     * ResourceAccessException is thrown for example if connection can not be established between
     * client and vendor server.
     *
     * @return list of aggregated courses or empty list if search term is empty or nothing is found.
     */
    @Cacheable("courses-list")
    public @NonNull List<Course> findCoursesBy(final String searchTerm)  {

        if(!StringUtils.hasLength(searchTerm)) {
            return new ArrayList<>();
        }
        logger.info("Concrete search is being executed for : {}", searchTerm);

        long start = System.currentTimeMillis();

        List<Course> courses = (courseServices).parallelStream()
                .flatMap(service -> new SearchInvocationHandler(searchTerm).handleInvocation(service))
                .sorted(Comparator.comparing(CourseXRelevancy::getRelevancy))
                .map(CourseXRelevancy::getCourse)
                .collect(Collectors.toList());

        long end = System.currentTimeMillis();
        logger.info("Concrete search aggregation for \"{}\", found {} courses, took {} milliseconds / {} seconds", searchTerm, courses.size(), end-start, (end-start)/1000);

        return courses;
    }
}