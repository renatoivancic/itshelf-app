package com.itcareer.app.core.search;

import com.itcareer.app.core.CourseService;
import com.itcareer.app.core.search.relevancy.CourseXRelevancy;
import com.itcareer.app.core.search.relevancy.SimpleRelevancyCalculator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.stream.Stream;

public class SearchInvocationHandler {

    private final String searchTerm;
    private final SimpleRelevancyCalculator simpleRelevancyCalculator;
    SearchInvocationHandler(String searchTerm) {
        this.searchTerm = searchTerm;
        this.simpleRelevancyCalculator = new SimpleRelevancyCalculator();
    }

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    Stream<CourseXRelevancy> handleInvocation(CourseService courseService) {
        try {
            logger.debug("Service {} invocation started.", courseService);
            long start = System.currentTimeMillis();
            Stream<CourseXRelevancy> courseXRelevancyStream = courseService
                    .findCoursesBy(searchTerm)
                    .map(course -> new CourseXRelevancy(course, simpleRelevancyCalculator.calculateRelevancy(course)));
            long end = System.currentTimeMillis();
            logger.debug("Service invocation ended{}, {} milliseconds / {} seconds", courseService, end-start, (end-start)/1000);
            return courseXRelevancyStream;
        } catch(Exception exception) {
            logger.error("SearchService() | Search with search term {} was not successful, this means user did not get correct results when " +
                    "searching for specific course. Results from at least one vendor are missing ", searchTerm, exception);
            return Stream.empty();
        }
    }
}