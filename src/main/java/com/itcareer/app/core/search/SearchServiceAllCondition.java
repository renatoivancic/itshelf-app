package com.itcareer.app.core.search;

import org.springframework.boot.autoconfigure.condition.AllNestedConditions;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;

public class SearchServiceAllCondition extends AllNestedConditions {

    public SearchServiceAllCondition() {
        super(ConfigurationPhase.PARSE_CONFIGURATION);
    }

    @ConditionalOnProperty( value="com.itcareer.toggle.xssfilter",
            havingValue = "true",
            matchIfMissing = true)
    static class XSSFilterCondition {
    }

    @ConditionalOnProperty(value="com.itcareer.toggle.3rdpartyvendors",
            havingValue = "true",
            matchIfMissing = true)
    static class VendorSearchServicesCondition {
    }
}
