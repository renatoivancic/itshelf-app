package com.itcareer.app.core.thymeleaf;

import java.util.Arrays;
import java.util.stream.Collectors;

public class ThymeleafUtils {

    private ThymeleafUtils(){}

    /**
     * Combine first 3 keywords and adds hash sign for Twitter hashtags in share button.
     *
     * @param keywords list of keywords of a specific event
     * @return text containing twitter hashtags of first 3 keywords of the event
     */
    public static String getKeywordsAsHashtags(String[] keywords) {
        if(keywords == null || keywords.length == 0) {
          return "";
        } else {
            return Arrays.stream(keywords).limit(3).collect(Collectors.joining(" #","#",""));
        }
    }
}
