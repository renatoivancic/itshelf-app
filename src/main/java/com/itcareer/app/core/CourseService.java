package com.itcareer.app.core;

import com.itcareer.app.domain.Course;

import java.util.stream.Stream;

/**
 * Service that provides features like listing, querying, filtering courses from a specific vendor
 */
public interface CourseService {

    /**
     * Search for all courses from a vendor that match against a search term.
     *
     * @param searchTerm by which the the vendor specific courses are being searched for
     * @return List of vendor specific courses
     */
    Stream<Course> findCoursesBy(String searchTerm);
}
