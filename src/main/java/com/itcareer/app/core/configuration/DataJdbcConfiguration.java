package com.itcareer.app.core.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.data.convert.WritingConverter;
import org.springframework.data.jdbc.core.convert.JdbcCustomConversions;
import org.springframework.data.jdbc.repository.config.AbstractJdbcConfiguration;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;

@Configuration
public class DataJdbcConfiguration extends AbstractJdbcConfiguration {

    @Override
    public JdbcCustomConversions jdbcCustomConversions() {

        return new JdbcCustomConversions(Arrays.asList(TimestampTzToDateConverter.INSTANCE, ZonedDateTimeToTimestampConverter.INSTANCE));
    }

    @ReadingConverter
    enum TimestampTzToDateConverter implements Converter<Timestamp, ZonedDateTime> {

        INSTANCE;

        @Override
        public ZonedDateTime convert(Timestamp source) {
            java.time.Instant instant = Instant.ofEpochMilli(source.getTime());
            return ZonedDateTime.ofInstant(instant, ZoneId.of("Europe/Paris"));
        }
    }

    @WritingConverter
    enum ZonedDateTimeToTimestampConverter implements Converter<ZonedDateTime, Timestamp> {

        INSTANCE;

        @Override
        public Timestamp convert(ZonedDateTime source) {
            return Timestamp.from(source.toInstant());
        }
    }
}