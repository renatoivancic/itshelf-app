package com.itcareer.app.core.markdown;

import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;
import org.springframework.stereotype.Component;

/**
 * Class responsible for processing markdown content and rendering it as HTML document.
 *
 * It is using library to parse Markdown and then render HTML content.
 *
 * It is composed of constant Parser and Renderer.
 *
 * Main method is process() that accepts markdown as String and returns HTML as String.
 */
@Component
public class MarkdownProcessor {
    private static final Parser MARKDOWN_PARSER = Parser.builder().build();
    private static final HtmlRenderer HTML_RENDERER = HtmlRenderer.builder().build();

    public String process(String markdown) {
        Node document = MARKDOWN_PARSER.parse(markdown);
        return HTML_RENDERER.render(document);
    }
}
