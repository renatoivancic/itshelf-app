package com.itcareer.app.landing;

import com.itcareer.app.domain.Event;
import com.itcareer.app.event.EventService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "/")
public class LandingController {

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    public static final int EVENTS_LIST_SIZE = 6;
    public static final String LANDING_PAGE_VIEW = "landing";
    public static final String LANDING_MODEL_NAME = "landingModel";

    private final EventService eventService;

    @Value("${com.itshelf.landing.numberOfEvents}")
    private Integer numberOfEvents;

    public LandingController(EventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping
    public ModelAndView getPage() {

        List<Event> events = eventService.findEventBy("").stream().limit(EVENTS_LIST_SIZE).collect(Collectors.toList());
        LandingResult landingResult = new LandingResult(
                "/img/logo/itshelf-logo.png",
                "/img/logo/itshelf-logo-sign.png", events, numberOfEvents);
        logger.debug("Requesting Landing Page");
        return new ModelAndView(LANDING_PAGE_VIEW, Map.of(LANDING_MODEL_NAME, landingResult));
    }
}
