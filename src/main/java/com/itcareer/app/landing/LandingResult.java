package com.itcareer.app.landing;

import com.itcareer.app.domain.Event;

import java.util.List;

public class LandingResult {
    private final String pathToLogo;
    private final String pathToLogoSign;
    private final List<Event> upcomingEvents;
    private final int numberOfUpcomingEvents;

    public LandingResult(String pathToLogo, String pathToLogoSign, List<Event> upcomingEvents, int numberOfUpcomingEvents) {

        this.pathToLogo = pathToLogo;
        this.pathToLogoSign = pathToLogoSign;
        this.upcomingEvents = upcomingEvents;
        this.numberOfUpcomingEvents = numberOfUpcomingEvents;
    }

    public String getPathToLogoSign() {
        return pathToLogoSign;
    }

    public String getPathToLogo() {
        return pathToLogo;
    }

    public List<Event> getUpcomingEvents() {
        return upcomingEvents;
    }

    @Override
    public String toString() {
        return "LandingResult{" +
                "pathToLogo='" + pathToLogo + '\'' +
                ", pathToLogoSign='" + pathToLogoSign + '\'' +
                ", upcomingEvents=" + upcomingEvents +
                ", numberOfUpcomingEvents=" + numberOfUpcomingEvents +
                '}';
    }

    public int getNumberOfUpcomingEvents() {
        return numberOfUpcomingEvents;
    }
}
