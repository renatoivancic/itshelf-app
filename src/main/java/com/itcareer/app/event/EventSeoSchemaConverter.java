package com.itcareer.app.event;

import com.itcareer.app.core.ITShelfStringUtils;
import com.itcareer.app.domain.Event;
import com.itcareer.app.seo.model.*;

import java.time.format.DateTimeFormatter;

/**
 * Class responsible to convert Event entity to Seo Event Schema.org entity.
 */
public class EventSeoSchemaConverter {

    private EventSeoSchemaConverter() {}

    private static final int SCHEMA_JSON_LD_DESCRIPTION_LENGTH = 250;

    public static EventSchemaOrgPojo convertEventTo(Event event) {

        return new EventSchemaOrgPojo(
                event.getName(),
                DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(event.getDateFrom()),
                event.getDateTo() != null ? DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(event.getDateTo()): null,
                event.getImage(),
                ITShelfStringUtils.ellipsize(event.getDescription(), SCHEMA_JSON_LD_DESCRIPTION_LENGTH),
                event.getUrl(),
                EventAttendanceMode.ONLINE_EVENT_ATTENDANCE_MODE,
                EventStatusType.EVENT_SCHEDULED,
                getOffer(event.getPriceFrom()),
                new Organization(event.getContactInfoEmail(), event.getVendorName(), event.getVendorLink()),
                event.getLocationAddress() != null ? new Place(event.getLocationAddress()):null);
    }

    /**
     * Get price currency and value form one string
     */
    private static Offer getOffer(String priceAsText) {
        if(priceAsText == null) {
            return null;
        }
        String[] price = priceAsText.split(" ");
        String currency = null;
        String value = null;
        for(String part: price) {
            if(isNumeric(part)) {
                value = part;
            } else {
                currency = part;
            }
        }
        return new Offer(value, currency);
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum); // NOSONAR have to try to convert numeric value
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
