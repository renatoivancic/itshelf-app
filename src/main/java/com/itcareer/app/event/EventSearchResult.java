package com.itcareer.app.event;

import com.itcareer.app.domain.Event;

import java.util.List;

public class EventSearchResult {
    private final List<Event> events;
    private final String searchTerm;
    private final String pathToLogo;
    private final String pathToLogoSign;

    public EventSearchResult(List<Event> events, String searchTerm, String pathToLogo, String pathToLogoSign) {
        this.events = events;
        this.searchTerm = searchTerm;
        this.pathToLogo = pathToLogo;
        this.pathToLogoSign = pathToLogoSign;
    }

    public String getPathToLogoSign() {
        return pathToLogoSign;
    }

    public List<Event> getEvents() {
        return events;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public String getPathToLogo() {
        return pathToLogo;
    }

    public EventSearchResult withEvents(List<Event> events) {
        return new EventSearchResult(events, this.searchTerm,this.pathToLogo, this.pathToLogoSign);
    }

    @Override
    public String toString() {
        return "SearchResult{" +
                "events=" + events +
                ", searchTerm='" + searchTerm + '\'' +
                ", pathToLogo='" + pathToLogo + '\'' +
                ", pathToLogoSign='" + pathToLogoSign + '\'' +
                '}';
    }
}
