package com.itcareer.app.event;

import com.itcareer.app.core.markdown.MarkdownProcessor;
import com.itcareer.app.core.security.SecurityUtils;
import com.itcareer.app.domain.Event;
import com.itcareer.app.seo.SeoService;
import com.itcareer.app.serp.SearchTermSanitizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;

/**
 * Event controller that serves results for:
 * <ul>
 * <li>Search results for events </li>
 * <li>Event detail page</li>
 * </ul>
 * <p>
 * Events can be filtered by the searchTerm.
 * </p>
 * <p>
 * Events can be one of (Conventions, Conferences, Meetups, Online Conferences, Camps, Workshops).
 * </p>
 */
@Controller
@RequestMapping(value = "/events")
public class EventController {

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    public static final String EVENT_SEARCH_RESULTS_MODEL_KEY = "eventSearchResults";
    public static final String EVENT_DETAIL_RESULTS_MODEL_KEY = "eventDetailResults";
    public static final String LOGGED_IN_MODEL_KEY = "isLoggedIn";
    public static final String IMG_LOGO_ITSHELF_LOGO_PNG = "/img/logo/itshelf-logo.png";
    public static final String IMG_LOGO_ITSHELF_LOGO_SIGN_PNG = "/img/logo/itshelf-logo-sign.png";
    public static final String EVENT_RESULTS_VIEW_NAME = "events";
    public static final String EVENT_DETAIL_PAGE_VIEW_NAME = "event";

    private final EventService eventService;
    private final SearchTermSanitizer searchTermSanitizer;
    private final MarkdownProcessor markdownProcessor;

    public EventController(EventService eventService, SearchTermSanitizer searchTermSanitizer, MarkdownProcessor markdownProcessor) {
        this.eventService = eventService;
        this.searchTermSanitizer = searchTermSanitizer;
        this.markdownProcessor = markdownProcessor;
    }

    @GetMapping
    public ModelAndView getSearchResult(@RequestParam(value = "searchTerm", required = false) String searchTerm) {

        String sanitizedSearchTerm = searchTermSanitizer.sanitizeInput(searchTerm);
        logger.debug("Searching Events, searchTerm: {}, sanitizedSearchTerm: {}", searchTerm, sanitizedSearchTerm);

        EventSearchResult searchResult = new EventSearchResult(
                List.of(),
                sanitizedSearchTerm,
                IMG_LOGO_ITSHELF_LOGO_PNG,
                IMG_LOGO_ITSHELF_LOGO_SIGN_PNG);

        return new ModelAndView(EVENT_RESULTS_VIEW_NAME,
                Map.of(EVENT_SEARCH_RESULTS_MODEL_KEY,
                        searchResult.withEvents(eventService.findEventBy(sanitizedSearchTerm))));
    }

    /**
     * Load event detail page found by the slug.
     *
     * @param slug seo friendly url path part of the URL
     * @return view that will render specific detail page for an event
     */
    @GetMapping("/{slug}")
    public ModelAndView getEventDetailResult(@PathVariable("slug") String slug) {

        logger.debug("Event Detail Page with slug: {}", slug);

        Event event = eventService.findEventBySlug(slug);
        String eventJsonLd = SeoService.generateSchemaOrgFor(EventSeoSchemaConverter.convertEventTo(event));
        String descriptionAsHTML = markdownProcessor.process(event.getDescription());
        List<Event> similarEvents = eventService.findSimilarEvents(event);

        logger.info("Event schema: {}", eventJsonLd);

        EventDetailResult eventDetailResult = new EventDetailResult(event,
                IMG_LOGO_ITSHELF_LOGO_PNG, IMG_LOGO_ITSHELF_LOGO_SIGN_PNG,
                similarEvents , descriptionAsHTML, eventJsonLd);

        return new ModelAndView(EVENT_DETAIL_PAGE_VIEW_NAME,
                Map.of(EVENT_DETAIL_RESULTS_MODEL_KEY, eventDetailResult,
                        LOGGED_IN_MODEL_KEY, SecurityUtils.isLoggedIn())
        );
    }
}
