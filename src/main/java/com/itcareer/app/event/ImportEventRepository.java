package com.itcareer.app.event;

import com.itcareer.app.domain.Event;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@ConditionalOnProperty(
        name = "com.itshelf.toggle.eventimport",
        matchIfMissing = false, havingValue="true")
public interface ImportEventRepository extends EventRepository {

    @Query("SELECT * " +
            "    FROM event e " +
            "    WHERE to_tsvector( coalesce(e.name, '') || ' ' || coalesce(e.keywords, '')) @@  plainto_tsquery(:searchTerm) " +
            "    AND date_from::date >= CURRENT_DATE  " +
            // "    AND status = 'Active' " +
            "    ORDER BY e.date_from ASC;")
    List<Event> findAllFromTodayBySearchTerm(@Param("searchTerm") String searchTerm);

    @Query("SELECT * " +
            "    FROM event e " +
            "    WHERE to_tsvector( coalesce(e.name, '') || ' ' || coalesce(e.keywords, '')) @@  to_tsquery(:keywords) " +
            "    AND date_from::date >= CURRENT_DATE  " +
            // "    AND status = 'Active' " +
            "    ORDER BY e.date_from ASC;")
    List<Event> findAllFromTodayByKeywords(@Param("keywords") String keywords);

    @Query("SELECT * " +
            "FROM event e " +
            "WHERE date_from::date >= CURRENT_DATE  " +
            // "AND status = 'Active' " +
            "ORDER BY e.date_from ASC")
    List<Event> findMostRecentUpcomingEvents();

    @Query("SELECT * FROM event e WHERE e.slug = :slug")
    Optional<Event> findBySlug(String slug);
}
