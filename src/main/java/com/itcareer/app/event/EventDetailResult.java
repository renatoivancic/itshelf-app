package com.itcareer.app.event;

import com.itcareer.app.domain.Event;

import java.util.List;

public class EventDetailResult {
    private final String pathToLogo;
    private final String pathToLogoSign;
    private final Event event;
    private final List<Event> relatedEvents;
    private final String descriptionAsHTML;
    private final String eventJsonLd;

    public EventDetailResult(Event event, String pathToLogo, String pathToLogoSign, List<Event> relatedEvents, String descriptionAsHTML, String eventJsonLd) {
        this.pathToLogo = pathToLogo;
        this.pathToLogoSign = pathToLogoSign;
        this.event = event;
        this.relatedEvents = relatedEvents;
        this.descriptionAsHTML = descriptionAsHTML;
        this.eventJsonLd = eventJsonLd;
    }

    public String getDescriptionAsHTML() {
        return descriptionAsHTML;
    }

    public String getPathToLogoSign() {
        return pathToLogoSign;
    }

    public String getPathToLogo() {
        return pathToLogo;
    }

    public Event getEvent() {
        return event;
    }

    @Override
    public String toString() {
        return "EventDetailResult{" +
                "pathToLogo='" + pathToLogo + '\'' +
                ", pathToLogoSign='" + pathToLogoSign + '\'' +
                ", event=" + event +
                '}';
    }

    public List<Event> getRelatedEvents() {
        return relatedEvents;
    }

    public String getEventJsonLd() {
        return eventJsonLd;
    }
}
