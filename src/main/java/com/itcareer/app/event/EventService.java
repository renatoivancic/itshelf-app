package com.itcareer.app.event;

import com.itcareer.app.domain.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.lang.invoke.MethodHandles;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class EventService {

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private final EventRepository eventRepository;

    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    /**
     * Find most relevant events by provided search term.
     * If searchTerm is empty then return next 20 upcoming events.
     *
     * @param searchTerm by which events have to be filtered
     * @return list of the filtered events
     */
    public @NonNull List<Event> findEventBy(@NonNull String searchTerm) {
        List<Event> events;
        if (searchTerm.isEmpty()) {
            events = eventRepository.findMostRecentUpcomingEvents();
        } else {
            searchTerm = "%" + searchTerm + "%";
            events = eventRepository.findAllFromTodayBySearchTerm(searchTerm);
        }

        logger.trace("Found events: {}", events);
        return events;
    }

    /**
     * Find event by its slug.
     *
     * @param slug Human friendly id that is being used as part of the URL path.
     * @return matching {@link Event} object.
     * @throws java.util.NoSuchElementException if no event can be found for this specific slug.
     */
    public Event findEventBySlug(String slug) {

        Event event = eventRepository.findBySlug(slug).orElseThrow();
        logger.trace("Found event {}, by slug: {}", event, slug);

        return event;
    }

    /**
     * Find similar events to this one based on the list of keywords of the event.
     * First 3 keywords are used for relevancy search.
     *
     * @param event by which a list of relevant events are being calculated.
     * @return list of similar events
     */
    public List<Event> findSimilarEvents(Event event) {
        String first3PreparedKeywords = Arrays.stream(event.getArrayOfKeywords())
                .limit(3)
                .map(word -> "'"+ word+"'")
                .collect(Collectors.joining(" | "));
        return eventRepository.findAllFromTodayByKeywords(first3PreparedKeywords)
                .stream()
                .filter(relatedEvent -> !event.getId().equals(relatedEvent.getId()))
                .limit(6)
                .collect(Collectors.toList());
    }

    /**
     *
     * @return all events for admin interface no matter what is the status or date of the event
     * @throws java.util.NoSuchElementException if no event can be found for this specific id.
     */
    public List<Event> getAllEvents() {
        return StreamSupport
                .stream(eventRepository.findAll().spliterator(), false)
                .sorted(Comparator.comparingLong(Event::getId))
                .collect(Collectors.toList());
    }

    public Event findEventById(Long id) {
        return eventRepository.findById(id).orElseThrow();
    }

    public Event saveEvent(Event event) {
        return eventRepository.save(event);
    }
}
