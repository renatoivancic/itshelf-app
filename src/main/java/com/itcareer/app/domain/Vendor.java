package com.itcareer.app.domain;

import java.util.Objects;

public class Vendor {
    private final String name;
    private final String logoUrl;
    private final String url;

    public Vendor(String name, String logoUrl, String url) {
        this.name = name;
        this.logoUrl = logoUrl;
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vendor vendor = (Vendor) o;
        return Objects.equals(name, vendor.name) &&
                Objects.equals(logoUrl, vendor.logoUrl) &&
                Objects.equals(url, vendor.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, logoUrl, url);
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    @Override
    public String toString() {
        return "Vendor{" +
                "name='" + name + '\'' +
                ", logoUrl='" + logoUrl + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
