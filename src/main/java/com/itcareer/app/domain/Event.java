package com.itcareer.app.domain;

import org.springframework.data.annotation.Id;

import java.time.ZonedDateTime;

/**
 * Entity representing an event registered in ITShelf platform.
 */
public class Event {
    @Id
    private final Long id;
    private final String name;
    private final String url;
    private final ZonedDateTime dateFrom;
    private final ZonedDateTime dateTo;
    private final String type;
    private final String locationCity;
    private final String locationCountry;
    private final String image;
    private final boolean free;
    private final String priceFrom;
    private final String priceTo;
    private final String description;
    private final String contactInfoEmail;
    private final String vendorName;
    private final String vendorLink;
    private final String twitterLink;
    private final String facebookLink;
    private final String youtubeLink;
    private final String linkedinLink;
    private final String status;
    private final String keywords;
    private final String locationAddress;
    private final String slug;

    public Event(Long id, // NOSONAR it has so many fields, and records are not yet fully compatible
                 String name,
                 String url,
                 ZonedDateTime dateFrom,
                 ZonedDateTime dateTo,
                 String type,
                 String locationCity,
                 String locationCountry,
                 String image,
                 Boolean free,
                 String priceFrom,
                 String priceTo,
                 String description,
                 String contactInfoEmail,
                 String vendorName,
                 String vendorLink,
                 String twitterLink,
                 String status,
                 String facebookLink,
                 String youtubeLink,
                 String linkedinLink,
                 String keywords,
                 String locationAddress,
                 String slug) {
        this.id = id;

        this.name = name;
        this.url = url;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.type = type;
        this.locationCity = locationCity;
        this.locationCountry = locationCountry;
        this.image = image;

        // if in the database boolean is set to false JDBC Data reads value as null
        this.free = Boolean.TRUE.equals(free);
        this.priceFrom = priceFrom;
        this.priceTo = priceTo;
        this.description = description;
        this.contactInfoEmail = contactInfoEmail;
        this.vendorName = vendorName;
        this.vendorLink = vendorLink;
        this.twitterLink = twitterLink;
        this.facebookLink = facebookLink;
        this.youtubeLink = youtubeLink;
        this.linkedinLink = linkedinLink;
        this.status = status;
        this.keywords = keywords;
        this.locationAddress = locationAddress;
        this.slug = slug;
    }

    public static Event getEmptyEvent() {
        return new Event(null, "", "", null, null, "", "", "",
                "", false, "", "", "", "", "",
                "", "", "", "", "", "", "",
                "", "");
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", dateFrom=" + dateFrom +
                ", toDate=" + dateTo +
                ", city=" + locationCity +
                ", country=" + locationCountry +
                ", type='" + type + '\'' +
                ", vendorName='" + vendorName + '\'' +
                ", keywords='" + keywords + '\'' +
                ", status=" + status +
                '}';
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public ZonedDateTime getDateFrom() {
        return dateFrom;
    }

    public ZonedDateTime getDateTo() {
        return dateTo;
    }

    public String getType() {
        return type;
    }

    public String getCity() {
        return locationCity;
    }

    public String getCountry() {
        return locationCountry;
    }

    public String getImage() {
        return image;
    }

    public boolean isFree() {
        return free;
    }

    public String getPriceFrom() {
        return priceFrom;
    }

    public String getPriceTo() {
        return priceTo;
    }

    public String getDescription() {
        return description;
    }

    public String getContactInfoEmail() {
        return contactInfoEmail;
    }

    public String getVendorName() {
        return vendorName;
    }

    public String getVendorLink() {
        return vendorLink;
    }

    public String getTwitterLink() {
        return twitterLink;
    }

    public String getFacebookLink() {
        return facebookLink;
    }

    public String getYoutubeLink() {
        return youtubeLink;
    }

    public String getLinkedinLink() {
        return linkedinLink;
    }

    public String getStatus() {
        return status;
    }

    public String getKeywords() {
        return keywords;
    }

    public String[] getArrayOfKeywords() {
        if (keywords != null) {
            return keywords.split(",");
        } else {
            return new String[]{};
        }
    }

    public String getLocationAddress() {
        return locationAddress;
    }

    public String getSlug() {
        return slug;
    }
}
