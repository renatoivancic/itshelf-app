package com.itcareer.app.domain;

import java.util.Objects;

public class Course {
    private final String name;
    private final Vendor vendor;
    private final String imageUrl;
    private final String url;
    private final String headline;
    private final String author;

    public Course(String name, Vendor vendor, String imageUrl, String url, String headline, String author) {
        this.name = name;
        this.vendor = vendor;
        this.imageUrl = imageUrl;
        this.url = url;
        this.headline = headline;
        this.author = author;
    }

    /**
     * Raw value of author display name.
     * Can be null if original course data doesn't contain authors.
     */
    public String getAuthor() {

        return author;
    }

    /**
     * Return author display name if exists otherwise return vendor display name.
     */
    public String getDisplayAuthor() {
        if(getAuthor() != null) {
            return getAuthor();
        } else {
            return getVendor().getName();
        }
    }

    /**
     * For display purposes create a text that is concatenated with author and vendor display name in form of:
     *
     * author / vendor
     *
     * If there is no author display only vendor name.
     */
    public String getAuthorWithVendor() {

        if(getAuthor() != null) { // Sonar lint does not support Objects.nonNull() method https://community.sonarsource.com/t/sq-8-4-1-unable-to-analyze-java-14-code/28972
            return getAuthor() + " / " + getVendor().getName();
        } else {
            return getVendor().getName();
        }
    }

    public String getHeadline() {
        return headline;
    }

    public String getName() {
        return name;
    }

    public Vendor getVendor() {
        return vendor;
    }


    @Override
    public String toString() {
        return "Course{" +
                "name='" + name + '\'' +
                ", vendor=" + vendor +
                ", imageUrl='" + imageUrl + '\'' +
                ", url='" + url + '\'' +
                ", headline='" + headline + '\'' +
                ", author='" + author + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return Objects.equals(name, course.name) &&
                Objects.equals(vendor, course.vendor) &&
                Objects.equals(imageUrl, course.imageUrl) &&
                Objects.equals(url, course.url) &&
                Objects.equals(headline, course.headline) &&
                Objects.equals(author, course.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, vendor, imageUrl, url, headline, author);
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getUrl() {
        return url;
    }
}
