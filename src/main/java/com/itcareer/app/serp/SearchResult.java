package com.itcareer.app.serp;

import com.itcareer.app.domain.Course;

import java.util.List;

public class SearchResult {
    private final List<Course> courses;
    private final String searchTerm;
    private final String pathToLogo;
    private final String pathToLogoSign;

    public SearchResult(List<Course> courses, String searchTerm, String pathToLogo, String pathToLogoSign) {
        this.courses = courses;
        this.searchTerm = searchTerm;
        this.pathToLogo = pathToLogo;
        this.pathToLogoSign = pathToLogoSign;
    }

    public String getPathToLogoSign() {
        return pathToLogoSign;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public String getPathToLogo() {
        return pathToLogo;
    }

    public SearchResult withCourses(List<Course> courses) {
        return new SearchResult(courses, this.searchTerm,this.pathToLogo, this.pathToLogoSign);
    }

    @Override
    public String toString() {
        return "SearchResult{" +
                "courses=" + courses +
                ", searchTerm='" + searchTerm + '\'' +
                ", pathToLogo='" + pathToLogo + '\'' +
                ", pathToLogoSign='" + pathToLogoSign + '\'' +
                '}';
    }
}
