package com.itcareer.app.serp;

import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.PolicyFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * https://github.com/OWASP/java-html-sanitizer
 * <p>
 * HTML encoding is not enough if the injection will be used in the script.
 * <p>
 * Text does not need to be longer than 100 characters.
 * <p>
 * Strip all known HTML/Script tags with org.owasp.html.
 */
@Component
public class SearchTermSanitizer {

    private static final PolicyFactory POLICY_FACTORY = new HtmlPolicyBuilder()
            .requireRelNofollowOnLinks()
            .toFactory();

    /**
     * <p>Flag for managing XSS filter.
     * Meant for testing the XSS attacks.
     * </p>
     * <p>
     * Set to
     * <ul>
     * <li><b>true</b> it will trim input and escape html</li>
     * <li><b>false</b> it will return the original input without sanitizing it</li>
     * </ul>
     */
    @Value("${com.itcareer.toggle.xssfilter}")
    private boolean filterXss;

    public String sanitizeInput(String searchTerm) {
        if (searchTerm == null) {
            return "";
        } else if (!filterXss) {

            // Used for testing XSS with Kali, do not filter it
            return searchTerm;
        }

        if (searchTerm.length() > 100) {
            searchTerm = searchTerm.substring(0, 100);
        }

        searchTerm = POLICY_FACTORY.sanitize(searchTerm);

        return searchTerm;
    }
}
