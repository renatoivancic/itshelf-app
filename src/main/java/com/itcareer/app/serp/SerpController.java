package com.itcareer.app.serp;

import com.itcareer.app.analytics.AnalyticsService;
import com.itcareer.app.core.search.SearchService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;

/**
 * Search page controller that serves results for:
 * - Courses (Cybrary, Coursera, Udemy, AWS, ...)
 * - Labs ()
 * - Events (Codemotion, Conventions, Conferences, Meetups, Online Conferences)
 * - Streams (Twitch) / Channels (Youtube)
 * - Persons (Authors, Developers, Evangelists)
 * - Blogs, Pages (Jaxenter)
 * - Books (Amazon, Weily, Pearson, wrox) https://isbndb.com/search/books/java%20oca
 * - Papers
 */
@Controller
@RequestMapping(value = "/search")
public class SerpController {

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    public static final String SEARCH_VIEW = "search";
    public static final String SEARCH_RESULTS_MODEL_NAME = "searchResults";

    private final SearchService searchService;
    private final AnalyticsService analyticsService;
    private final SearchTermSanitizer searchTermSanitizer;

    public SerpController(SearchService searchService, AnalyticsService analyticsService, SearchTermSanitizer searchTermSanitizer) {
        this.searchService = searchService;
        this.analyticsService = analyticsService;
        this.searchTermSanitizer = searchTermSanitizer;
    }

    @GetMapping
    public ModelAndView getSearchResult(@RequestParam(value = "searchTerm", required = false) String searchTerm) {

        String sanitizedSearchTerm = searchTermSanitizer.sanitizeInput(searchTerm);
        logger.debug("Searching, searchTerm: {}, sanitizedSearchTerm: {}", searchTerm, sanitizedSearchTerm);

        SearchResult searchResult = new SearchResult(
                List.of(),
                sanitizedSearchTerm,
                "/img/logo/itshelf-logo.png",
                "/img/logo/itshelf-logo-sign.png");

        if(StringUtils.isEmpty(searchTerm)){
            return new ModelAndView(SEARCH_VIEW, Map.of(SEARCH_RESULTS_MODEL_NAME, searchResult));
        }

        analyticsService.persistSearchEvent(searchTerm);

        return new ModelAndView(SEARCH_VIEW, Map.of(SEARCH_RESULTS_MODEL_NAME, searchResult.withCourses(searchService.findCoursesBy(sanitizedSearchTerm))));
    }
}
