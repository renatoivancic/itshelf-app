package com.itcareer.app.admin;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * X-Robots header is added to admin section of the page to prevent search engines to index it.
 */
@WebFilter(urlPatterns = {"/admin/*","/login"})
public class AdminWebFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        httpServletResponse.addHeader("X-Robots-Tag", "noindex, nofollow");
        chain.doFilter(request, response);
    }
}
