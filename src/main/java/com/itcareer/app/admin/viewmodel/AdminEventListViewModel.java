package com.itcareer.app.admin.viewmodel;

import com.itcareer.app.admin.enrich.AdminEvent;
import com.itcareer.app.admin.enrich.EventQualityStats;

import java.util.List;

public class AdminEventListViewModel {
    private final List<AdminEvent> events;
    private final String username;
    private final EventQualityStats eventQualityStats;

    public AdminEventListViewModel(List<AdminEvent> events, String username, EventQualityStats eventQualityStats) {
        this.events = events;
        this.username = username;
        this.eventQualityStats = eventQualityStats;
    }

    public String getUsername() {
        return username;
    }

    public List<AdminEvent> getEvents() {
        return events;
    }

    @Override
    public String toString() {
        return "AdminResult{" +
                "events=" + events +
                '}';
    }

    public EventQualityStats getEventQualityStats() {
        return eventQualityStats;
    }
}
