package com.itcareer.app.admin.viewmodel;

import com.itcareer.app.domain.Event;

public class AdminEventViewModel {
    private final Event event;
    private final String username;

    public AdminEventViewModel(Event event, String username) {
        this.event = event;
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public Event getEvent() {
        return event;
    }

    @Override
    public String toString() {
        return "AdminResult{" +
                "event=" + event +
                '}';
    }
}
