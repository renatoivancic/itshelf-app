package com.itcareer.app.admin.viewmodel;

import java.util.HashMap;
import java.util.Map;

public class ErrorMap {
    Map<String, String> errors = new HashMap<>();

    public void setError(String code, String message) {
        errors.put(code, message);
    }

    public boolean hasErrors() {
        return errors.size()>0;
    }

    public boolean containsError(String code) {
        return errors.containsKey(code);
    }
}
