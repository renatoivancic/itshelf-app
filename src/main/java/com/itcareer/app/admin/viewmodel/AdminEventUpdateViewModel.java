package com.itcareer.app.admin.viewmodel;

import com.itcareer.app.admin.enrich.DateTimeConstrain;

import jakarta.validation.constraints.NotBlank;

/**
 * Event information used to display and edit information in the HTML form.
 * Every part of event that can be edited in Admin Detail Page has to be present here.
 * <p>
 * Class has to be mutable as new values can be set in the view.
 */
public class AdminEventUpdateViewModel {

    public static final String ADMIN_NEW_EVENT_ID_NEW = "new";
    String id;
    @NotBlank
    private String name;
    private String description;
    private String image;
    private String keywords;
    private String status;
    @DateTimeConstrain
    private String dateTimeFrom;
    private String dateTimeTo;
    private String timezoneOffsetFrom;
    private String timezoneOffsetTo;
    private Boolean isDateTimeToFlag;
    private String externalURL;
    private String slug;
    private String type;
    private String city;
    private String country;
    private String address;
    private String twitterLink;
    private String facebookLink;
    private String youtubeLink;
    private String linkedinLink;
    private Boolean isFree;
    private String priceFrom;
    private String priceTo;
    private String vendorName;
    private String vendorLink;
    private String vendorEmail;
    public AdminEventUpdateViewModel(String id, // NOSONAR Its has so many properties
                                     String name,
                                     String description,
                                     String image,
                                     String keywords,
                                     String status,
                                     String dateTimeFrom,
                                     String dateTimeTo,
                                     Boolean isDateTimeToFlag,
                                     String timezoneOffsetFrom,
                                     String timezoneOffsetTo,
                                     String externalURL,
                                     String slug,
                                     String type,
                                     Boolean isFree,
                                     String priceFrom,
                                     String priceTo,
                                     String city,
                                     String country,
                                     String address,
                                     String vendorName,
                                     String vendorLink,
                                     String vendorEmail,
                                     String twitterLink,
                                     String facebookLink,
                                     String youtubeLink,
                                     String linkedinLink) {
        this.name = name;
        this.description = description;
        this.image = image;
        this.keywords = keywords;
        this.status = status;
        this.dateTimeFrom = dateTimeFrom;
        this.dateTimeTo = dateTimeTo;
        this.isDateTimeToFlag = isDateTimeToFlag != null && isDateTimeToFlag;
        this.timezoneOffsetFrom = timezoneOffsetFrom;
        this.timezoneOffsetTo = timezoneOffsetTo;
        this.externalURL = externalURL;
        this.slug = slug;
        this.type = type;
        this.city = city;
        this.country = country;
        this.address = address;
        this.twitterLink = twitterLink;
        this.facebookLink = facebookLink;
        this.youtubeLink = youtubeLink;
        this.linkedinLink = linkedinLink;
        this.isFree = isFree != null && isFree;
        this.priceFrom = priceFrom;
        this.priceTo = priceTo;
        this.vendorName = vendorName;
        this.vendorLink = vendorLink;
        this.vendorEmail = vendorEmail;
        this.id = id;
    }

    public static boolean isNewEvent(String id) {
        return ADMIN_NEW_EVENT_ID_NEW.equals(id);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExternalURL() {
        return externalURL;
    }

    public void setExternalURL(String externalURL) {
        this.externalURL = externalURL;
    }

    public String getTimezoneOffsetFrom() {
        return timezoneOffsetFrom;
    }

    public void setTimezoneOffsetFrom(String timezoneOffsetFrom) {
        this.timezoneOffsetFrom = timezoneOffsetFrom;
    }

    public String getTimezoneOffsetTo() {
        return timezoneOffsetTo;
    }

    public void setTimezoneOffsetTo(String timezoneOffsetTo) {
        this.timezoneOffsetTo = timezoneOffsetTo;
    }

    public boolean isDateTimeToFlag() {
        return isDateTimeToFlag;
    }

    public void setDateTimeToFlag(Boolean dateTimeTo) {
        isDateTimeToFlag = dateTimeTo != null && dateTimeTo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SaveEvent{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getDateTimeFrom() {
        return dateTimeFrom;
    }

    public void setDateTimeFrom(String dateTimeFrom) {
        this.dateTimeFrom = dateTimeFrom;
    }

    public String getDateTimeTo() {
        return dateTimeTo;
    }

    public void setDateTimeTo(String dateTimeTo) {
        this.dateTimeTo = dateTimeTo;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTwitterLink() {
        return twitterLink;
    }

    public void setTwitterLink(String twitterLink) {
        this.twitterLink = twitterLink;
    }

    public String getFacebookLink() {
        return facebookLink;
    }

    public void setFacebookLink(String facebookLink) {
        this.facebookLink = facebookLink;
    }

    public String getYoutubeLink() {
        return youtubeLink;
    }

    public void setYoutubeLink(String youtubeLink) {
        this.youtubeLink = youtubeLink;
    }

    public String getLinkedinLink() {
        return linkedinLink;
    }

    public void setLinkedinLink(String linkedinLink) {
        this.linkedinLink = linkedinLink;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isFree() {
        return isFree;
    }

    public void setFree(Boolean free) {
        isFree = free != null && free;
    }

    public String getPriceFrom() {
        return priceFrom;
    }

    public void setPriceFrom(String priceFrom) {
        this.priceFrom = priceFrom;
    }

    public String getPriceTo() {
        return priceTo;
    }

    public void setPriceTo(String priceTo) {
        this.priceTo = priceTo;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorLink() {
        return vendorLink;
    }

    public void setVendorLink(String vendorLink) {
        this.vendorLink = vendorLink;
    }

    public String getVendorEmail() {
        return vendorEmail;
    }

    public void setVendorEmail(String vendorEmail) {
        this.vendorEmail = vendorEmail;
    }
}
