package com.itcareer.app.admin;

import com.itcareer.app.admin.enrich.DateTimeHelper;
import com.itcareer.app.admin.enrich.ErrorMapFactory;
import com.itcareer.app.admin.enrich.EventQualityService;
import com.itcareer.app.admin.update.EventDetailModelViewFactory;
import com.itcareer.app.admin.update.UpdateStrategyFactory;
import com.itcareer.app.admin.viewmodel.AdminEventUpdateViewModel;
import com.itcareer.app.admin.viewmodel.AdminEventListViewModel;
import com.itcareer.app.admin.enrich.AdminEvent;
import com.itcareer.app.admin.viewmodel.ErrorMap;
import com.itcareer.app.core.security.SecurityUtils;
import com.itcareer.app.domain.Event;
import com.itcareer.app.event.EventService;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import jakarta.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {

    public static final String ADMIN_MAIN_VIEW = "admin/adminMain";
    public static final String ADMIN_EVENT_LIST_VIEW_MODEL_KEY = "adminEventListViewModel";

    private final EventService eventService;
    private final UpdateStrategyFactory updateStrategyFactory;

    public AdminController(EventService eventService) {
        this.eventService = eventService;
        this.updateStrategyFactory = new UpdateStrategyFactory(eventService);
    }

    /**
     * <ol>
     *   <li>1 Get all events</li>
     *   <li>2 Get words of events and check if its upcoming</li>
     *   <li>3 Calculate quality (only upcoming)</li>
     *   <li>4 Create DTO</li>
     *   <li>5 Return DTO</li>
     * </ol>
     *
     * @return DTO containing all the events and useful statistics for the admin.
     */
    @GetMapping
    public ModelAndView getAdminLandingPage() {

        List<Event> allEvents = eventService.getAllEvents();
        List<AdminEvent> adminEvents = allEvents
                .stream()
                .map(event -> new AdminEvent(event,
                        EventQualityService.countWordsUsingStringTokenizer(event.getDescription())
                , DateTimeHelper.isUpcoming(event.getDateFrom(), event.getDateTo())))
                .collect(Collectors.toList());
        return new ModelAndView(ADMIN_MAIN_VIEW,
                Map.of(ADMIN_EVENT_LIST_VIEW_MODEL_KEY,
                        new AdminEventListViewModel(adminEvents,
                                SecurityUtils.getPrincipalUserName(),
                                EventQualityService.getEventQualityStats(adminEvents))));
    }

    @GetMapping("/events/{id}")
    public ModelAndView getEventDetailPage(@PathVariable("id") String id) {
        Event event;
        if(AdminEventUpdateViewModel.isNewEvent(id)){
            event = Event.getEmptyEvent();
        } else {
            event = eventService.findEventById(Long.parseLong(id));
        }
        return EventDetailModelViewFactory.createEventDetailPageModelAndView(event, SecurityUtils.getPrincipalUserName());
    }

    /**
     * 1) Redirect in case that the new event was created to the path with the correct id.
     * 2) Verify that the date is in the correct format. That can be saved in the database.
     *    Use the validation annotation
     * 3) Return map of validation errors.
     * 4) Show errors on the admin page
     * 5) Default ZoneID is not valid.
     */
    @PostMapping("/events/{id}")
    public ModelAndView eventSubmit(@PathVariable("id") String id, @Valid @ModelAttribute AdminEventUpdateViewModel saveEvent, BindingResult bindingResult) {
        final String username = SecurityUtils.getPrincipalUserName();

        ErrorMap errorMap = ErrorMapFactory.buildErrorMap(bindingResult);

        return updateStrategyFactory
                .getUpdateStrategy(saveEvent,errorMap, username)
                .update();
    }
}
