package com.itcareer.app.admin.enrich;

public class EventQualityStats {
    private final double bad;
    private final double good;
    private final int numberOfAllEvents;
    private final int numberOfUpcomingEvents;

    public EventQualityStats(double bad, double good, int numberOfAllEvents, int numberOfUpcomingEvents) {
        this.bad = bad;
        this.good = good;
        this.numberOfAllEvents = numberOfAllEvents;
        this.numberOfUpcomingEvents = numberOfUpcomingEvents;
    }

    public int getNumberOfAllEvents() {
        return numberOfAllEvents;
    }

    public double getBad() {
        return bad;
    }

    public double getGood() {
        return good;
    }

    public int getNumberOfUpcomingEvents() {
        return numberOfUpcomingEvents;
    }
}
