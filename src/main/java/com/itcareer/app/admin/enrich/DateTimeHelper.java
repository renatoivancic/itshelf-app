package com.itcareer.app.admin.enrich;

import java.time.ZonedDateTime;

public class DateTimeHelper {

    private DateTimeHelper(){}

    public static boolean isUpcoming(ZonedDateTime from, ZonedDateTime to) {
        return ZonedDateTime.now().isBefore(to != null ? to : from);
    }

    public static String getDateTimeAsText(ZonedDateTime dateFrom) {
        if(dateFrom == null) {
            return "";
        }
        return dateFrom.toLocalDateTime().toString();
    }

    public static String getOffsetAsText(ZonedDateTime dateFrom) {
        if(dateFrom == null) {
            return "";
        }
        return dateFrom.getOffset().toString();
    }
}
