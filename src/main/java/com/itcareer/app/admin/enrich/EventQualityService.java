package com.itcareer.app.admin.enrich;

import java.util.List;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

public class EventQualityService {

    private EventQualityService() {

    }

    /**
     * calculate content quality of all the events
     * Can be improved with one go through the collection instead of calculating for every value separately, but at the moment its not of an importance
     *
     * @param adminEvents
     */
    public static EventQualityStats getEventQualityStats(List<AdminEvent> adminEvents) {
        List<AdminEvent> upcomingEvents = adminEvents.stream().filter(AdminEvent::isUpcoming).collect(Collectors.toList());
        double bad = 0;
        double good = 0;
        if (!upcomingEvents.isEmpty()) {
            bad = (double) upcomingEvents.stream().filter(event -> event.getNumberOfWords() < 150).count() * 100 / upcomingEvents.size();
            good = (double) upcomingEvents.stream().filter(event -> 150 <= event.getNumberOfWords()).count() * 100 / upcomingEvents.size();
        }
        return new EventQualityStats(bad, good, adminEvents.size(), upcomingEvents.size());
    }

    /**
     * Count the number of words in a String.
     *
     * @param sentence the text in which the words should be counted
     * @return number of words in sentence
     */
    public static int countWordsUsingStringTokenizer(String sentence) {
        if (sentence == null || sentence.isEmpty()) {
            return 0;
        }
        StringTokenizer tokens = new StringTokenizer(sentence);
        return tokens.countTokens();
    }
}
