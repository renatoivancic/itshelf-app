package com.itcareer.app.admin.enrich;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = DateTimeValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface DateTimeConstrain {
    String message() default "Invalid date time format";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}