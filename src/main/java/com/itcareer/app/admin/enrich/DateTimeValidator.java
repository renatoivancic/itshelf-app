package com.itcareer.app.admin.enrich;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

/**
 * Validate that the string can be parsed to date time presentation like
 * 2020-09-24T03:27
 */
public class DateTimeValidator implements
        ConstraintValidator<DateTimeConstrain, String> {

    @Override
    public void initialize(DateTimeConstrain dateTimeConstrain) { // NOSONAR
    }

    @Override
    public boolean isValid(String contactField,
                           ConstraintValidatorContext cxt) {
        if(contactField != null && (contactField.length() > 8)){
            try {
                LocalDateTime.parse(contactField);
            } catch (DateTimeParseException dtpe) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }
}