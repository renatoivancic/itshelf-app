package com.itcareer.app.admin.enrich;

import com.itcareer.app.admin.viewmodel.ErrorMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

public class ErrorMapFactory {

    private ErrorMapFactory() {}

    public static ErrorMap buildErrorMap(BindingResult bindingResult) {
        ErrorMap errorMap = new ErrorMap();
        for(ObjectError objectError : bindingResult.getAllErrors()){
            if(objectError instanceof FieldError) {
                errorMap.setError(((FieldError) objectError).getField(), objectError.getCode());
            }
        }
        return errorMap;
    }
}
