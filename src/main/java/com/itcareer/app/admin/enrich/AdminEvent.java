package com.itcareer.app.admin.enrich;

import com.itcareer.app.domain.Event;

public class AdminEvent {

    private final Event event;
    private final int numberOfWords;
    private final boolean isUpcoming;
    public AdminEvent(Event event, int numberOfWords, boolean isUpcoming) {
        this.event = event;
        this.numberOfWords = numberOfWords;
        this.isUpcoming = isUpcoming;
    }

    public Event getEvent() {
        return event;
    }

    public int getNumberOfWords() {
        return numberOfWords;
    }

    public boolean isUpcoming() {
        return isUpcoming;
    }
}
