package com.itcareer.app.admin.update;

import com.itcareer.app.admin.viewmodel.AdminEventUpdateViewModel;
import com.itcareer.app.domain.Event;
import com.itcareer.app.event.EventService;
import org.springframework.web.servlet.ModelAndView;

/**
 * 1) Create event from update event entity.
 * 2) Persist event.
 * 3) Return ModelView
 */
public class NewUpdate implements UpdateStrategy {

    private final EventService eventService;
    private final AdminEventUpdateViewModel saveEvent;
    private final String username;

    public NewUpdate(EventService eventService, AdminEventUpdateViewModel saveEvent, String username) {
        this.eventService = eventService;
        this.saveEvent = saveEvent;
        this.username = username;
    }

    @Override
    public ModelAndView update() {
        Event event = AdminEventUpdateConverter.buildEvent(Event.getEmptyEvent(), saveEvent);
        event = eventService.saveEvent(event);
        ModelAndView eventDetailPageModelAndView = EventDetailModelViewFactory.createEventDetailPageModelAndView(event, username);
        eventDetailPageModelAndView.setViewName("redirect:/admin/events/" + event.getId());
        return eventDetailPageModelAndView;
    }
}
