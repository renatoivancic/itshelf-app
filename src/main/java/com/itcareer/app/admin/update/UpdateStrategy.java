package com.itcareer.app.admin.update;

import org.springframework.web.servlet.ModelAndView;

public interface UpdateStrategy {
    public ModelAndView update();
}
