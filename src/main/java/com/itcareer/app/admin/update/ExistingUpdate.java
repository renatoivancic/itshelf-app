package com.itcareer.app.admin.update;

import com.itcareer.app.admin.viewmodel.AdminEventUpdateViewModel;
import com.itcareer.app.domain.Event;
import com.itcareer.app.event.EventService;
import org.springframework.web.servlet.ModelAndView;

public class ExistingUpdate implements UpdateStrategy {

    private final EventService eventService;
    private final String id;
    private final AdminEventUpdateViewModel saveEvent;
    private final String user;

    public ExistingUpdate(EventService eventService, String id, AdminEventUpdateViewModel saveEvent, String user) {
        this.eventService = eventService;
        this.id = id;
        this.saveEvent = saveEvent;
        this.user = user;
    }

    @Override
    public ModelAndView update() {
        Event event = eventService.findEventById(Long.parseLong(id));
        event = AdminEventUpdateConverter.buildEvent(event, saveEvent);
        event = eventService.saveEvent(event);
        return EventDetailModelViewFactory.createEventDetailPageModelAndView(event, user);
    }
}
