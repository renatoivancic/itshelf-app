package com.itcareer.app.admin.update;

import com.itcareer.app.admin.viewmodel.AdminEventUpdateViewModel;
import com.itcareer.app.admin.viewmodel.ErrorMap;
import com.itcareer.app.domain.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;

import java.lang.invoke.MethodHandles;

/**
 * Generate ModelView from update event entity with containing errors.
 */
public class NewUpdateWithError implements UpdateStrategy {

    private final AdminEventUpdateViewModel saveEvent;
    private final ErrorMap errorMap;
    private final String username;

    public NewUpdateWithError(AdminEventUpdateViewModel saveEvent, ErrorMap errorMap, String username) {
        this.saveEvent = saveEvent;
        this.errorMap = errorMap;
        this.username = username;
    }

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public ModelAndView update() {
        logger.error("Admin event can not be updated {}", errorMap);
        Event event = AdminEventUpdateConverter.buildEvent(Event.getEmptyEvent(), saveEvent);
        return EventDetailModelViewFactory.createEventDetailPageModelAndViewWithError(event,
                username, errorMap);
    }
}
