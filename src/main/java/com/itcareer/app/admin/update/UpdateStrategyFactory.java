package com.itcareer.app.admin.update;

import com.itcareer.app.admin.viewmodel.AdminEventUpdateViewModel;
import com.itcareer.app.admin.viewmodel.ErrorMap;
import com.itcareer.app.event.EventService;

public class UpdateStrategyFactory {

    private final EventService eventService;

    public UpdateStrategyFactory(EventService eventService) {
        this.eventService = eventService;
    }

    public UpdateStrategy getUpdateStrategy(AdminEventUpdateViewModel saveEvent, ErrorMap errorMap, String username) {
        if (errorMap.hasErrors() && AdminEventUpdateViewModel.isNewEvent(saveEvent.getId())) {
            return new NewUpdateWithError(saveEvent, errorMap, username);
        } else if (AdminEventUpdateViewModel.isNewEvent(saveEvent.getId())) {
            return new NewUpdate(eventService, saveEvent, username);
        } else if (errorMap.hasErrors() && !AdminEventUpdateViewModel.isNewEvent(saveEvent.getId())) {
            return new ExistingUpdateWithError(eventService, saveEvent.getId(), saveEvent, errorMap, username);
        } else {
            return new ExistingUpdate(eventService, saveEvent.getId(), saveEvent, username);
        }
    }
}
