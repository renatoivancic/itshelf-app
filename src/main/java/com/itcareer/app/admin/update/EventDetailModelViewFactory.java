package com.itcareer.app.admin.update;

import com.itcareer.app.admin.viewmodel.AdminEventViewModel;
import com.itcareer.app.admin.viewmodel.ErrorMap;
import com.itcareer.app.domain.Event;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

public class EventDetailModelViewFactory {

    private EventDetailModelViewFactory(){}

    public static final String ADMIN_EVENT_DETAIL_VIEW = "admin/adminEvent";
    public static final String ADMIN_EVENT_UPDATE_VIEW_MODEL_KEY = "adminEventUpdateViewModel";
    public static final String ADMIN_EVENT_VIEW_MODEL_KEY = "adminEventViewModel";
    public static final String ADMIN_EVENT_ERROR_VIEW_MODEL_KEY = "adminEventErrorViewModel";
    /**
     * Assign view name and all the view models that are necessary for rendering the view itself
     * @param event Event that will be rendered on the event detail page.
     * @param username logged in username
     * @return ViewModel for the event detail page
     */
    public static ModelAndView createEventDetailPageModelAndView(Event event, String username) {
        return new ModelAndView(ADMIN_EVENT_DETAIL_VIEW, Map.of(
                ADMIN_EVENT_VIEW_MODEL_KEY, new AdminEventViewModel(event, username),
                ADMIN_EVENT_UPDATE_VIEW_MODEL_KEY, AdminEventUpdateConverter.buildUpdateAdminEvent(event))
        );
    }

    /**
     * Addition to creating standard ViewModel for event detail page also check for any errors and append them to the result
     */
    public static ModelAndView createEventDetailPageModelAndViewWithError(Event event, String username, ErrorMap errorMap) {
        ModelAndView eventDetailPageModelAndView = createEventDetailPageModelAndView(event, username);
        return eventDetailPageModelAndView.addObject(ADMIN_EVENT_ERROR_VIEW_MODEL_KEY,errorMap);
    }
}
