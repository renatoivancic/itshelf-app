package com.itcareer.app.admin.update;

import com.itcareer.app.admin.viewmodel.AdminEventUpdateViewModel;
import com.itcareer.app.admin.viewmodel.ErrorMap;
import com.itcareer.app.domain.Event;
import com.itcareer.app.event.EventService;
import org.springframework.lang.NonNull;
import org.springframework.web.servlet.ModelAndView;

public class ExistingUpdateWithError implements UpdateStrategy {

    private final EventService eventService;
    private final String id;
    private final AdminEventUpdateViewModel saveEvent;
    private final ErrorMap errorMap;
    private final String user;

    public ExistingUpdateWithError(EventService eventService, String id, AdminEventUpdateViewModel saveEvent, @NonNull ErrorMap errorMap, String user) {
        this.eventService = eventService;
        this.id = id;
        this.saveEvent = saveEvent;
        this.errorMap = errorMap;
        this.user = user;
    }

    @Override
    public ModelAndView update() {
        Event event = eventService.findEventById(Long.parseLong(id));
        event = AdminEventUpdateConverter.buildEvent(event, saveEvent);
        return EventDetailModelViewFactory.createEventDetailPageModelAndViewWithError(event, user, errorMap);
    }
}
