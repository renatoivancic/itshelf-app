package com.itcareer.app.admin.update;

import com.itcareer.app.admin.enrich.DateTimeHelper;
import com.itcareer.app.admin.viewmodel.AdminEventUpdateViewModel;
import com.itcareer.app.domain.Event;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Aggregates and enriches raw data from backend for admin presentation.
 */
@Service
public class AdminEventUpdateConverter {

    private AdminEventUpdateConverter() {}

    /**
     * In keywords remove spaces and empty keywords between commas or excessive commas.
     */
    public static Event buildEvent(Event event, AdminEventUpdateViewModel saveEvent) {

        String cleanKeywords = Arrays.stream(saveEvent.getKeywords().split(","))
                .map(String::trim)
                .filter(keyword -> !StringUtils.isEmpty(keyword))
                .collect(Collectors.joining(","));

        ZonedDateTime timeFrom = null;
        if(!saveEvent.getDateTimeFrom().isEmpty()) {
            timeFrom = ZonedDateTime.of(LocalDateTime.parse(saveEvent.getDateTimeFrom()), ZoneId.of(saveEvent.getTimezoneOffsetFrom()));
        }
        ZonedDateTime timeTo = null;

        if(saveEvent.isDateTimeToFlag()) {
            timeTo = ZonedDateTime.of(LocalDateTime.parse(saveEvent.getDateTimeTo()), ZoneId.of(saveEvent.getTimezoneOffsetTo()));
        }

        return new Event(event.getId()
                ,saveEvent.getName(),
                saveEvent.getExternalURL(),
                timeFrom,
                timeTo,
                saveEvent.getType(),
                saveEvent.getCity(),
                saveEvent.getCountry(),
                saveEvent.getImage(),
                saveEvent.isFree(),
                saveEvent.getPriceFrom(),
                saveEvent.getPriceTo(),
                saveEvent.getDescription(),
                saveEvent.getVendorEmail(),
                saveEvent.getVendorName(),
                saveEvent.getVendorLink(),
                saveEvent.getTwitterLink(),
                saveEvent.getStatus(),
                saveEvent.getFacebookLink(),
                saveEvent.getYoutubeLink(),
                saveEvent.getLinkedinLink(),
                cleanKeywords,
                saveEvent.getAddress(),
                saveEvent.getSlug());
    }

    public static  AdminEventUpdateViewModel buildUpdateAdminEvent(Event event) {
        String id;
        if(event.getId() == null || event.getId() == -1) {
            id = "new";
        } else {
            id = event.getId().toString();
        }
        return new AdminEventUpdateViewModel(id,
                event.getName(),
                event.getDescription(),
                event.getImage(),
                event.getKeywords(),
                event.getStatus(),
                DateTimeHelper.getDateTimeAsText(event.getDateFrom()),
                DateTimeHelper.getDateTimeAsText(event.getDateTo()),
                event.getDateTo() != null,
                DateTimeHelper.getOffsetAsText(event.getDateFrom()),
                DateTimeHelper.getOffsetAsText(event.getDateTo()),
                event.getUrl(),
                event.getSlug(),
                event.getType(),
                event.isFree(),
                event.getPriceFrom(),
                event.getPriceTo(),
                event.getCity(),
                event.getCountry(),
                event.getLocationAddress(),
                event.getVendorName(),
                event.getVendorLink(),
                event.getContactInfoEmail(),
                event.getTwitterLink(),
                event.getFacebookLink(),
                event.getYoutubeLink(),
                event.getLinkedinLink()
        );
    }
}
