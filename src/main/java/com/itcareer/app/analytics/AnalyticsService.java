package com.itcareer.app.analytics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.lang.invoke.MethodHandles;

/**
 * https://spring.io/guides/gs/relational-data-access/
 * <p>
 * https://docs.spring.io/spring-data/jdbc/docs/2.0.0.RELEASE/reference/html/#reference
 * <p>
 * https://lumberjackdev.com/spring-data-jdbc
 * <p>
 * Local database
 * <p>
 * docker exec -it postgres1 /bin/bash
 * <p>
 * search_term table
 * <p>
 * 0.0.0.0:5432
 * <p>
 * username: postgres
 *
 */
@Service
public class AnalyticsService {

    private final SearchEventRepository searchEventRepository;

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public AnalyticsService(SearchEventRepository searchEventRepository) {
        this.searchEventRepository = searchEventRepository;
    }

    /**
     * Async persist search event for analytics purposes.
     * Fire and forget.
     *
     * As this process is not process critical if it fails do handle it gracefully and log the error.
     *
     * @param searchTerm search term that will be persisted together with the timestamp
     */
    public void persistSearchEvent(final String searchTerm) {
        try {
            new Thread(() -> {
                SearchEvent searchEvent = new SearchEvent(searchTerm);
                searchEventRepository.save(searchEvent);
            }).start();
        } catch (Exception exception) {
            logger.error("Could not persist SearchEvent to DB, cause: {}", exception.getMessage(), exception);
        }
    }
}
