package com.itcareer.app.analytics;

import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

public class SearchEvent {
    @Id
    private long id;
    private LocalDateTime timestamp;
    private String searchTerm;

    public SearchEvent(String searchTerm) {
        this.searchTerm = searchTerm;
        this.timestamp = LocalDateTime.now();
    }

    @Override
    public String toString() {
        return "SearchEvent{" +
                "id=" + id +
                ", timestamp=" + timestamp +
                ", searchTerm='" + searchTerm + '\'' +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getTimeStamp() {
        return timestamp;
    }

    public void setTimeStamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }
}
