package com.itcareer.app.analytics;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchEventRepository extends CrudRepository<SearchEvent, Long> {
}
