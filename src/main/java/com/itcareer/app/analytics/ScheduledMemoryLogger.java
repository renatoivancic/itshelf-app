package com.itcareer.app.analytics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;

/**
 * Logging total and free memory of JVM.
 */
@Component
public class ScheduledMemoryLogger {

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Scheduled(fixedDelayString = "${com.itshelf.memory.logger.delay}")
    public void scheduledMemoryLogger() {
        logMemoryConsumption();
    }

    /**
     * Method for testing purposes that logs memory usage of the application.
     *
     * Suppress runtime.gc() warning as memory consumption is intentionally printed in logging, instead of relying on
     * monitoring with Grafana or similar as there isn't any infrastructure set up for it.
     */
    @SuppressWarnings("java:S1215")
    private void logMemoryConsumption() {

        long start = System.currentTimeMillis();
        // Get the Java runtime
        Runtime runtime = Runtime.getRuntime();
        // Run the garbage collector
        runtime.gc();
        // Calculate the used memory
        long memory = runtime.totalMemory() - runtime.freeMemory();
        logger.debug("Total memory: {}",
                bytesToMegabytes(runtime.totalMemory()));
        logger.debug("Max memory: {}",
                bytesToMegabytes(runtime.maxMemory()));
        logger.debug("Free memory: {}",
                bytesToMegabytes(runtime.freeMemory()));
        logger.debug("Used memory is bytes: {}", memory);
        logger.debug("Used memory is megabytes: {}",
                bytesToMegabytes(memory));

        long end = System.currentTimeMillis();
        logger.debug("Used memory calculation took {} milliseconds / {} seconds", end-start, (end-start)/1000);
    }

    private static final long MEGABYTE = 1024L * 1024L;

    public static long bytesToMegabytes(long bytes) {
        return bytes / MEGABYTE;
    }
}
