package com.itcareer.app.seo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itcareer.app.seo.model.EventSchemaOrgPojo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;

public class SeoService {

    private SeoService() {}

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    public static String generateSchemaOrgFor(EventSchemaOrgPojo eventSchemaOrgPojo) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(eventSchemaOrgPojo);
        } catch (JsonProcessingException e) {
            logger.error("Exception while generating event schema.org. Maintainer has to resolve it.", e);
            return "";
        }
    }
}
