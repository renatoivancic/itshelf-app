package com.itcareer.app.seo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EventStatusType {
    @JsonProperty("EventCancelled")
    EVENT_CANCELLED,
    @JsonProperty("EventMovedOnline")
    EVENT_MOVED_ONLINE,
    @JsonProperty("EventPostponed")
    EVENT_POSTPONED,
    @JsonProperty("EventRescheduled")
    EVENT_RESCHEDULED,
    @JsonProperty("EventScheduled")
    EVENT_SCHEDULED
}





