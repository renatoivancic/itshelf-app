package com.itcareer.app.seo.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Offer {
    @JsonProperty("@type")
    private static final String TYPE = "Offer";
    @JsonProperty("price")
    private final String price;
    @JsonProperty("priceCurrency")
    private final String priceCurrency;

    public Offer(String price, String priceCurrency) {
        this.price = price;
        this.priceCurrency = priceCurrency;
    }

    public static String getTYPE() {
        return TYPE;
    }

    public String getPrice() {
        return price;
    }

    public String getPriceCurrency() {
        return priceCurrency;
    }
}
