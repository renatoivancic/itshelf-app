package com.itcareer.app.seo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Place {

    @JsonProperty("@type")
    final String type = "Place";  // NOSONAR need to be serialized for schema.org JSON
    @JsonProperty("address")
    final String address;

    public Place(String address) {
        this.address = address;
    }
}
