package com.itcareer.app.seo.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EventAttendanceMode {
    @JsonProperty("OnlineEventAttendanceMode")
    ONLINE_EVENT_ATTENDANCE_MODE,
    @JsonProperty("OfflineEventAttendanceMode")
    OFFLINE_EVENT_ATTENDANCE_MODE,
    @JsonProperty("MixedEventAttendanceMode")
    MIXED_EVENT_ATTENDANCE_MODE
}
