package com.itcareer.app.seo.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Organization {
    @JsonProperty("@type")
    final String type = "Organization"; // NOSONAR need to be serialized for schema.org JSON
    @JsonProperty("email")
    final String email;
    @JsonProperty("name")
    final String name;
    @JsonProperty("url")
    final String url;

    public Organization(String email, String name, String url) {
        this.email = email;
        this.name = name;
        this.url = url;
    }
}
