package com.itcareer.app.seo.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * POJO class for generating event Shema.org JSON-LD markup for SERP.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EventSchemaOrgPojo {

    @JsonProperty("@context")
    private static final String CONTEXT = "http://schema.org";
    @JsonProperty("@type")
    private static final String TYPE = "Event";
    @JsonProperty("name")
    private final String name;
    @JsonProperty("startDate")
    private final String startDate;
    @JsonProperty("endDate")
    private final String endDate;
    @JsonProperty("image")
    private final String image;
    @JsonProperty("description")
    private final String description;
    @JsonProperty("url")
    private final String url;
    @JsonProperty("eventAttendanceMode")
    private final EventAttendanceMode eventAttendanceMode;
    @JsonProperty("eventStatus")
    private final EventStatusType eventStatusType;
    @JsonProperty("offers")
    private final Offer offer;
    @JsonProperty("organizer")
    private final Organization organization;
    @JsonProperty("location")
    private final Place place;

    public EventSchemaOrgPojo(String name, String startDate, String endDate, // NOSONAR there are so many arguments as there are fields
                              String image, String description,
                              String url, EventAttendanceMode eventAttendanceMode,
                              EventStatusType eventStatusType, Offer offer,
                              Organization organization, Place place) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.image = image;
        this.description = description;
        this.url = url;
        this.eventAttendanceMode = eventAttendanceMode;
        this.eventStatusType = eventStatusType;
        this.offer = offer;
        this.organization = organization;
        this.place = place;
    }

    @JsonProperty("@context")
    public String getContext() {
        return CONTEXT;
    }

    @JsonProperty("@type")
    public String getType() {
        return TYPE;
    }

    public String getName() {
        return name;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getImage() {
        return image;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public EventAttendanceMode getEventAttendanceMode() {
        return eventAttendanceMode;
    }

    public EventStatusType getEventStatusType() {
        return eventStatusType;
    }

    public Offer getOffer() {
        return offer;
    }

    public Organization getOrganization() {
        return organization;
    }

    public Place getPlace() {
        return place;
    }
}
