package com.itcareer.app.sitemap;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.List;

@JacksonXmlRootElement(localName = "urlset")
public class SitemapUrlSet {

    @JacksonXmlProperty(isAttribute = true)
    private String xmlns = "http://www.sitemaps.org/schemas/sitemap/0.9";

    @JacksonXmlElementWrapper(useWrapping = false)
    private final List<SitemapUrl> url;

    SitemapUrlSet(List<SitemapUrl> urls) {
        this.url = urls;
    }

    public List<SitemapUrl> getUrl() {
        return url;
    }
}
