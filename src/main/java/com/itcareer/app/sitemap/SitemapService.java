package com.itcareer.app.sitemap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.itcareer.app.domain.Event;
import com.itcareer.app.event.EventService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import java.lang.invoke.MethodHandles;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Return dynamically generated sitemap that contains all of the upcoming events links.
 */
@Service
public class SitemapService {

    public static final String HTTPS_ITSHELF_COM_EVENTS_URL = "https://itshelf.com/events/";
    public static final String EVENT_SITEMAP_PRIORITY = "1.0";
    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static final String XML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ISO_DATE;
    private final EventService eventService;

    public SitemapService(EventService eventService) {
        this.eventService = eventService;
    }

    public @ResponseBody
    String getEventsSitemap() throws JsonProcessingException {

        // Get all upcoming events
        List<Event> upcomingEvents = eventService.findEventBy("");
        List<SitemapUrl> urls = convertToSitemapUrls(upcomingEvents);
        String sitemapXml = serializeSitemapToXml(urls);
        logger.info("Generated XML: {}", sitemapXml);
        return sitemapXml;
    }

    private String serializeSitemapToXml(List<SitemapUrl> urls) throws JsonProcessingException {
        return XML_HEADER + new XmlMapper().writeValueAsString(new SitemapUrlSet(urls));
    }

    private List<SitemapUrl> convertToSitemapUrls(List<Event> upcomingEvents) {
        String formattedDateOfToday = DATE_TIME_FORMATTER.format(LocalDateTime.of(LocalDate.now(), LocalTime.of(0, 0, 0)));
        return upcomingEvents.stream().map(event ->
                new SitemapUrl(HTTPS_ITSHELF_COM_EVENTS_URL + event.getSlug(), formattedDateOfToday, EVENT_SITEMAP_PRIORITY)
        ).collect(Collectors.toList());
    }
}
