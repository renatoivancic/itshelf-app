package com.itcareer.app.sitemap;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Controller that is responsible to generate sitemaps.
 * Currently only event sitemap is generated dynamically.
 */
@Controller
public class SitemapController {

    private final SitemapService sitemapService;

    public SitemapController(SitemapService sitemapService) {
        this.sitemapService = sitemapService;
    }

    @GetMapping(value="/sitemap-events.xml", produces=MediaType.APPLICATION_XML_VALUE)
    public @ResponseBody String getEventsSitemap() throws JsonProcessingException {

        return sitemapService.getEventsSitemap();
    }
}
