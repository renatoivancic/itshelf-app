package com.itcareer.app.sitemap;

public class SitemapUrl {
    private final String loc;
    private final String lastmod;
    private final String priority;

    SitemapUrl(String loc, String lastmod, String priority) {
        this.loc = loc;
        this.lastmod = lastmod;
        this.priority = priority;
    }

    public String getPriority() {
        return priority;
    }

    public String getLastmod() {
        return lastmod;
    }

    public String getLoc() {
        return loc;
    }
}
