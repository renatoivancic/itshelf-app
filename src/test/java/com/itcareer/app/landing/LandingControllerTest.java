package com.itcareer.app.landing;

import com.itcareer.app.core.security.WebSecurityConfig;
import com.itcareer.app.event.EventService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(LandingController.class)
@Import(WebSecurityConfig.class)
public class LandingControllerTest {

    @MockBean
    RestTemplate restTemplate;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    EventService eventService;

    @Test
    public void testEmptyRequest( ) throws Exception {

        // when
        this.mockMvc
                .perform(get("/"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testRequest( ) throws Exception {

        // when
        MvcResult mvcResult = this.mockMvc
                .perform(get("/"))
                .andDo(print())
                .andExpect(status().isOk()).andReturn();

        // then
        // test the model and view do not test how the response is being rendered.
        // for this the UI tests have to be implemented.
        Object landingResultObject = Objects.requireNonNull(mvcResult.getModelAndView()).getModel().get("landingModel");
        Assertions.assertTrue(landingResultObject instanceof LandingResult);
        LandingResult landingResult = (LandingResult) landingResultObject;
        Assertions.assertEquals("/img/logo/itshelf-logo.png", landingResult.getPathToLogo());
        Assertions.assertEquals("/img/logo/itshelf-logo-sign.png", landingResult.getPathToLogoSign());
        Assertions.assertEquals(55, landingResult.getNumberOfUpcomingEvents());
    }
}
