package com.itcareer.app.sitemap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.itcareer.app.domain.Event;
import com.itcareer.app.event.EventService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class SitemapServiceTest {

    @Mock
    EventService eventService;

    /**
     * Test event dynamic sitemap generation
     */
    @Test
    public void testDynamicEventSitemapGeneration() throws JsonProcessingException {

        // given
        List<Event> listOfEvents = new ArrayList<>();

        Event firstEvent = new Event(4L,
                "AWS Summit Online EMEA",
                "https://aws.amazon.com/events/summits/online/emea/",
                ZonedDateTime.of(2020,6,17,0,0,0,0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                null,
"Online Conference",
                null,
                null,
                "https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png",
                true,
                null,
                null,
                "Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                "AWS,Cloud,DevOps,InfoSec,Data Warehouses,AI/ML,Machine Learning,CI/CD,Kubernetes,GitOps,Applications,REST,GraphQL,Kafka,Startup",
                "",
                "aws-online-summit-emea"
        );

        Event secondEvent = new Event(5L,
                "ProductCon",
                "https://productschool.com/productcon/seattle/",
                ZonedDateTime.of(2020,7,9,0,0,0,0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                null,
                "Online Conference",
                null,
                null,
                "something.jpg",
                true,
                null,
                null,
                "ProductCon description ",
                "",
                "Product School",
                "",
                "",
                "",
                "",
                "",
                "",
                "AWS,Cloud,DevOps,InfoSec,Data Warehouses,AI/ML,Machine Learning,CI/CD,Kubernetes,GitOps,Applications,REST,GraphQL,Kafka,Startup",
                "",
                "aws-online-summit-emea"
        );

        listOfEvents.add(firstEvent);
        listOfEvents.add(secondEvent);

        Mockito.when(eventService.findEventBy("")).thenReturn(listOfEvents);
        SitemapService sitemapService = new SitemapService(eventService);

        // when
        String eventsSitemap = sitemapService.getEventsSitemap();

        // then
        Assertions.assertNotNull(eventsSitemap);
        String date = LocalDate.now().format(DateTimeFormatter.ofPattern("uuuu-MM-dd"));
        Assertions.assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?><urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"><url><loc>https://itshelf.com/events/aws-online-summit-emea</loc><lastmod>"+date+"</lastmod><priority>1.0</priority></url><url><loc>https://itshelf.com/events/aws-online-summit-emea</loc><lastmod>"+date+"</lastmod><priority>1.0</priority></url></urlset>", eventsSitemap);
    }
}
