package com.itcareer.app.sitemap;

import com.itcareer.app.core.security.WebSecurityConfig;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.client.RestTemplate;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(SitemapController.class)
@Import(WebSecurityConfig.class)
public class SitemapControllerTest {

    @MockBean
    RestTemplate restTemplate;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SitemapService sitemapService;

    @Test
    public void testEmptyRequest( ) throws Exception {

        // when
        this.mockMvc
                .perform(get("/sitemap-events.xml"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * Mocked Service returns one 2 events in a sitemap.
     */
    @Test
    public void test2EventsForSitemap( ) throws Exception {

        // given
        when(sitemapService.getEventsSitemap()).thenReturn("<?xml version=\"1.0\" encoding=\"UTF-8\"?><urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"><url><loc>https://itshelf.com/events/aws-online-summit-emea</loc><lastmod>2020-07-06</lastmod><priority>1.0</priority></url><url><loc>https://itshelf.com/events/aws-online-summit-emea</loc><lastmod>2020-07-06</lastmod><priority>1.0</priority></url></urlset>");

        // when
        MvcResult mvcResult = this.mockMvc
                .perform(get("/sitemap-events.xml"))
                .andDo(print())
                .andExpect(status().isOk()).andReturn();

        // then
        Assertions.assertNotNull(mvcResult.getResponse().getContentAsString());
        Assertions.assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?><urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"><url><loc>https://itshelf.com/events/aws-online-summit-emea</loc><lastmod>2020-07-06</lastmod><priority>1.0</priority></url><url><loc>https://itshelf.com/events/aws-online-summit-emea</loc><lastmod>2020-07-06</lastmod><priority>1.0</priority></url></urlset>", mvcResult.getResponse().getContentAsString());
    }
}
