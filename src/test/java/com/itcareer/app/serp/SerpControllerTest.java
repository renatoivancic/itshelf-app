package com.itcareer.app.serp;

import com.itcareer.app.analytics.AnalyticsService;
import com.itcareer.app.core.search.SearchService;
import com.itcareer.app.core.security.WebSecurityConfig;
import com.itcareer.app.domain.Course;
import com.itcareer.app.domain.Vendor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(SerpController.class)
@Import(WebSecurityConfig.class)
public class SerpControllerTest {

    @MockBean
    private SearchService searchService;

    @MockBean
    private AnalyticsService analyticsService;

    @MockBean
    private SearchTermSanitizer searchTermSanitizer;

    @MockBean
    RestTemplate restTemplate;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testEmptyRequest( ) throws Exception {

        // when
        this.mockMvc
                .perform(get("/search"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * Mocked Service returns one course
     */
    @Test
    public void testJavaRequest( ) throws Exception {

        // given
        List<Course> courses = new ArrayList<>();
        Course course = new Course(
                "Java Course",
                new Vendor("ITShelf","ITShelf logo url", "ITShelf url"),
                "Course Image Url",
                "Course URL",
                "headline1",
                "author");
        courses.add(course);
        when(searchTermSanitizer.sanitizeInput("java")).thenReturn("java");
        when(searchService.findCoursesBy("java")).thenReturn(courses);

        // when
        MvcResult mvcResult = this.mockMvc
                .perform(get("/search?searchTerm=java"))
                .andDo(print())
                .andExpect(status().isOk()).andReturn();

        // then
        // test the model and view do not test how the response is being rendered.
        // for this the UI tests have to be implemented.
        Object searchResults = Objects.requireNonNull(mvcResult.getModelAndView()).getModel().get("searchResults");
        Assertions.assertTrue(searchResults instanceof SearchResult);
        SearchResult searchResult = (SearchResult) searchResults;
        Assertions.assertEquals(courses, searchResult.getCourses());
        Assertions.assertEquals("java", searchResult.getSearchTerm());
        Assertions.assertEquals("/img/logo/itshelf-logo.png", searchResult.getPathToLogo());
        Assertions.assertEquals("headline1", searchResult.getCourses().get(0).getHeadline());
        Assertions.assertEquals("author", searchResult.getCourses().get(0).getAuthor());
        verify(analyticsService, times(1)).persistSearchEvent("java");
    }
}
