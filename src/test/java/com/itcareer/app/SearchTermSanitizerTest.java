package com.itcareer.app;

import com.itcareer.app.serp.SearchTermSanitizer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.stream.Stream;

/**
 * Test that if filterXss flag is one that the input will be OWASP sanitized.
 *
 * Pentest done with Kali / xsser
 */
@DisplayName("Search term sanitizer test with XSS filtering and without")
public class SearchTermSanitizerTest {

    @DisplayName("Should calculate the correct sum")
    @ParameterizedTest(name = "With XSS filter enabled: {0}, Type of injection: {3}")
    @MethodSource("testSearchTermProvider")
    public void testPolyglotInjectionFilterDisabled(boolean filterXss, String searchTerm, String sanitizedSearchTerm, String name) {

        // given
        SearchTermSanitizer searchTermSanitizer = new SearchTermSanitizer();
        ReflectionTestUtils.setField(searchTermSanitizer, "filterXss", filterXss);

        // when
        String result = searchTermSanitizer.sanitizeInput(searchTerm);

        // then
        Assertions.assertNotNull(result);
        Assertions.assertEquals(sanitizedSearchTerm, result);
    }

    private static Stream<Arguments> testSearchTermProvider() {
        return Stream.of(
                // Without sanitation
                Arguments.of(Boolean.FALSE,
                        "javascript:/*--></title></style></textarea></script></xmp><svg/onload='+/\"/+/onmouseover=1/+/[*/[]/+alert(1)//'>",
                        "javascript:/*--></title></style></textarea></script></xmp><svg/onload='+/\"/+/onmouseover=1/+/[*/[]/+alert(1)//'>",
                        "Polyglot Injection"),
                Arguments.of(Boolean.FALSE,
                        "<IMG SRC=\"javascript:alert('XSS');\">",
                        "<IMG SRC=\"javascript:alert('XSS');\">",
                        "Image Injection"),
                Arguments.of(Boolean.FALSE,
                        "java security",
                        "java security",
                        "Normal search"),
                Arguments.of(Boolean.FALSE,
                        null,
                        "",
                        "null search"),

                Arguments.of(Boolean.TRUE,
                        "javascript:/*--></title></style></textarea></script></xmp><svg/onload='+/\"/+/onmouseover=1/+/[*/[]/+alert(1)//'>",
                        "javascript:/*--&gt;",
                        "Polyglot Injection"),
                Arguments.of(Boolean.TRUE,
                        "<IMG SRC=\"javascript:alert('XSS');\">",
                        "",
                        "Image Injection"),
                Arguments.of(Boolean.TRUE,
                        "java security",
                        "java security",
                        "Normal search"),
                Arguments.of(Boolean.TRUE,
                        null,
                        "",
                        "null search")
        );
    }
}
