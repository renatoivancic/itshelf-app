package com.itcareer.app.admin;

import com.itcareer.app.admin.viewmodel.*;
import com.itcareer.app.admin.viewmodel.AdminEventListViewModel;
import com.itcareer.app.core.markdown.MarkdownProcessor;
import com.itcareer.app.core.security.WebSecurityConfig;
import com.itcareer.app.domain.Event;
import com.itcareer.app.event.EventService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.client.RestTemplate;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AdminController.class)
@Import(WebSecurityConfig.class)
public class AdminControllerTest {

    @MockBean
    RestTemplate restTemplate;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EventService eventService;

    @TestConfiguration
    static class TestConfig {

        @Bean
        public MarkdownProcessor markdownProcessorBean() {
            return new MarkdownProcessor();
        }
    }

    @Test
    void testEmptyRequestNotAuthorized( ) throws Exception {

        // when
        this.mockMvc
                .perform(get("/admin"))
                .andDo(print())
                .andExpect(status().is(302));
    }

    @Test
    void testEmptyRequestAuthorized( ) throws Exception {

        Event event = new Event(11L,
                "AWS Java",
                "https://aws.amazon.com/events/summits/online/emea/",
                ZonedDateTime.of(2020,6,17,0,0,0,0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                null,
                "Online Conference",
                null,
                null,
                "https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png",
                true,
                null,
                null,
                "Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                "AWS,Cloud,DevOps,Java,Data Warehouses,AI/ML,Machine Learning,CI/CD,Kubernetes,GitOps,Applications,REST,GraphQL,Kafka,Startup",
                "",
                "aws-java"
        );

       //  List<AdminEvent> events = Collections.singletonList(new AdminEvent(event, 200, true));

        Mockito.when(eventService.getAllEvents()).thenReturn(Collections.singletonList(event));

        // when
        MvcResult mvcResult =this.mockMvc
                .perform(get("/admin")
                        .with(user("admin")
                                .password("test1234")
                                .roles("ADMIN")))
                .andDo(print())
                .andExpect(status().is(200)).andReturn();


        // then
        // test the model and view do not test how the response is being rendered.
        // for this the UI tests have to be implemented.
        Object adminResults = Objects.requireNonNull(mvcResult.getModelAndView()).getModel().get("adminEventListViewModel");
        Assertions.assertTrue(adminResults instanceof AdminEventListViewModel);

        AdminEventListViewModel adminEventListResultViewModel = (AdminEventListViewModel) adminResults;
        Assertions.assertEquals(1, adminEventListResultViewModel.getEvents().size());
        Assertions.assertEquals(50, adminEventListResultViewModel.getEvents().get(0).getNumberOfWords());
        Assertions.assertEquals(11, adminEventListResultViewModel.getEvents().get(0).getEvent().getId());
        Assertions.assertEquals("admin", adminEventListResultViewModel.getUsername());
        Assertions.assertEquals(0, adminEventListResultViewModel.getEventQualityStats().getBad());
        Assertions.assertEquals(0, adminEventListResultViewModel.getEventQualityStats().getGood());
        Assertions.assertEquals(1, adminEventListResultViewModel.getEventQualityStats().getNumberOfAllEvents());
        Assertions.assertEquals(0, adminEventListResultViewModel.getEventQualityStats().getNumberOfUpcomingEvents());
    }

    @Test
    void getEventDetailPage() throws Exception {
        Event event = new Event(11L,
                "AWS Java",
                "https://aws.amazon.com/events/summits/online/emea/",
                ZonedDateTime.of(2020,6,17,0,0,0,0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                null,
                "Online Conference",
                null,
                null,
                "https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png",
                true,
                null,
                null,
                "Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                "AWS,Cloud,DevOps,Java,Data Warehouses,AI/ML,Machine Learning,CI/CD,Kubernetes,GitOps,Applications,REST,GraphQL,Kafka,Startup",
                "",
                "aws-java"
        );

        Mockito.when(eventService.findEventById(11L)).thenReturn(event);

        // when
        MvcResult mvcResult =this.mockMvc
                .perform(get("/admin/events/11")
                        .with(user("admin")
                                .password("test1234")
                                .roles("ADMIN")))
                .andDo(print())
                .andExpect(status().is(200)).andReturn();


        // then
        // test the model and view do not test how the response is being rendered.
        // for this the UI tests have to be implemented.
        Object adminEventResults = Objects.requireNonNull(mvcResult.getModelAndView()).getModel().get("adminEventViewModel");

        Assertions.assertTrue(adminEventResults instanceof AdminEventViewModel);
        AdminEventViewModel adminEventViewModel = (AdminEventViewModel) adminEventResults;

        Assertions.assertEquals("admin", adminEventViewModel.getUsername());
        Assertions.assertEquals(11L, adminEventViewModel.getEvent().getId());
        Assertions.assertEquals("AWS Java", adminEventViewModel.getEvent().getName());

        Object adminEventDTO= Objects.requireNonNull(mvcResult.getModelAndView()).getModel().get("adminEventUpdateViewModel");
        Assertions.assertTrue(adminEventDTO instanceof AdminEventUpdateViewModel);
        AdminEventUpdateViewModel adminEventUpdateViewModelInstance = (AdminEventUpdateViewModel) adminEventDTO;
        Assertions.assertEquals("AWS Java", adminEventUpdateViewModelInstance.getName());
        Assertions.assertEquals("Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ", adminEventUpdateViewModelInstance.getDescription());
    }

    @Test
    void getNewEventDetailPage() throws Exception {

        // when
        MvcResult mvcResult =this.mockMvc
                .perform(get("/admin/events/new")
                        .with(user("admin")
                                .password("test1234")
                                .roles("ADMIN")))
                .andDo(print())
                .andExpect(status().is(200)).andReturn();


        // then
        // test the model and view do not test how the response is being rendered.
        // for this the UI tests have to be implemented.
        Object adminEventResults = Objects.requireNonNull(mvcResult.getModelAndView()).getModel().get("adminEventViewModel");

        Assertions.assertTrue(adminEventResults instanceof AdminEventViewModel);
        AdminEventViewModel adminEventViewModel = (AdminEventViewModel) adminEventResults;

        Assertions.assertEquals("admin", adminEventViewModel.getUsername());
        Assertions.assertNull(adminEventViewModel.getEvent().getId());
        Assertions.assertEquals("", adminEventViewModel.getEvent().getName());

        Object adminEventDTO= Objects.requireNonNull(mvcResult.getModelAndView()).getModel().get("adminEventUpdateViewModel");
        Assertions.assertTrue(adminEventDTO instanceof AdminEventUpdateViewModel);
        AdminEventUpdateViewModel adminEventUpdateViewModelInstance = (AdminEventUpdateViewModel) adminEventDTO;
        Assertions.assertEquals("", adminEventUpdateViewModelInstance.getName());
        Assertions.assertEquals("", adminEventUpdateViewModelInstance.getDescription());
    }

    @Test
    void saveEvent() throws Exception {
        Event event = new Event(11L,
                "AWS Java1",
                "https://events.itrevolution.com/virtual/",
                ZonedDateTime.of(2020,6,17,0,0,0,0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                ZonedDateTime.of(2020,6,17,0,0,0,0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                "Online Conference",
                null,
                null,
                "image",
                true,
                null,
                null,
                "Description of an event",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                "DDD, Software Architecture, TDD",
                "",
                "new-aws-java"
        );
        Event savedEvent = new Event(11L,
                "AWS Java1",
                "https://events.itrevolution.com/virtual/",
                ZonedDateTime.of(2020,6,23,2,0,0,0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                ZonedDateTime.of(2020,6,24,6,0,0,0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                "Online Conference",
                null,
                null,
                "image",
                true,
                null,
                null,
                "Description of an event",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                "DDD,Software Architecture,TDD",
                "",
                "new-aws-java"
        );
        Mockito.when(eventService.findEventById(11L)).thenReturn(event);
        Mockito.when(eventService.saveEvent(any())).thenReturn(savedEvent);

        // when
        MvcResult mvcResult =this.mockMvc
                .perform(post("/admin/events/11")
                        .param("name", "AWS Java1")
                        .param("description","Description of an event")
                        .param("image","image")
                        .param("keywords", "DDD, Software Architecture, TDD")
                        .param("externalURL", "https://events.itrevolution.com/virtual/")
                        .param("dateTimeFrom", "2020-06-23T02:00:00.000")
                        .param("dateTimeTo", "2020-06-24T06:00:00.000")
                        .param("timezoneOffsetFrom", "+02:00")
                        .param("timezoneOffsetTo", "+03:00")
                        .param("isDateTimeToFlag", "true")
                        .param("slug", "new-aws-java")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                        .accept(MediaType.TEXT_HTML, MediaType.APPLICATION_XHTML_XML)
                        .with(user("admin")
                                .password("test1234")
                                .roles("ADMIN")).with(csrf()))
                .andDo(print())
                .andExpect(status().is(200)).andReturn();

        // then
        // test the model and view do not test how the response is being rendered.
        // for this the UI tests have to be implemented.
        Object adminEventResults = Objects.requireNonNull(mvcResult.getModelAndView()).getModel().get("adminEventViewModel");

        Assertions.assertTrue(adminEventResults instanceof AdminEventViewModel);
        AdminEventViewModel adminEventViewModel = (AdminEventViewModel) adminEventResults;

        Assertions.assertEquals("admin", adminEventViewModel.getUsername());
        Assertions.assertEquals(11L, adminEventViewModel.getEvent().getId());
        Assertions.assertEquals("AWS Java1", adminEventViewModel.getEvent().getName());
        Assertions.assertEquals("image", adminEventViewModel.getEvent().getImage());
        Assertions.assertEquals("DDD,Software Architecture,TDD", adminEventViewModel.getEvent().getKeywords());
        Assertions.assertEquals("https://events.itrevolution.com/virtual/", adminEventViewModel.getEvent().getUrl());
        Assertions.assertEquals(ZonedDateTime.of(2020,6,23,2,0,0,0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))), adminEventViewModel.getEvent().getDateFrom());
        Assertions.assertEquals(ZonedDateTime.of(2020,6,24,6,0,0,0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))), adminEventViewModel.getEvent().getDateTo());
        Assertions.assertEquals( "new-aws-java", adminEventViewModel.getEvent().getSlug());

        Object adminEventDTO= Objects.requireNonNull(mvcResult.getModelAndView()).getModel().get("adminEventUpdateViewModel");
        Assertions.assertTrue(adminEventDTO instanceof AdminEventUpdateViewModel);
        AdminEventUpdateViewModel adminEventUpdateViewModelInstance = (AdminEventUpdateViewModel) adminEventDTO;
        Assertions.assertEquals("AWS Java1", adminEventUpdateViewModelInstance.getName());
        Assertions.assertEquals("Description of an event", adminEventUpdateViewModelInstance.getDescription());
        Assertions.assertEquals("image", adminEventUpdateViewModelInstance.getImage());
        Assertions.assertEquals("new-aws-java", adminEventUpdateViewModelInstance.getSlug());
    }
}
