package com.itcareer.app.admin;

import com.itcareer.app.admin.update.AdminEventUpdateConverter;
import com.itcareer.app.admin.viewmodel.AdminEventUpdateViewModel;
import com.itcareer.app.domain.Event;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.ZoneId;
import java.time.ZonedDateTime;

@ExtendWith(MockitoExtension.class)
public class AdminEventUpdateConverterTest {

    @Test
    void convertTest() {

        // given
        Event firstEvent = new Event(11L,
                "AWS Java",
                "https://aws.amazon.com/events/summits/online/emea/",
                ZonedDateTime.of(2020, 6, 17, 0, 0, 0, 0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                null,
                "Online Conference",
                null,
                null,
                "https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png",
                true,
                null,
                null,
                "Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                null,
                "Ulica Vinke Megle 12",
                "aws-java"
        );

        AdminEventUpdateViewModel adminEventUpdateViewModel = new AdminEventUpdateViewModel("11",
                "New name", "New description", "image",
                "DDD, Software Architecture, TDD", "Active","2020-06-23T02:00:00.000", null,
                false, "+02:00", null,
                "https://events.itrevolution.com/virtual/","slug", "Conference",
                true, null, null, "Maribor",
                "Slovenia", null, "AWS","https://aws.com/event","aws@mail.com",
                "twitter-link", "facebook-link", "youtube-link", "linkedin-link");

        // when
        Event persistedEvent = AdminEventUpdateConverter.buildEvent(firstEvent, adminEventUpdateViewModel);

        // then
        Assertions.assertNotNull(persistedEvent);
        Assertions.assertEquals(11, persistedEvent.getId());
        Assertions.assertEquals("New name", persistedEvent.getName());
        Assertions.assertEquals("New description", persistedEvent.getDescription());
        Assertions.assertEquals("Active", persistedEvent.getStatus());
        Assertions.assertEquals("image", persistedEvent.getImage());
        Assertions.assertEquals(ZonedDateTime.of(2020, 6, 23, 2, 0, 0, 0, ZoneId.of("+02:00")), persistedEvent.getDateFrom());
        Assertions.assertEquals("slug", persistedEvent.getSlug());
        Assertions.assertEquals("Conference", persistedEvent.getType());
        Assertions.assertTrue(persistedEvent.isFree());
        Assertions.assertNull(persistedEvent.getPriceFrom());
        Assertions.assertNull(persistedEvent.getPriceTo());
        Assertions.assertEquals("Maribor", persistedEvent.getCity());
        Assertions.assertEquals("Slovenia", persistedEvent.getCountry());
        Assertions.assertNull(persistedEvent.getLocationAddress());
        Assertions.assertEquals("AWS", persistedEvent.getVendorName());
        Assertions.assertEquals("https://aws.com/event", persistedEvent.getVendorLink());
        Assertions.assertEquals("aws@mail.com", persistedEvent.getContactInfoEmail());
        Assertions.assertEquals("twitter-link", persistedEvent.getTwitterLink());
        Assertions.assertEquals("facebook-link", persistedEvent.getFacebookLink());
        Assertions.assertEquals("youtube-link", persistedEvent.getYoutubeLink());
        Assertions.assertEquals("linkedin-link", persistedEvent.getLinkedinLink());
    }

    @Test
    void convertTestWithTimeToWithFlagFalse() {

        // given
        Event firstEvent = new Event(11L,
                "AWS Java",
                "https://aws.amazon.com/events/summits/online/emea/",
                ZonedDateTime.of(2020, 6, 17, 0, 0, 0, 0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                null,
                "Online Conference",
                null,
                null,
                "https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png",
                true,
                null,
                null,
                "Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                null,
                "Ulica Vinke Megle 12",
                "aws-java"
        );

        AdminEventUpdateViewModel adminEventUpdateViewModel = new AdminEventUpdateViewModel("11",
                "New name", "New description", "image",
                "DDD, Software Architecture, TDD", "Canceled","2020-06-23T02:00:00.000", "2020-06-24T06:00:00.000",
                false, "+02:00", "+03:00",
                "https://events.itrevolution.com/virtual/", "slug", "Webinar",
                false, "10 EUR", "20 EUR","Maribor", "Slovenia",
                "Some address","AWS","https://aws.com/event","aws@mail.com",
                "twitter-link", "facebook-link", "youtube-link", "linkedin-link");

        // when
        Event persistedEvent = AdminEventUpdateConverter.buildEvent(firstEvent, adminEventUpdateViewModel);

        // then
        Assertions.assertNotNull(persistedEvent);
        Assertions.assertEquals(11, persistedEvent.getId());
        Assertions.assertEquals("New name", persistedEvent.getName());
        Assertions.assertEquals("New description", persistedEvent.getDescription());
        Assertions.assertEquals("Canceled", persistedEvent.getStatus());
        Assertions.assertEquals("image", persistedEvent.getImage());
        Assertions.assertEquals(ZonedDateTime.of(2020, 6, 23, 2, 0, 0, 0, ZoneId.of("+02:00")), persistedEvent.getDateFrom());
        Assertions.assertNull(persistedEvent.getDateTo());
        Assertions.assertEquals("slug", persistedEvent.getSlug());
        Assertions.assertEquals("Webinar", persistedEvent.getType());
        Assertions.assertFalse(persistedEvent.isFree());
        Assertions.assertEquals("10 EUR",persistedEvent.getPriceFrom());
        Assertions.assertEquals("20 EUR", persistedEvent.getPriceTo());
        Assertions.assertEquals("Maribor", persistedEvent.getCity());
        Assertions.assertEquals("Slovenia", persistedEvent.getCountry());
        Assertions.assertEquals("Some address", persistedEvent.getLocationAddress());
        Assertions.assertEquals("AWS", persistedEvent.getVendorName());
        Assertions.assertEquals("https://aws.com/event", persistedEvent.getVendorLink());
        Assertions.assertEquals("aws@mail.com", persistedEvent.getContactInfoEmail());
        Assertions.assertEquals("twitter-link", persistedEvent.getTwitterLink());
        Assertions.assertEquals("facebook-link", persistedEvent.getFacebookLink());
        Assertions.assertEquals("youtube-link", persistedEvent.getYoutubeLink());
        Assertions.assertEquals("linkedin-link", persistedEvent.getLinkedinLink());
    }

    @Test
    void convertTestWithTimeToWithFlagTrue() {

        // given
        Event firstEvent = new Event(11L,
                "AWS Java",
                "https://aws.amazon.com/events/summits/online/emea/",
                ZonedDateTime.of(2020, 6, 17, 0, 0, 0, 0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                null,
                "Online Conference",
                null,
                null,
                "https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png",
                true,
                null,
                null,
                "Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                null,
                "Ulica Vinke Megle 12",
                "aws-java"
        );

        AdminEventUpdateViewModel adminEventUpdateViewModel = new AdminEventUpdateViewModel("11",
                "New name", "New description", "image",
                "DDD, Software Architecture, TDD", "Inactive","2020-06-23T02:00:00.000", "2020-06-24T06:00:00.000",
                true, "+02:00", "+03:00",
                "https://events.itrevolution.com/virtual/", "slug", "Online Conference",
                false, "10 EUR", "20 EUR",null, null,
                "84 Northyards Blvd NW,\n" +
                        "Atlanta,\n" +
                        "GA 30313", "AWS", null, "",
                "twitter-link", null, "youtube-link", null);

        // when
        Event persistedEvent = AdminEventUpdateConverter.buildEvent(firstEvent, adminEventUpdateViewModel);

        // then
        Assertions.assertNotNull(persistedEvent);
        Assertions.assertEquals(11, persistedEvent.getId());
        Assertions.assertEquals("New name", persistedEvent.getName());
        Assertions.assertEquals("New description", persistedEvent.getDescription());
        Assertions.assertEquals("Inactive", persistedEvent.getStatus());
        Assertions.assertEquals("image", persistedEvent.getImage());
        Assertions.assertEquals(ZonedDateTime.of(2020, 6, 23, 2, 0, 0, 0, ZoneId.of("+02:00")), persistedEvent.getDateFrom());
        Assertions.assertEquals(ZonedDateTime.of(2020, 6, 24, 6, 0, 0, 0, ZoneId.of("+03:00")),persistedEvent.getDateTo());
        Assertions.assertEquals("slug", persistedEvent.getSlug());
        Assertions.assertEquals("84 Northyards Blvd NW,\n" +
                "Atlanta,\n" +
                "GA 30313", persistedEvent.getLocationAddress());
        Assertions.assertEquals("Online Conference", persistedEvent.getType());
        Assertions.assertFalse(persistedEvent.isFree());
        Assertions.assertEquals("10 EUR",persistedEvent.getPriceFrom());
        Assertions.assertEquals("20 EUR", persistedEvent.getPriceTo());
        Assertions.assertNull(persistedEvent.getCity());
        Assertions.assertNull(persistedEvent.getCountry());
        Assertions.assertNull(persistedEvent.getVendorLink());
        Assertions.assertEquals("AWS", persistedEvent.getVendorName());
        Assertions.assertEquals("twitter-link", persistedEvent.getTwitterLink());
        Assertions.assertNull(persistedEvent.getFacebookLink());
        Assertions.assertEquals("youtube-link", persistedEvent.getYoutubeLink());
        Assertions.assertNull( persistedEvent.getLinkedinLink());
    }

    @Test
    void newIdTest() {

        // given
        Event event = new Event(-1L,
                "AWS Java",
                "https://aws.amazon.com/events/summits/online/emea/",
                ZonedDateTime.of(2020, 6, 17, 0, 0, 0, 0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                null,
                "Online Conference",
                null,
                null,
                "https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png",
                true,
                null,
                null,
                "Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                null,
                "Ulica Vinke Megle 12",
                "aws-java"
        );

        // when
        AdminEventUpdateViewModel adminEventUpdateViewModel = AdminEventUpdateConverter.buildUpdateAdminEvent(event);

        // then
        Assertions.assertNotNull(adminEventUpdateViewModel);
        Assertions.assertEquals("new", adminEventUpdateViewModel.getId());
        Assertions.assertEquals("AWS Java", adminEventUpdateViewModel.getName());
        Assertions.assertEquals("Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ", adminEventUpdateViewModel.getDescription());
    }
}
