package com.itcareer.app.admin;

import com.itcareer.app.admin.enrich.DateTimeValidator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DateTimeValidatorTest {

    @Test
    void basicTest() {
        DateTimeValidator dateTimeValidator = new DateTimeValidator();
        Assertions.assertFalse(dateTimeValidator.isValid("", null));
    }

    @Test
    void timeConvertTest() {
        DateTimeValidator dateTimeValidator = new DateTimeValidator();
        Assertions.assertTrue(dateTimeValidator.isValid("2020-09-24T03:27", null));
    }

    @Test
    void falseTimeConvertTest() {
        DateTimeValidator dateTimeValidator = new DateTimeValidator();
        Assertions.assertFalse(dateTimeValidator.isValid("2020-0Tfs4T03:27", null));
    }
}
