package com.itcareer.app.admin.update;

import com.itcareer.app.admin.viewmodel.AdminEventUpdateViewModel;
import com.itcareer.app.admin.viewmodel.ErrorMap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.web.servlet.ModelAndView;

public class NewUpdateWithErrorTest {

    @Test
    void testStrategy() {

        // given
        AdminEventUpdateViewModel adminEventUpdateViewModel = new AdminEventUpdateViewModel("11",
                "New name", "New description", "image",
                "DDD, Software Architecture, TDD", "Active","2020-06-23T02:00:00.000", null,
                false, "+02:00", null,
                "https://events.itrevolution.com/virtual/","slug", "Conference",
                true, null, null, "Maribor",
                "Slovenia", null, "AWS","https://aws.com/event","aws@mail.com",
                "twitter-link", "facebook-link", "youtube-link", "linkedin-link");
        ErrorMap errorMap = new ErrorMap();
        errorMap.setError("objectField", "message");
        errorMap.setError("objectName", "message");
        NewUpdateWithError newUpdateWithError = new NewUpdateWithError(adminEventUpdateViewModel, errorMap, "itshelf_user");

        // when
        ModelAndView modelAndView = newUpdateWithError.update();

        // then
        Assertions.assertNotNull(modelAndView);
        Assertions.assertTrue(((ErrorMap)modelAndView.getModel().get("adminEventErrorViewModel")).containsError("objectField"));
        Assertions.assertEquals("admin/adminEvent",modelAndView.getViewName());
    }
}
