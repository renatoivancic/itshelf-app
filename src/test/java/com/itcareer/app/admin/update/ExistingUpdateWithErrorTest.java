package com.itcareer.app.admin.update;

import com.itcareer.app.admin.viewmodel.AdminEventUpdateViewModel;
import com.itcareer.app.admin.viewmodel.ErrorMap;
import com.itcareer.app.domain.Event;
import com.itcareer.app.event.EventService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.servlet.ModelAndView;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class ExistingUpdateWithErrorTest {

    @Mock
    EventService eventService;

    @Test
    void testStrategyWhenInvokeReturnReturnModelViewWithError() {

        // given
        Event event = new Event(11L,
                "AWS Java1",
                "https://events.itrevolution.com/virtual/",
                ZonedDateTime.of(2020, 6, 17, 0, 0, 0, 0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                ZonedDateTime.of(2020, 6, 17, 0, 0, 0, 0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                "Online Conference",
                null,
                null,
                "image",
                true,
                null,
                null,
                "Description of an event",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                "DDD, Software Architecture, TDD",
                "",
                "new-aws-java"
        );
        AdminEventUpdateViewModel adminEventUpdateViewModel = new AdminEventUpdateViewModel("11",
                "New name", "New description", "image",
                "DDD, Software Architecture, TDD", "Active","2020-06-23T02:00:00.000", null,
                false, "+02:00", null,
                "https://events.itrevolution.com/virtual/","slug", "Conference",
                true, null, null, "Maribor",
                "Slovenia", null, "AWS","https://aws.com/event","aws@mail.com",
                "twitter-link", "facebook-link", "youtube-link", "linkedin-link");
        ErrorMap errorMap = new ErrorMap();
        errorMap.setError("objectField", "message");
        errorMap.setError("objectName", "message");
        Mockito.when(eventService.findEventById(any())).thenReturn(event);
        ExistingUpdateWithError existingUpdateWithError = new ExistingUpdateWithError(eventService,"11",adminEventUpdateViewModel, errorMap, "itshelf_user");

        // when
        ModelAndView modelAndView = existingUpdateWithError.update();

        // then
        Assertions.assertNotNull(modelAndView);
        Assertions.assertTrue(((ErrorMap)modelAndView.getModel().get("adminEventErrorViewModel")).containsError("objectField"));
        Assertions.assertEquals("admin/adminEvent",modelAndView.getViewName());
    }
}
