package com.itcareer.app.admin.update;

import com.itcareer.app.admin.viewmodel.ErrorMap;
import com.itcareer.app.domain.Event;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.web.servlet.ModelAndView;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public class EventDetailModelViewFactoryTest {

    @Test
    void createEventDetailModelView() {

        // given
        Event event = new Event(11L,
                "AWS Java1",
                "https://events.itrevolution.com/virtual/",
                ZonedDateTime.of(2020,6,17,0,0,0,0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                ZonedDateTime.of(2020,6,17,0,0,0,0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                "Online Conference",
                null,
                null,
                "image",
                true,
                null,
                null,
                "Description of an event",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                "DDD, Software Architecture, TDD",
                "",
                "new-aws-java"
        );
        ErrorMap errorMap = new ErrorMap();
        errorMap.setError("objectField", "message");
        errorMap.setError("objectName", "message");

        // when
        ModelAndView modelAndView = EventDetailModelViewFactory.createEventDetailPageModelAndViewWithError(event, "rivancic", errorMap);

        // then
        Assertions.assertNotNull(modelAndView.getModel().get("adminEventErrorViewModel"));
    }
}
