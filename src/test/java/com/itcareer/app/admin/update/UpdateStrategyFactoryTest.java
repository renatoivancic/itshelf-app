package com.itcareer.app.admin.update;

import com.itcareer.app.admin.viewmodel.AdminEventUpdateViewModel;
import com.itcareer.app.admin.viewmodel.ErrorMap;
import com.itcareer.app.event.EventService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UpdateStrategyFactoryTest {

    @Mock
    EventService eventService;

    @Test
    void testExistingUpdateStrategyFactory() {

        // given
        AdminEventUpdateViewModel adminEventUpdateViewModel = new AdminEventUpdateViewModel("11",
                "New name", "New description", "image",
                "DDD, Software Architecture, TDD", "Active","2020-06-23T02:00:00.000", null,
                false, "+02:00", null,
                "https://events.itrevolution.com/virtual/","slug", "Conference",
                true, null, null, "Maribor",
                "Slovenia", null, "AWS","https://aws.com/event","aws@mail.com",
                "twitter-link", "facebook-link", "youtube-link", "linkedin-link");

        ErrorMap errorMap = new ErrorMap();
        UpdateStrategyFactory updateStrategyFactory = new UpdateStrategyFactory(eventService);

        // when
        UpdateStrategy updateStrategy = updateStrategyFactory.getUpdateStrategy(adminEventUpdateViewModel, errorMap, "rivancic");

        // then
        Assertions.assertNotNull(updateStrategy);
        Assertions.assertTrue(updateStrategy instanceof ExistingUpdate);
    }

    @Test
    void testExistingUpdateStrategyWithErrorFactory() {

        // given
        AdminEventUpdateViewModel adminEventUpdateViewModel = new AdminEventUpdateViewModel("11",
                "New name", "New description", "image",
                "DDD, Software Architecture, TDD", "Active","2020-06-23T02:00:00.000", null,
                false, "+02:00", null,
                "https://events.itrevolution.com/virtual/","slug", "Conference",
                true, null, null, "Maribor",
                "Slovenia", null, "AWS","https://aws.com/event","aws@mail.com",
                "twitter-link", "facebook-link", "youtube-link", "linkedin-link");
        ErrorMap errorMap = new ErrorMap();
        errorMap.setError("error","error message");
        UpdateStrategyFactory updateStrategyFactory = new UpdateStrategyFactory(eventService);

        // when
        UpdateStrategy updateStrategy = updateStrategyFactory.getUpdateStrategy(adminEventUpdateViewModel, errorMap, "rivancic");

        // then
        Assertions.assertNotNull(updateStrategy);
        Assertions.assertTrue(updateStrategy instanceof ExistingUpdateWithError);
    }

    @Test
    void testNewUpdateStrategyWithErrorFactory() {

        // given
        AdminEventUpdateViewModel adminEventUpdateViewModel = new AdminEventUpdateViewModel("new",
                "New name", "New description", "image",
                "DDD, Software Architecture, TDD", "Active","2020-06-23T02:00:00.000", null,
                false, "+02:00", null,
                "https://events.itrevolution.com/virtual/","slug", "Conference",
                true, null, null, "Maribor",
                "Slovenia", null, "AWS","https://aws.com/event","aws@mail.com",
                "twitter-link", "facebook-link", "youtube-link", "linkedin-link");
        ErrorMap errorMap = new ErrorMap();
        errorMap.setError("error","error message");
        UpdateStrategyFactory updateStrategyFactory = new UpdateStrategyFactory(eventService);

        // when
        UpdateStrategy updateStrategy = updateStrategyFactory.getUpdateStrategy(adminEventUpdateViewModel, errorMap, "rivancic");

        // then
        Assertions.assertNotNull(updateStrategy);
        Assertions.assertTrue(updateStrategy instanceof NewUpdateWithError);
    }

    @Test
    void testNewUpdateStrategyFactory() {

        // given
        AdminEventUpdateViewModel adminEventUpdateViewModel = new AdminEventUpdateViewModel("new",
                "New name", "New description", "image",
                "DDD, Software Architecture, TDD", "Active","2020-06-23T02:00:00.000", null,
                false, "+02:00", null,
                "https://events.itrevolution.com/virtual/","slug", "Conference",
                true, null, null, "Maribor",
                "Slovenia", null, "AWS","https://aws.com/event","aws@mail.com",
                "twitter-link", "facebook-link", "youtube-link", "linkedin-link");
        ErrorMap errorMap = new ErrorMap();
        UpdateStrategyFactory updateStrategyFactory = new UpdateStrategyFactory(eventService);

        // when
        UpdateStrategy updateStrategy = updateStrategyFactory.getUpdateStrategy(adminEventUpdateViewModel, errorMap, "rivancic");

        // then
        Assertions.assertNotNull(updateStrategy);
        Assertions.assertTrue(updateStrategy instanceof NewUpdate);
    }
}
