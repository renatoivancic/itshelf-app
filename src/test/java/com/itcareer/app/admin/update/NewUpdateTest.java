package com.itcareer.app.admin.update;

import com.itcareer.app.admin.viewmodel.AdminEventUpdateViewModel;
import com.itcareer.app.admin.viewmodel.AdminEventViewModel;
import com.itcareer.app.domain.Event;
import com.itcareer.app.event.EventService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.servlet.ModelAndView;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class NewUpdateTest {

    @Mock
    EventService eventService;

    @Test
    void testStrategy() {

        // given
        Event event = new Event(11L,
                "AWS Java1",
                "https://events.itrevolution.com/virtual/",
                ZonedDateTime.of(2020, 6, 17, 0, 0, 0, 0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                ZonedDateTime.of(2020, 6, 17, 0, 0, 0, 0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                "Online Conference",
                null,
                null,
                "image",
                true,
                null,
                null,
                "Description of an event",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                "DDD, Software Architecture, TDD",
                "",
                "new-aws-java"
        );

        AdminEventUpdateViewModel adminEventUpdateViewModel = new AdminEventUpdateViewModel("11",
                "New name", "New description", "image",
                "DDD, Software Architecture, TDD", "Active", "2020-06-23T02:00:00.000", null,
                false, "+02:00", null,
                "https://events.itrevolution.com/virtual/", "slug", "Conference",
                true, null, null, "Maribor",
                "Slovenia", null, "AWS", "https://aws.com/event", "aws@mail.com",
                "twitter-link", "facebook-link", "youtube-link", "linkedin-link");

        Mockito.when(eventService.saveEvent(any())).thenReturn(event);
        NewUpdate newUpdate = new NewUpdate(eventService, adminEventUpdateViewModel, "itshelf_user");

        // when
        ModelAndView modelAndView = newUpdate.update();

        // then
        Assertions.assertNotNull(modelAndView);
        Assertions.assertNull(modelAndView.getModel().get("adminEventErrorViewModel"));
        Assertions.assertEquals("redirect:/admin/events/11", modelAndView.getViewName());
        Assertions.assertNotNull(modelAndView.getModel().get("adminEventViewModel"));
        Assertions.assertEquals("itshelf_user", ((AdminEventViewModel) modelAndView.getModel().get("adminEventViewModel")).getUsername());
        Assertions.assertEquals(11L, ((AdminEventViewModel) modelAndView.getModel().get("adminEventViewModel")).getEvent().getId());
    }
}
