package com.itcareer.app.admin.viewmodel;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ErrorMapTest {

    @Test
    void basicTest() {

        // given
        ErrorMap errorMap = new ErrorMap();
        errorMap.setError("Error_Code", "Error message");

        // then
        Assertions.assertTrue(errorMap.containsError("Error_Code"));
    }
}
