package com.itcareer.app.admin.enrich;

import com.itcareer.app.admin.viewmodel.ErrorMap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class ErrorMapFactoryTest {

    @Mock
    BindingResult bindingResult;

    @Test
    void testFactory() {

        // given
        List<ObjectError> errors = new ArrayList<>();
        ObjectError fieldError = new FieldError("objectName", "objectField", "message");
        errors.add(fieldError);
        ObjectError objectError = new ObjectError("objectName", "message");
        errors.add(objectError);
        Mockito.when(bindingResult.getAllErrors()).thenReturn(errors);

        // when
        ErrorMap errorMap = ErrorMapFactory.buildErrorMap(bindingResult);

        // then
        Assertions.assertTrue(errorMap.containsError("objectField"));
    }
}
