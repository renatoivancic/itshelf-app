package com.itcareer.app.admin.enrich;

import com.itcareer.app.domain.Event;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

public class EventQualityServiceTest {

    @Test
    void testEmptyAdminEvents() {
        List<AdminEvent> adminEvents = new ArrayList<>();
        EventQualityStats eventQualityStats = EventQualityService.getEventQualityStats(adminEvents);
        Assertions.assertEquals(0 , eventQualityStats.getNumberOfAllEvents());
        Assertions.assertEquals(0.0 , eventQualityStats.getGood());
        Assertions.assertEquals(0.0 , eventQualityStats.getBad());
        Assertions.assertEquals(0.0 , eventQualityStats.getNumberOfUpcomingEvents());
    }

    @Test
    void testAdminEvents() {
        List<AdminEvent> adminEvents = new ArrayList<>();
        Event event = new Event(11L,
                "AWS Java",
                "https://aws.amazon.com/events/summits/online/emea/",
                ZonedDateTime.now().plusDays(2L),
                null,
                "Online Conference",
                null,
                null,
                "https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png",
                true,
                null,
                null,
                "Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                "AWS,Cloud,DevOps,Java,Data Warehouses,AI/ML,Machine Learning,CI/CD,Kubernetes,GitOps,Applications,REST,GraphQL,Kafka,Startup",
                "",
                "aws-java"
        );
        AdminEvent adminEvent = new AdminEvent(event, 40, true);
        adminEvents.add(adminEvent);
        EventQualityStats eventQualityStats = EventQualityService.getEventQualityStats(adminEvents);
        Assertions.assertEquals(1 , eventQualityStats.getNumberOfAllEvents());
        Assertions.assertEquals(0.0 , eventQualityStats.getGood());
        Assertions.assertEquals(100 , eventQualityStats.getBad());
        Assertions.assertEquals(1 , eventQualityStats.getNumberOfUpcomingEvents());
    }

    @Test
    void countWordsTest() {

        // when
        int result = EventQualityService.countWordsUsingStringTokenizer("one two");

        // then
        Assertions.assertEquals(2 , result);
    }

    @Test
    void countEmptyWordsTest() {

        // when
        int result = EventQualityService.countWordsUsingStringTokenizer("");

        // then
        Assertions.assertEquals(0 , result);
    }

    @Test
    void countNullWordsTest() {

        // when
        int result = EventQualityService.countWordsUsingStringTokenizer(null);

        // then
        Assertions.assertEquals(0 , result);
    }
}
