package com.itcareer.app.admin;

import com.itcareer.app.admin.viewmodel.AdminEventUpdateViewModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AdminEventUpdateViewModelTest {

    @Test
    void testEventDTO() {

        AdminEventUpdateViewModel adminEventUpdateViewModel = new AdminEventUpdateViewModel("11","",
                "", "", "", "",
                "", "", false,
                "", "", "","", "",
                false, "", "",
                "", "","", "", "", "",
                "", "", "", "");
        adminEventUpdateViewModel.setDescription("description");
        adminEventUpdateViewModel.setName("name");
        adminEventUpdateViewModel.setImage("image");
        adminEventUpdateViewModel.setKeywords("DDD, Software Architecture, TDD");
        adminEventUpdateViewModel.setStatus("Active");
        adminEventUpdateViewModel.setDateTimeFrom("10:09");
        adminEventUpdateViewModel.setDateTimeTo("10:10");
        adminEventUpdateViewModel.setDateTimeToFlag(true);
        adminEventUpdateViewModel.setTimezoneOffsetFrom("-1");
        adminEventUpdateViewModel.setTimezoneOffsetTo("-2");
        adminEventUpdateViewModel.setExternalURL("https://events.itrevolution.com/virtual/");
        adminEventUpdateViewModel.setSlug("new-slug");
        adminEventUpdateViewModel.setType("Conference");
        adminEventUpdateViewModel.setFree(false);
        adminEventUpdateViewModel.setPriceFrom("10 EUR");
        adminEventUpdateViewModel.setPriceTo("20 EUR");
        adminEventUpdateViewModel.setCity("Maribor");
        adminEventUpdateViewModel.setCountry("Slovenia");
        adminEventUpdateViewModel.setAddress("84 Northyards Blvd NW,\n" +
                "Atlanta,\n" +
                "GA 30313");
        adminEventUpdateViewModel.setVendorName("vendor name");
        adminEventUpdateViewModel.setVendorLink("vendor link");
        adminEventUpdateViewModel.setVendorEmail("vendor email");
        adminEventUpdateViewModel.setTwitterLink("twitter-link");
        adminEventUpdateViewModel.setFacebookLink("facebook-link");
        adminEventUpdateViewModel.setYoutubeLink("youtube-link");
        adminEventUpdateViewModel.setLinkedinLink("linkedin-link");

        Assertions.assertEquals("name", adminEventUpdateViewModel.getName());
        Assertions.assertEquals("description", adminEventUpdateViewModel.getDescription());
        Assertions.assertEquals("image", adminEventUpdateViewModel.getImage());
        Assertions.assertEquals("DDD, Software Architecture, TDD", adminEventUpdateViewModel.getKeywords());
        Assertions.assertEquals("Active", adminEventUpdateViewModel.getStatus());
        Assertions.assertEquals("10:09", adminEventUpdateViewModel.getDateTimeFrom());
        Assertions.assertEquals("10:10", adminEventUpdateViewModel.getDateTimeTo());
        Assertions.assertTrue(adminEventUpdateViewModel.isDateTimeToFlag());
        Assertions.assertEquals("-1", adminEventUpdateViewModel.getTimezoneOffsetFrom());
        Assertions.assertEquals("-2", adminEventUpdateViewModel.getTimezoneOffsetTo());
        Assertions.assertEquals("https://events.itrevolution.com/virtual/", adminEventUpdateViewModel.getExternalURL());
        Assertions.assertEquals("new-slug", adminEventUpdateViewModel.getSlug());
        Assertions.assertEquals("Conference", adminEventUpdateViewModel.getType());
        Assertions.assertFalse(adminEventUpdateViewModel.isFree());
        Assertions.assertEquals("10 EUR", adminEventUpdateViewModel.getPriceFrom());
        Assertions.assertEquals("20 EUR", adminEventUpdateViewModel.getPriceTo());
        Assertions.assertEquals("Maribor", adminEventUpdateViewModel.getCity());
        Assertions.assertEquals("Slovenia", adminEventUpdateViewModel.getCountry());
        Assertions.assertEquals("84 Northyards Blvd NW,\n" +
                "Atlanta,\n" +
                "GA 30313", adminEventUpdateViewModel.getAddress());
        Assertions.assertEquals("vendor name", adminEventUpdateViewModel.getVendorName());
        Assertions.assertEquals("vendor link", adminEventUpdateViewModel.getVendorLink());
        Assertions.assertEquals("vendor email", adminEventUpdateViewModel.getVendorEmail());
        Assertions.assertEquals("twitter-link", adminEventUpdateViewModel.getTwitterLink());
        Assertions.assertEquals("facebook-link", adminEventUpdateViewModel.getFacebookLink());
        Assertions.assertEquals("youtube-link", adminEventUpdateViewModel.getYoutubeLink());
        Assertions.assertEquals("linkedin-link", adminEventUpdateViewModel.getLinkedinLink());
    }
}
