package com.itcareer.app.event;

import com.itcareer.app.domain.Event;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

@ExtendWith(MockitoExtension.class)
public class EventServiceTest {

    @Mock
    EventRepository eventRepository;

    /**
     * When user is searching without typing anything return him upcoming events.
     */
    @Test
    public void testEventByEmptyKeyword() {

        // given
        List<Event> listOfEvents = new ArrayList<>();

        Event firstEvent = new Event(4L,
                "AWS Summit Online EMEA",
                "https://aws.amazon.com/events/summits/online/emea/",
                ZonedDateTime.of(2020, 6, 17, 0, 0, 0, 0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                null,
                "Online Conference",
                null,
                null,
                "https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png",
                true,
                null,
                null,
                "Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                "AWS,Cloud,DevOps,InfoSec,Data Warehouses,AI/ML,Machine Learning,CI/CD,Kubernetes,GitOps,Applications,REST,GraphQL,Kafka,Startup",
                "",
                "aws-online-summit-emea"
        );

        listOfEvents.add(firstEvent);

        Mockito.when(eventRepository.findMostRecentUpcomingEvents()).thenReturn(listOfEvents);
        EventService eventService = new EventService(eventRepository);

        // when
        List<Event> events = eventService.findEventBy("");

        // then
        Assertions.assertNotNull(events);
        Assertions.assertEquals(1, events.size());
        Assertions.assertEquals(4, events.get(0).getId());
        Assertions.assertEquals("Active", events.get(0).getStatus());
    }

    /**
     * When user is searching events with a specific keyword return him filtered result
     */
    @Test
    public void testEventByNonEmptyKeyword() {

        // given
        List<Event> listOfEvents = new ArrayList<>();

        Event firstEvent = new Event(11L,
                "AWS Java",
                "https://aws.amazon.com/events/summits/online/emea/",
                ZonedDateTime.of(2020, 6, 17, 0, 0, 0, 0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                null,
                "Online Conference",
                null,
                null,
                "https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png",
                true,
                null,
                null,
                "Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                "AWS,Cloud,DevOps,Java,Data Warehouses,AI/ML,Machine Learning,CI/CD,Kubernetes,GitOps,Applications,REST,GraphQL,Kafka,Startup",
                "",
                "aws-java"
        );

        listOfEvents.add(firstEvent);

        Mockito.when(eventRepository.findAllFromTodayBySearchTerm("%java%")).thenReturn(listOfEvents);
        EventService eventService = new EventService(eventRepository);

        // when
        List<Event> events = eventService.findEventBy("java");

        // then
        Assertions.assertNotNull(events);
        Assertions.assertEquals(1, events.size());
        Assertions.assertEquals(11, events.get(0).getId());
        Assertions.assertEquals("AWS Java", events.get(0).getName());

        Assertions.assertNull(events.get(0).getPriceFrom());
        Assertions.assertNull(events.get(0).getPriceTo());
        Assertions.assertEquals("Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ", events.get(0).getDescription());
        Assertions.assertEquals("", events.get(0).getContactInfoEmail());
        Assertions.assertEquals("Amazon Web Services", events.get(0).getVendorName());
        Assertions.assertEquals("https://twitter.com/hashtag/AWSSummit,Active", events.get(0).getTwitterLink());
        Assertions.assertEquals("https://www.facebook.com/amazonwebservices.de/", events.get(0).getFacebookLink());
        Assertions.assertEquals("https://www.youtube.com/user/AmazonWebServices", events.get(0).getYoutubeLink());
        Assertions.assertEquals("https://www.linkedin.com/company/amazon-web-services/", events.get(0).getLinkedinLink());
        Assertions.assertTrue(events.get(0).isFree());
        Assertions.assertEquals("AWS,Cloud,DevOps,Java,Data Warehouses,AI/ML,Machine Learning,CI/CD,Kubernetes,GitOps,Applications,REST,GraphQL,Kafka,Startup", events.get(0).getKeywords());
        Assertions.assertEquals("https://aws.amazon.com/", events.get(0).getVendorLink());
    }

    /**
     * Service is searching event by the slug that does not exist. NoSuchElement exception has to be thrown.
     */
    @Test
    public void testFindEventBySlugThatDoesNotExists() {

        // given
        Mockito.when(eventRepository.findBySlug("nonexisting")).thenReturn(Optional.empty());
        EventService eventService = new EventService(eventRepository);

        // then
        Assertions.assertThrows(NoSuchElementException.class, () -> {

            // when
            eventService.findEventBySlug("nonexisting");
        });
    }

    /**
     * Service is searching event by the slug that does exist.
     * One single element is returned.
     */
    @Test
    public void testEventBySlug() {

        // given
        Event firstEvent = new Event(11L,
                "AWS Java",
                "https://aws.amazon.com/events/summits/online/emea/",
                ZonedDateTime.of(2020, 6, 17, 0, 0, 0, 0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                null,
                "Online Conference",
                null,
                null,
                "https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png",
                true,
                null,
                null,
                "Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                null,
                "Ulica Vinke Megle 12",
                "aws-java"
        );

        Mockito.when(eventRepository.findBySlug("java")).thenReturn(Optional.of(firstEvent));
        EventService eventService = new EventService(eventRepository);

        // when
        Event event = eventService.findEventBySlug("java");

        // then
        Assertions.assertNotNull(event);
        Assertions.assertEquals(11, event.getId());
        Assertions.assertEquals("AWS Java", event.getName());

        Assertions.assertNull(event.getPriceFrom());
        Assertions.assertNull(event.getPriceTo());
        Assertions.assertEquals("Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ", event.getDescription());
        Assertions.assertEquals("", event.getContactInfoEmail());
        Assertions.assertEquals("Amazon Web Services", event.getVendorName());
        Assertions.assertEquals("https://twitter.com/hashtag/AWSSummit,Active", event.getTwitterLink());
        Assertions.assertEquals("https://www.facebook.com/amazonwebservices.de/", event.getFacebookLink());
        Assertions.assertEquals("https://www.youtube.com/user/AmazonWebServices", event.getYoutubeLink());
        Assertions.assertEquals("https://www.linkedin.com/company/amazon-web-services/", event.getLinkedinLink());
        Assertions.assertTrue(event.isFree());
        Assertions.assertEquals(0, event.getArrayOfKeywords().length);
        Assertions.assertEquals("https://aws.amazon.com/", event.getVendorLink());
        Assertions.assertEquals("Ulica Vinke Megle 12", event.getLocationAddress());
        Assertions.assertEquals("aws-java", event.getSlug());
    }


    @Test
    public void testFetchingAllEventsForAdmin() {
        // given
        Event firstEvent = new Event(11L,
                "AWS Java",
                "https://aws.amazon.com/events/summits/online/emea/",
                ZonedDateTime.of(2020, 6, 17, 0, 0, 0, 0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                null,
                "Online Conference",
                null,
                null,
                "https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png",
                true,
                null,
                null,
                "Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                null,
                "Ulica Vinke Megle 12",
                "aws-java"
        );

        Event secondEvent = new Event(12L,
                "AWS Python",
                "https://aws.amazon.com/events/summits/online/emea/",
                ZonedDateTime.of(2020, 6, 17, 0, 0, 0, 0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                null,
                "Online Conference",
                null,
                null,
                "https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png",
                true,
                null,
                null,
                "Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                null,
                "Ulica Vinke Megle 12",
                "aws-java"
        );

        Event thirdEvent = new Event(13L,
                "AWS Groovy",
                "https://aws.amazon.com/events/summits/online/emea/",
                ZonedDateTime.of(2020, 6, 17, 0, 0, 0, 0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                null,
                "Online Conference",
                null,
                null,
                "https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png",
                true,
                null,
                null,
                "Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                null,
                "Ulica Vinke Megle 12",
                "aws-java"
        );

        Mockito.when(eventRepository.findAll()).thenReturn(Arrays.asList(firstEvent, secondEvent, thirdEvent));
        EventService eventService = new EventService(eventRepository);

        // when
        List<Event> events = eventService.getAllEvents();

        // then
        Assertions.assertNotNull(events);
        Assertions.assertEquals(3, events.size());
        Assertions.assertEquals(11, events.get(0).getId());
        Assertions.assertEquals("AWS Java", events.get(0).getName());
        Assertions.assertEquals(12, events.get(1).getId());
        Assertions.assertEquals("AWS Python", events.get(1).getName());
        Assertions.assertEquals(13, events.get(2).getId());
        Assertions.assertEquals("AWS Groovy", events.get(2).getName());
    }

    /**
     * Service is searching event by the id that does exist.
     * One single element is returned.
     */
    @Test
    public void testEventById() {

        // given
        Event firstEvent = new Event(11L,
                "AWS Java",
                "https://aws.amazon.com/events/summits/online/emea/",
                ZonedDateTime.of(2020, 6, 17, 0, 0, 0, 0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                null,
                "Online Conference",
                null,
                null,
                "https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png",
                true,
                null,
                null,
                "Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                null,
                "Ulica Vinke Megle 12",
                "aws-java"
        );

        Mockito.when(eventRepository.findById(11L)).thenReturn(Optional.of(firstEvent));
        EventService eventService = new EventService(eventRepository);

        // when
        Event event = eventService.findEventById(11L);

        // then
        Assertions.assertNotNull(event);
        Assertions.assertEquals(11, event.getId());
        Assertions.assertEquals("AWS Java", event.getName());

        Assertions.assertNull(event.getPriceFrom());
        Assertions.assertNull(event.getPriceTo());
        Assertions.assertEquals("Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ", event.getDescription());
        Assertions.assertEquals("", event.getContactInfoEmail());
        Assertions.assertEquals("Amazon Web Services", event.getVendorName());
        Assertions.assertEquals("https://twitter.com/hashtag/AWSSummit,Active", event.getTwitterLink());
        Assertions.assertEquals("https://www.facebook.com/amazonwebservices.de/", event.getFacebookLink());
        Assertions.assertEquals("https://www.youtube.com/user/AmazonWebServices", event.getYoutubeLink());
        Assertions.assertEquals("https://www.linkedin.com/company/amazon-web-services/", event.getLinkedinLink());
        Assertions.assertTrue(event.isFree());
        Assertions.assertEquals(0, event.getArrayOfKeywords().length);
        Assertions.assertEquals("https://aws.amazon.com/", event.getVendorLink());
        Assertions.assertEquals("Ulica Vinke Megle 12", event.getLocationAddress());
        Assertions.assertEquals("aws-java", event.getSlug());
    }

    /**
     * Service is searching event by the id that doesn't exist.
     * Exception is thrown.
     */
    @Test
    public void testEventByIdButDoesntExist() {

        // given
        Event firstEvent = new Event(11L,
                "AWS Java",
                "https://aws.amazon.com/events/summits/online/emea/",
                ZonedDateTime.of(2020, 6, 17, 0, 0, 0, 0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                null,
                "Online Conference",
                null,
                null,
                "https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png",
                true,
                null,
                null,
                "Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                null,
                "Ulica Vinke Megle 12",
                "aws-java"
        );

        Mockito.lenient().when(eventRepository.findById(11L)).thenReturn(Optional.of(firstEvent));
        EventService eventService = new EventService(eventRepository);

        // when
        Assertions.assertThrows(NoSuchElementException.class, () -> {
            Event event = eventService.findEventById(12L);

            // then
            Assertions.assertNotNull(event);
        });
    }

    /**
     * Service is persisting event.
     * Event is returned.
     */
    @Test
    public void testEventPersistence() {

        // given
        Event firstEvent = new Event(11L,
                "AWS Java",
                "https://aws.amazon.com/events/summits/online/emea/",
                ZonedDateTime.of(2020, 6, 17, 0, 0, 0, 0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                null,
                "Online Conference",
                null,
                null,
                "https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png",
                true,
                null,
                null,
                "Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                null,
                "Ulica Vinke Megle 12",
                "aws-java"
        );

        Mockito.when(eventRepository.save(firstEvent)).thenReturn(firstEvent);
        EventService eventService = new EventService(eventRepository);

        // when
        Event event = eventService.saveEvent(firstEvent);

        // then
        Assertions.assertNotNull(event);
        Assertions.assertEquals(11, event.getId());
        Assertions.assertEquals("AWS Java", event.getName());

        Assertions.assertNull(event.getPriceFrom());
        Assertions.assertNull(event.getPriceTo());
        Assertions.assertEquals("Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ", event.getDescription());
        Assertions.assertEquals("", event.getContactInfoEmail());
        Assertions.assertEquals("Amazon Web Services", event.getVendorName());
        Assertions.assertEquals("https://twitter.com/hashtag/AWSSummit,Active", event.getTwitterLink());
        Assertions.assertEquals("https://www.facebook.com/amazonwebservices.de/", event.getFacebookLink());
        Assertions.assertEquals("https://www.youtube.com/user/AmazonWebServices", event.getYoutubeLink());
        Assertions.assertEquals("https://www.linkedin.com/company/amazon-web-services/", event.getLinkedinLink());
        Assertions.assertTrue(event.isFree());
        Assertions.assertEquals(0, event.getArrayOfKeywords().length);
        Assertions.assertEquals("https://aws.amazon.com/", event.getVendorLink());
        Assertions.assertEquals("Ulica Vinke Megle 12", event.getLocationAddress());
        Assertions.assertEquals("aws-java", event.getSlug());
    }
}
