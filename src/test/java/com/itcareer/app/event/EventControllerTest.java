package com.itcareer.app.event;

import com.itcareer.app.analytics.AnalyticsService;
import com.itcareer.app.core.markdown.MarkdownProcessor;
import com.itcareer.app.core.security.WebSecurityConfig;
import com.itcareer.app.domain.Event;
import com.itcareer.app.serp.SearchTermSanitizer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.client.RestTemplate;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(EventController.class)
@Import(WebSecurityConfig.class)
public class EventControllerTest {

    @MockBean
    RestTemplate restTemplate;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EventService eventService;

    @MockBean
    private AnalyticsService analyticsService;

    @MockBean
    private SearchTermSanitizer searchTermSanitizer;

    @TestConfiguration
    static class TestConfig {

        @Bean
        public MarkdownProcessor markdownProcessorBean() {

            return new MarkdownProcessor();
        }
    }

    @Test
    public void testEmptyRequest( ) throws Exception {

        // when
        this.mockMvc
                .perform(get("/events"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * Mocked Service returns one event
     */
    @Test
    public void testJavaRequest( ) throws Exception {

        // given
        List<Event> events = new ArrayList<>();
        Event event = new Event(11L,
                "AWS Java",
                "https://aws.amazon.com/events/summits/online/emea/",
                ZonedDateTime.of(2020,6,17,0,0,0,0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                null,
                "Online Conference",
                null,
                null,
                "https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png",
                true,
                null,
                null,
                "Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                "AWS,Cloud,DevOps,Java,Data Warehouses,AI/ML,Machine Learning,CI/CD,Kubernetes,GitOps,Applications,REST,GraphQL,Kafka,Startup",
                "",
                "aws-java"
        );
        events.add(event);
        when(searchTermSanitizer.sanitizeInput("java")).thenReturn("java");
        when(eventService.findEventBy("java")).thenReturn(events);

        // when
        MvcResult mvcResult = this.mockMvc
                .perform(get("/events?searchTerm=java"))
                .andDo(print())
                .andExpect(status().isOk()).andReturn();

        // then
        // test the model and view do not test how the response is being rendered.
        // for this the UI tests have to be implemented.
        Object eventSearchResults = Objects.requireNonNull(mvcResult.getModelAndView()).getModel().get("eventSearchResults");
        Assertions.assertTrue(eventSearchResults instanceof EventSearchResult);
        EventSearchResult eventSearchResult = (EventSearchResult) eventSearchResults;
        Assertions.assertEquals(events, eventSearchResult.getEvents());

        Assertions.assertEquals("java", eventSearchResult.getSearchTerm());
        Assertions.assertEquals("/img/logo/itshelf-logo.png", eventSearchResult.getPathToLogo());
        Assertions.assertEquals("AWS Java", eventSearchResult.getEvents().get(0).getName());
        Assertions.assertEquals(ZonedDateTime.of(2020,6,17,0,0,0,0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))), eventSearchResult.getEvents().get(0).getDateFrom());
        //verify(analyticsService, times(1)).persistSearchEvent("java");
    }

    /**
     * Mock request to get event by the slug.
     */
    @Test
    public void testJavaDetailPageRequest( ) throws Exception {

        // given
        Event event = new Event(11L,
                "AWS Java",
                "https://aws.amazon.com/events/summits/online/emea/",
                ZonedDateTime.of(2020,6,17,0,0,0,0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                null,
                "Online Conference",
                null,
                null,
                "https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png",
                true,
                null,
                null,
                "Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                "AWS,Cloud,DevOps,Java,Data Warehouses,AI/ML,Machine Learning,CI/CD,Kubernetes,GitOps,Applications,REST,GraphQL,Kafka,Startup",
                "",
                "aws-java"
        );
        when(eventService.findEventBySlug("java")).thenReturn(event);

        // when
        MvcResult mvcResult = this.mockMvc
                .perform(get("/events/java"))
                .andDo(print())
                .andExpect(status().isOk()).andReturn();

        // then
        // test the model and view do not test how the response is being rendered.
        // for this the UI tests have to be implemented.
        Object eventDetailResults = Objects.requireNonNull(mvcResult.getModelAndView()).getModel().get("eventDetailResults");
        Assertions.assertTrue(eventDetailResults instanceof EventDetailResult);
        EventDetailResult eventDetailResult = (EventDetailResult) eventDetailResults;
        Assertions.assertEquals(event, eventDetailResult.getEvent());

        Assertions.assertEquals("/img/logo/itshelf-logo.png", eventDetailResult.getPathToLogo());
        Assertions.assertEquals("AWS Java", eventDetailResult.getEvent().getName());
        Assertions.assertEquals(ZonedDateTime.of(2020,6,17,0,0,0,0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))), eventDetailResult.getEvent().getDateFrom());
    }
}
