package com.itcareer.app.event;

import com.itcareer.app.domain.Event;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class EventServiceRelevantEventsTest {

    @Mock
    EventRepository eventRepository;

    /**
     * None of the events is returned as the same event is filtered out.
     */
    @Test
    public void testRelavantEventIdentity() {

        // given
        List<Event> listOfEvents = new ArrayList<>();

        Event firstEvent = new Event(4L,
                "AWS Summit Online EMEA",
                "https://aws.amazon.com/events/summits/online/emea/",
                ZonedDateTime.of(2020,6,17,0,0,0,0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                null,
"Online Conference",
                null,
                null,
                "https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png",
                true,
                null,
                null,
                "Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                "AWS,Cloud,DevOps,InfoSec,Data Warehouses,AI/ML,Machine Learning,CI/CD,Kubernetes,GitOps,Applications,REST,GraphQL,Kafka,Startup",
                "",
                "aws-online-summit-emea"
        );

        listOfEvents.add(firstEvent);

        Mockito.when(eventRepository.findAllFromTodayByKeywords("'AWS' | 'Cloud' | 'DevOps'")).thenReturn(listOfEvents);
        // Mockito.when(eventRepository.findBySlug("aws-online-summit-emea")).thenReturn(Optional.of(firstEvent));
        EventService eventService = new EventService(eventRepository);

        // when
        List<Event> events = eventService.findSimilarEvents(firstEvent);

        // then
        Assertions.assertNotNull(events);
        Assertions.assertEquals(0, events.size());
    }

    /**
     * One event is returned the other one is filtered as its the same as the event that is used to look for relevant events.
     */
    @Test
    public void testRelavantEvent() {

        // given
        List<Event> listOfEvents = new ArrayList<>();

        Event firstEvent = new Event(4L,
                "AWS Summit Online EMEA",
                "https://aws.amazon.com/events/summits/online/emea/",
                ZonedDateTime.of(2020,6,17,0,0,0,0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                null,
                "Online Conference",
                null,
                null,
                "https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png",
                true,
                null,
                null,
                "Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                "AWS,Cloud,DevOps,InfoSec,Data Warehouses,AI/ML,Machine Learning,CI/CD,Kubernetes,GitOps,Applications,REST,GraphQL,Kafka,Startup",
                "",
                "aws-online-summit-emea"
        );

        Event secondEvent = new Event(5L,
                "Cisco",
                "https://aws.amazon.com/events/summits/online/emea/",
                ZonedDateTime.of(2020,6,17,0,0,0,0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                null,
                "Online Conference",
                null,
                null,
                "https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png",
                true,
                null,
                null,
                "Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                "AWS,Cloud,DevOps,InfoSec,Data Warehouses,AI/ML,Machine Learning,CI/CD,Kubernetes,GitOps,Applications,REST,GraphQL,Kafka,Startup",
                "",
                "cisco"
        );

        listOfEvents.add(firstEvent);
        listOfEvents.add(secondEvent);

        Mockito.when(eventRepository.findAllFromTodayByKeywords("'AWS' | 'Cloud' | 'DevOps'")).thenReturn(listOfEvents);
        // Mockito.when(eventRepository.findBySlug("aws-online-summit-emea")).thenReturn(Optional.of(firstEvent));
        EventService eventService = new EventService(eventRepository);

        // when
        List<Event> events = eventService.findSimilarEvents(firstEvent);

        // then
        Assertions.assertNotNull(events);
        Assertions.assertEquals(1, events.size());
        Assertions.assertEquals(5, events.get(0).getId());
        Assertions.assertEquals("Cisco", events.get(0).getName());
    }

}
