package com.itcareer.app.event;

import com.itcareer.app.domain.Event;
import com.itcareer.app.seo.model.EventAttendanceMode;
import com.itcareer.app.seo.model.EventSchemaOrgPojo;
import com.itcareer.app.seo.model.EventStatusType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public class EventSeoSchemaConverterTest {

    /**
     * When user is searching without typing anything return him upcoming events.
     */
    @Test
    public void testEventToSeoSchemaEntityConverter() {

        // given
        Event firstEvent = new Event(4L,
                "AWS Summit Online EMEA",
                "https://aws.amazon.com/events/summits/online/emea/",
                ZonedDateTime.of(2020, 6, 17, 0, 0, 0, 0, ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))),
                null,
                "Online Conference",
                null,
                null,
                "https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png",
                true,
                "EUR 10.05",
                null,
                "Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including getting started, building advanced architectures, app development, DevOps and more. ",
                "",
                "Amazon Web Services",
                "https://aws.amazon.com/",
                "https://twitter.com/hashtag/AWSSummit,Active",
                "Active",
                "https://www.facebook.com/amazonwebservices.de/",
                "https://www.youtube.com/user/AmazonWebServices",
                "https://www.linkedin.com/company/amazon-web-services/",
                "AWS,Cloud,DevOps,InfoSec,Data Warehouses,AI/ML,Machine Learning,CI/CD,Kubernetes,GitOps,Applications,REST,GraphQL,Kafka,Startup",
                "",
                "aws-online-summit-emea"
        );

        // when
        EventSchemaOrgPojo eventSchemaOrgPojo = EventSeoSchemaConverter.convertEventTo(firstEvent);

        // then
        Assertions.assertNotNull(eventSchemaOrgPojo);
        Assertions.assertEquals("https://d1.awsstatic.com/events/AWS_Summit-2020_780x300@2x.b272272a7a650508fb496fcd78714d35a821eb28.png", eventSchemaOrgPojo.getImage());
        Assertions.assertEquals("AWS Summit Online EMEA", eventSchemaOrgPojo.getName());
        Assertions.assertEquals("http://schema.org", eventSchemaOrgPojo.getContext());
        Assertions.assertEquals("Event", eventSchemaOrgPojo.getType());
        Assertions.assertEquals("Hear from your local AWS country leaders about the latest trends, customers and partners in your market, followed by the opening keynote with Werner Vogels, CTO, Amazon.com. After the keynote, dive deep in 55 breakout sessions across 11 tracks, including...", eventSchemaOrgPojo.getDescription());
        Assertions.assertEquals("https://aws.amazon.com/events/summits/online/emea/", eventSchemaOrgPojo.getUrl());
        Assertions.assertEquals(EventAttendanceMode.ONLINE_EVENT_ATTENDANCE_MODE, eventSchemaOrgPojo.getEventAttendanceMode());
        Assertions.assertEquals(EventStatusType.EVENT_SCHEDULED, eventSchemaOrgPojo.getEventStatusType());
        Assertions.assertEquals("EUR", eventSchemaOrgPojo.getOffer().getPriceCurrency());
        Assertions.assertEquals("10.05", eventSchemaOrgPojo.getOffer().getPrice());
    }

    @Test
    public void testIsValueNumeric() {

        // given

        // when
        boolean numeric = EventSeoSchemaConverter.isNumeric("10");

        // then
        Assertions.assertTrue(numeric);
    }

    @Test
    public void testIsNotValueNumeric() {

        // given

        // when
        boolean numeric = EventSeoSchemaConverter.isNumeric("ab10");

        // then
        Assertions.assertFalse(numeric);
    }

    @Test
    public void testIsNullValueNumeric() {

        // given

        // when
        boolean numeric = EventSeoSchemaConverter.isNumeric(null);

        // then
        Assertions.assertFalse(numeric);
    }
}
