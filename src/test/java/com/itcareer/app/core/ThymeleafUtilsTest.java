package com.itcareer.app.core;

import com.itcareer.app.core.thymeleaf.ThymeleafUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ThymeleafUtilsTest {

    @Test
    public void testNullKeywords() {

        // given
        // when
        String keywordsAsHashtags = ThymeleafUtils.getKeywordsAsHashtags(null);

        // then
        Assertions.assertEquals("", keywordsAsHashtags);
    }

    @Test
    public void testEmptyKeywords() {

        // given
        // when
        String keywordsAsHashtags = ThymeleafUtils.getKeywordsAsHashtags(new String[]{});

        // then
        Assertions.assertEquals("", keywordsAsHashtags);
    }

    @Test
    public void testSingleKeyword() {

        // given
        // when
        String keywordsAsHashtags = ThymeleafUtils.getKeywordsAsHashtags(new String[]{"keyword"});

        // then
        Assertions.assertEquals("#keyword", keywordsAsHashtags);
    }
}
