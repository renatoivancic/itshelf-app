package com.itcareer.app.core;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

public class SecurityTest {

    @Test
    @Disabled
    public void password() { // NOSONAR use to generate password
        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
	    System.out.println(encoder.encode("test1234"));
    }
}
