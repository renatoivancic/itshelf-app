package com.itcareer.app.core;

import com.itcareer.app.core.search.SearchService;
import com.itcareer.app.domain.Course;
import com.itcareer.app.domain.Vendor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.mockito.Mockito.lenient;


@DisplayName("Search service has to go through all vendors and return aggregated result")
@ExtendWith(MockitoExtension.class)
public class SearchServiceTest {

    @Mock
     CourseService courseService1;

    @Mock
     CourseService courseService2;

    List<CourseService> courseServices;

    @BeforeEach
    void before() {

        Vendor vendor1 = new Vendor("Android course provider", "Android Vendor Logo URL", "Android Vendor URL");
        Course androidCourseProvider1 = new Course("Best Android Course",
                vendor1, "Image URL", "Course URL", "headline1", "author");

        lenient().when(courseService1.findCoursesBy("android")).thenReturn(Stream.of(androidCourseProvider1));
        lenient().when(courseService2.findCoursesBy("")).thenReturn(Stream.empty());
        courseServices = Arrays.asList(courseService1, courseService2);

    }

    @DisplayName("Should aggregate and return search results for courses")
    @ParameterizedTest(name = "Search term: {0}, Expected result: {1}")
    @MethodSource("testPayloadProvider")
    public void testPolyglotInjectionFilterDisabled(String searchTerm, List<Course> result) {

        // given
        SearchService searchService = new SearchService(courseServices);

        // when
        List<Course> findCourses = searchService.findCoursesBy(searchTerm);


        // then
        Assertions.assertNotNull(findCourses);
        Assertions.assertEquals(result, findCourses);
    }

    private static Stream<Arguments> testPayloadProvider() {

        List<Course> androidCourses = new ArrayList<>();
        Vendor vendor1 = new Vendor("Android course provider", "Android Vendor Logo URL", "Android Vendor URL");
        Course androidCourseProvider1 = new Course("Best Android Course",
                vendor1, "Image URL", "Course URL", "headline1", "author");

        androidCourses.add(androidCourseProvider1);

        return Stream.of(


                // Basic case
                Arguments.of("", new ArrayList<Course>()),
                Arguments.of("android", androidCourses)
        );
    }
}
