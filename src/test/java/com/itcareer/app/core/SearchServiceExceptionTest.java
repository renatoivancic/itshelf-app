package com.itcareer.app.core;

import com.itcareer.app.core.search.SearchService;
import com.itcareer.app.domain.Course;
import com.itcareer.app.domain.Vendor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.ResourceAccessException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SearchServiceExceptionTest {

    @Mock
     CourseService courseService1;

    @Mock
     CourseService courseService2;

    List<CourseService> courseServices;

    @BeforeEach
    void before() {

        Vendor vendor1 = new Vendor("Android course provider", "Android Vendor Logo URL", "Android Vendor URL");
        Course androidCourseProvider1 = new Course("Best Android Course",
                vendor1, "Image URL", "Course URL", "headline1", "author");

        when(courseService1.findCoursesBy("android")).thenReturn(Stream.of(androidCourseProvider1));
        when(courseService2.findCoursesBy("android")).thenThrow(new ResourceAccessException("Connection could not be established"));
        courseServices = Arrays.asList(courseService1, courseService2);

    }

    /**
     * Even if one service throws exception others are aggregated and result is provided.
     * For the failed vendor results will be missing in the final result page.
     */
    @Test
    public void testHandledException() {

        // given
        SearchService searchService = new SearchService(courseServices);

        // when
        List<Course> findCourses = searchService.findCoursesBy("android");


        List<Course> androidCourses = new ArrayList<>();
        Vendor vendor1 = new Vendor("Android course provider", "Android Vendor Logo URL", "Android Vendor URL");
        Course androidCourseProvider1 = new Course("Best Android Course",
                vendor1, "Image URL", "Course URL", "headline1", "author");

        androidCourses.add(androidCourseProvider1);

        // then
        Assertions.assertNotNull(findCourses);
        Assertions.assertEquals(androidCourses, findCourses);
    }
}
