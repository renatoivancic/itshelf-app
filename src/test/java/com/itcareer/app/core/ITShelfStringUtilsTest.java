package com.itcareer.app.core;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public class ITShelfStringUtilsTest {

    @DisplayName("Should properly cut off sentence to specific length")
    @ParameterizedTest(name = "input text: {0}, mac length {1} Expected result: {2}")
    @MethodSource("testPayloadProvider")
    public void toShortStatementToCutTest(String input, int maxLength, String expectedOutput) {

        // when
        String ellipsizedText = ITShelfStringUtils.ellipsize(input, maxLength);

        // then
        Assertions.assertNotNull(ellipsizedText);
        Assertions.assertEquals(expectedOutput, ellipsizedText);
    }

    private static Stream<Arguments> testPayloadProvider() {

        return Stream.of(

                // Basic case
                Arguments.of("Short text that will not be trimmed", 150,"Short text that will not be trimmed"),
                Arguments.of("Pneumonoultramicroscopicsilicovolcanoconiosis", 20,"Pneumonoultramicr..."),
                Arguments.of("Text that will be trimmed withverylongwordattheend", 30,"Text that will be trimmed...")
        );
    }
}
