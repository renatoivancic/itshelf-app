package com.itcareer.app.analytics;


import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.system.CapturedOutput;
import org.springframework.boot.test.system.OutputCaptureExtension;

@ExtendWith(OutputCaptureExtension.class)
public class ScheduledMemoryLoggerTest {

    @Test
    public void testMemoryLogger(CapturedOutput output) {

        // given
        ScheduledMemoryLogger scheduledMemoryLogger = new ScheduledMemoryLogger();

        // when
        scheduledMemoryLogger.scheduledMemoryLogger();

        // then
        Assertions.assertThat(output).contains("Total memory");
        Assertions.assertThat(output).contains("Used memory calculation took");
    }
}