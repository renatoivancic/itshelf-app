package com.itcareer.app.analytics;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class AnalyticsServiceTest {

    /**
     * Test analytics service with mocked repository.
     */
    @Test
    public void testEventPersistence() throws InterruptedException, ExecutionException {

        // given
        SearchEventMockRepository searchEventMockRepository = new SearchEventMockRepository();
        AnalyticsService analyticsService = new AnalyticsService(searchEventMockRepository);
        CompletableFuture<SearchEvent> eventCallback = searchEventMockRepository.callback;

        // when
        analyticsService.persistSearchEvent("java");

        // then
        SearchEvent searchEvent = eventCallback.get();
        Optional<SearchEvent> byId = searchEventMockRepository.findById(1l);
        Assertions.assertNotNull(byId);
        Assertions.assertTrue(byId.isPresent());
        Assertions.assertEquals(1, byId.get().getId());
        Assertions.assertEquals("java", byId.get().getSearchTerm());
    }
}

class SearchEventMockRepository implements SearchEventRepository {

    SearchEvent searchEvent;
    CompletableFuture<SearchEvent> callback= new CompletableFuture<>();

    @Override
    public <S extends SearchEvent> S save(S entity) {
        this.searchEvent = entity;
        this.searchEvent.setId(1l);
        callback.complete(searchEvent);
        return null;
    }

    @Override
    public <S extends SearchEvent> Iterable<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<SearchEvent> findById(Long aLong) {
        if(searchEvent.getId() == aLong) {
            return Optional.of(searchEvent);
        }
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public Iterable<SearchEvent> findAll() {
        return null;
    }

    @Override
    public Iterable<SearchEvent> findAllById(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(SearchEvent entity) {

    }

    @Override
    public void deleteAllById(Iterable<? extends Long> longs) {

    }

    @Override
    public void deleteAll(Iterable<? extends SearchEvent> entities) {

    }

    @Override
    public void deleteAll() {

    }
}
