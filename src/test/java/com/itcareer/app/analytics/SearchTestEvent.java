package com.itcareer.app.analytics;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

public class SearchTestEvent {

    @Test
    public void testSearchEvent() {

        SearchEvent searchEvent = new SearchEvent("java");
        searchEvent.setSearchTerm("security");
        System.out.println(searchEvent.toString());
        searchEvent.setTimeStamp(LocalDateTime.parse("2020-05-19T14:09:44"));

        Assertions.assertTrue(searchEvent.toString().contains("SearchEvent{id=0, timestamp="));
        Assertions.assertTrue(searchEvent.toString().contains(", searchTerm='security'}"));
        Assertions.assertEquals("security", searchEvent.getSearchTerm());
        Assertions.assertEquals(0, searchEvent.getId());
        Assertions.assertNotNull(searchEvent.getTimeStamp());
        Assertions.assertEquals(LocalDateTime.parse("2020-05-19T14:09:44"), searchEvent.getTimeStamp());
    }
}
