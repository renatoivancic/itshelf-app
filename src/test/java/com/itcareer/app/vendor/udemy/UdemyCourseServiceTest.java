package com.itcareer.app.vendor.udemy;

import com.itcareer.app.domain.Course;
import com.itcareer.app.domain.Vendor;
import com.itcareer.app.vendor.coursera.CourseraCourseApiClient;
import com.itcareer.app.vendor.coursera.CourseraCourseService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class UdemyCourseServiceTest {

    @Mock
    UdemyCourseApiClient udemyCourseApiClient;

    @Test
    public void testCourseServiceToStringMethod( ) {

        // given
        UdemyCourseService udemyCourseService = new UdemyCourseService(null);

        // then
        Assertions.assertEquals("UdemyCourseService",udemyCourseService.toString());
    }

    @Test
    public void testGetAtLeastOneJavaCourse( ) {

        // given
        var searchTerm = "Java";
        Stream<Course> mockCourses = Stream.of(
                new Course(
                        "Java 11",
                        new Vendor("Udemy", "logoUrl", "vendor1Url"),
                        "imageUrl",
                        "course1Url",
                        "headline1",
                        "author1"),
                new Course(
                        "Java 14",
                        new Vendor("Udemy", "logoUrl", "vendor2Url"),
                        "imageUrl",
                        "course2Url",
                        "headline2",
                        "author2")
        );
        Mockito.when(udemyCourseApiClient.findCoursesBy(any())).thenReturn(mockCourses);
        UdemyCourseService udemyCourseService = new UdemyCourseService(udemyCourseApiClient);

        // when
        Stream<Course> courses = udemyCourseService.findCoursesBy(searchTerm);
        List<Course> coursesList = courses.collect(Collectors.toList());

        // then
        Assertions.assertNotNull(coursesList);
        Assertions.assertEquals(2, coursesList.size());
        Assertions.assertEquals(new Course(
                "Java 11",
                new Vendor("Udemy", "logoUrl", "vendor1Url"),
                "imageUrl",
                "course1Url",
                "headline1",
                "author1"), coursesList.get(0));
        Assertions.assertEquals(new Course(
                "Java 14",
                new Vendor("Udemy", "logoUrl", "vendor2Url"),
                "imageUrl",
                "course2Url",
                "headline2",
                "author2"), coursesList.get(1));
    }
}
