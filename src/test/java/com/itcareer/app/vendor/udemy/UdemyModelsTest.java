package com.itcareer.app.vendor.udemy;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UdemyModelsTest {

    @Test
    public void testUdemySearchResponse( ) {

        // given
        List<Results> results = new ArrayList<>();
        Results result = new Results(
                11,
                "Title of the course",
                "url path to medium image",
                "course url",
                "headline1",
                Collections.singletonList(new VisibleInstructors("Max Mustermann")), new Category(288));
        results.add(result);
        UdemyCoursesSearchResponse udemyCoursesSearchResponse = new UdemyCoursesSearchResponse(10, results);

        // then
        Assertions.assertNotNull(udemyCoursesSearchResponse);
        Assertions.assertEquals(10,   udemyCoursesSearchResponse.getCount());
        Assertions.assertEquals(1,   udemyCoursesSearchResponse.getResults().size());
        Assertions.assertEquals(11,   udemyCoursesSearchResponse.getResults().get(0).getId());
        Assertions.assertEquals("Title of the course",   udemyCoursesSearchResponse.getResults().get(0).getTitle());
        Assertions.assertEquals("url path to medium image",   udemyCoursesSearchResponse.getResults().get(0).getImageMedium());
        Assertions.assertEquals("course url",   udemyCoursesSearchResponse.getResults().get(0).getUrl());
        Assertions.assertEquals("headline1",   udemyCoursesSearchResponse.getResults().get(0).getHeadline());
        Assertions.assertEquals("Max Mustermann",   udemyCoursesSearchResponse.getResults().get(0).getInstructorsAsText());
        Assertions.assertEquals(288,   udemyCoursesSearchResponse.getResults().get(0).getCategory().getId());
    }
}
