package com.itcareer.app.vendor.udemy;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.itcareer.app.domain.Course;
import com.itcareer.app.domain.Vendor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class UdemyApiClientTest {

    WireMockServer wireMockServer;

    @BeforeEach
    public void setup() {
        wireMockServer = new WireMockServer(8090);
        wireMockServer.start();
        setupStub();
    }

    public void setupStub() {

        wireMockServer.stubFor(get(urlEqualTo("/api-2.0/courses/?page=1&page_size=3&search=java&fields%5Bcourse%5D=@default,primary_category"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBodyFile("json/udemy-java.json")));
    }

    @AfterEach
    public void teardown() {
        wireMockServer.stop();
    }

    /**
     * Test second result form Udemy static wiremock response.
     */
    @Test
    public void testUdemySearchResponse() {

        // given
        final RestTemplate restTemplate = new RestTemplate();
        UdemyCourseApiClient udemyCourseApiClient = new UdemyCourseApiClient(restTemplate);
        ReflectionTestUtils.setField(udemyCourseApiClient, "udemyApiUrl", "http://localhost:8090/api-2.0");
        ReflectionTestUtils.setField(udemyCourseApiClient, "udemyApiClientId", "XXX");
        ReflectionTestUtils.setField(udemyCourseApiClient, "udemyApiClientSecret", "XXX");
        ReflectionTestUtils.setField(udemyCourseApiClient, "udemyApiClientQueryLimit", "3");

        // when
        Stream<Course> results = udemyCourseApiClient.findCoursesBy("java");

        // then
        List<Course> resultsInList = results.collect(Collectors.toList());
        Assertions.assertNotNull(resultsInList);
        System.out.println(resultsInList);
        Assertions.assertEquals(5, resultsInList.size());
        Course course = new Course(
                "Selenium WebDriver with Java -Basics to Advanced+Frameworks",
                new Vendor("Udemy", "/img/vendors/udemy/udemy-logo.png", "https://www.udemy.com/"),
                "https://img-a.udemycdn.com/course/240x135/354176_fe73_5.jpg",
                "https://www.udemy.com/course/selenium-real-time-examplesinterview-questions/",
                "\"TOP RATED (BEST SELLER) #1 Master SELENIUM java course\" -5 Million students learning worldWide with great collaboration",
                "Rahul Shetty");
        Assertions.assertEquals(course, resultsInList.get(1));
    }
}
