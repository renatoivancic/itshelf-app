package com.itcareer.app.vendor.microsoft;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class MicrosoftStaticFilePojoTest {

    @Test
    public void testMicrosoftStaticFilePojoClass() {

        // given
        List<Module> modules = new ArrayList<>();
        Module module = new Module("Summary", "uid", "type", "title", 10, "iconUrl","de","10-12-2019","url");
        modules.add(module);
        MicrosoftStaticFilePojo microsoftStaticFilePojo = new MicrosoftStaticFilePojo(modules);

        // when

        // then
        Assertions.assertNotNull(microsoftStaticFilePojo);
        Assertions.assertNotNull(microsoftStaticFilePojo.getModules());
        Assertions.assertEquals(1, microsoftStaticFilePojo.getModules().size());
        Assertions.assertEquals("MicrosoftStaticFilePojo{modules=[Module{summary='Summary', uid='uid', type='type', title='title', durationInMinutes=10, iconUrl='iconUrl', locale='de', lastModified='10-12-2019', url='url'}]}", microsoftStaticFilePojo.toString());

        Assertions.assertEquals("title", microsoftStaticFilePojo.getModules().get(0).getTitle());
        Assertions.assertEquals("Summary", microsoftStaticFilePojo.getModules().get(0).getSummary());
        Assertions.assertEquals("uid", microsoftStaticFilePojo.getModules().get(0).getUid());
        Assertions.assertEquals("type", microsoftStaticFilePojo.getModules().get(0).getType());
        Assertions.assertEquals(10, microsoftStaticFilePojo.getModules().get(0).getDurationInMinutes());
        Assertions.assertEquals("iconUrl", microsoftStaticFilePojo.getModules().get(0).getIconUrl());
        Assertions.assertEquals("de", microsoftStaticFilePojo.getModules().get(0).getLocale());
        Assertions.assertEquals("10-12-2019", microsoftStaticFilePojo.getModules().get(0).getLastModified());
        Assertions.assertEquals("url", microsoftStaticFilePojo.getModules().get(0).getUrl());

        Assertions.assertEquals("Module{summary='Summary', uid='uid', type='type', title='title', durationInMinutes=10, iconUrl='iconUrl', locale='de', lastModified='10-12-2019', url='url'}", microsoftStaticFilePojo.getModules().get(0).toString());


    }
}
