package com.itcareer.app.vendor.microsoft;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MicrosoftCourseServiceTest {

    @Test
    public void parseMicrosoftStaticCoursesFileWithNullTest() {

        // given
        MicrosoftCourseService microsoftCourseService = new MicrosoftCourseService(null);

        // then
        Assertions.assertNotNull(microsoftCourseService);
        Assertions.assertEquals("MicrosoftCourseService", microsoftCourseService.toString());
    }
}
