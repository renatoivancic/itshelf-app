package com.itcareer.app.vendor.microsoft;

import com.itcareer.app.TestHelper;
import com.itcareer.app.domain.Course;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.DefaultResourceLoader;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MicrosoftCourseParserTest {

    @Test
    public void parseMicrosoftStaticCoursesFileWithNullTest() {

        // given
        MicrosoftCourseService microsoftCourseService = new MicrosoftCourseService(new MicrosoftCourseParser(new DefaultResourceLoader()));

        // when
        List<Course> microsoftCourses = microsoftCourseService.findCoursesBy(null).collect(Collectors.toList());

        // then
        Assertions.assertNotNull(microsoftCourses);
        Assertions.assertEquals(0, microsoftCourses.size());
    }

    @Test
    public void parseMicrosoftStaticCoursesFileWithEmptyTest() {

        // given
        MicrosoftCourseService microsoftCourseService = new MicrosoftCourseService(new MicrosoftCourseParser(new DefaultResourceLoader()));

        // when
        List<Course> microsoftCourses = microsoftCourseService.findCoursesBy("").collect(Collectors.toList());

        // then
        Assertions.assertNotNull(microsoftCourses);
        Assertions.assertEquals(0, microsoftCourses.size());
    }

    @Test
    public void parseMicrosoftStaticCoursesFileWithAzureTest() {

        // given
        MicrosoftCourseService microsoftCourseService = new MicrosoftCourseService(new MicrosoftCourseParser(new DefaultResourceLoader()));

        // when
        List<Course> microsoftCourses = microsoftCourseService.findCoursesBy("Azure").collect(Collectors.toList());

        // then
        Assertions.assertNotNull(microsoftCourses);
        Assertions.assertEquals(314, microsoftCourses.size());
        // Test course at index 109
        Assertions.assertEquals("Create a multistage pipeline by using Azure Pipelines", microsoftCourses.get(109).getName());
        Assertions.assertEquals("https://docs.microsoft.com/en-us/learn/modules/create-multi-stage-pipeline/?WT.mc_id=api_CatalogApi", microsoftCourses.get(109).getUrl());
        Assertions.assertEquals("Design and create a realistic release pipeline that promotes changes to various testing and staging environments.", microsoftCourses.get(109).getHeadline());
        Assertions.assertEquals("https://docs.microsoft.com/en-us/learn/achievements/azure-devops/create-multi-stage-pipeline.svg", microsoftCourses.get(109).getImageUrl());
        Assertions.assertNull(microsoftCourses.get(109).getAuthor());
        Assertions.assertEquals("Microsoft Learn", microsoftCourses.get(109).getAuthorWithVendor());
        Assertions.assertEquals("Microsoft Learn", microsoftCourses.get(109).getDisplayAuthor());
        Assertions.assertEquals("Microsoft Learn", microsoftCourses.get(109).getVendor().getName());
        Assertions.assertEquals("https://docs.microsoft.com/en-us/learn/", microsoftCourses.get(109).getVendor().getUrl());
        Assertions.assertEquals("/img/vendors/microsoft/microsoft-logo.png", microsoftCourses.get(109).getVendor().getLogoUrl());
    }

    @Test
    public void parseMicrosoftStaticCoursesFileWithCSharpTest() {

        // given
        MicrosoftCourseService microsoftCourseService = new MicrosoftCourseService(new MicrosoftCourseParser(new DefaultResourceLoader()));

        // when
        List<Course> microsoftCourses = microsoftCourseService.findCoursesBy("csharp").collect(Collectors.toList());

        // then
        Assertions.assertNotNull(microsoftCourses);
        Assertions.assertEquals(18, microsoftCourses.size());
        // Test course at index 109
        Assertions.assertEquals("Store and iterate through sequences of data using Arrays and the foreach statement in C#", microsoftCourses.get(11).getName());
        Assertions.assertEquals("https://docs.microsoft.com/en-us/learn/modules/csharp-arrays/?WT.mc_id=api_CatalogApi", microsoftCourses.get(11).getUrl());
        Assertions.assertEquals("Work with sequences of related data in data structures known as arrays. Then, learn to iterate through each item in the sequence.", microsoftCourses.get(11).getHeadline());
        Assertions.assertEquals("https://docs.microsoft.com/en-us/learn/achievements/csharp-arrays.svg", microsoftCourses.get(11).getImageUrl());
        Assertions.assertNull(microsoftCourses.get(11).getAuthor());
        Assertions.assertEquals("Microsoft Learn", microsoftCourses.get(11).getAuthorWithVendor());
        Assertions.assertEquals("Microsoft Learn", microsoftCourses.get(11).getDisplayAuthor());
        Assertions.assertEquals("Microsoft Learn", microsoftCourses.get(11).getVendor().getName());
        Assertions.assertEquals("https://docs.microsoft.com/en-us/learn/", microsoftCourses.get(11).getVendor().getUrl());
        Assertions.assertEquals("/img/vendors/microsoft/microsoft-logo.png", microsoftCourses.get(11).getVendor().getLogoUrl());
    }

    @Test()
    public void parseMicrosoftStaticCoursesFileFileDoesNotExistTest() throws ReflectiveOperationException {

        Exception exception = assertThrows(MicrosoftCoursesInitializationException.class, () -> {
            TestHelper.setFinalStaticField(
                    MicrosoftCourseParser.class,
                    "microsoftCoursesFilePath",
                    "classpath:vendors/microsoft/microsoft-courses-wrong-file.json"
            );
            MicrosoftCourseParser microsoftCourseParser = new MicrosoftCourseParser(new DefaultResourceLoader());
        });

        assertNotNull(exception);

        // Revert changes
        TestHelper.setFinalStaticField(
                MicrosoftCourseParser.class,
                "microsoftCoursesFilePath",
                "classpath:vendors/microsoft/microsoft-courses.json");
    }
}
