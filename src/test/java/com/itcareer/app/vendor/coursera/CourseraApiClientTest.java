package com.itcareer.app.vendor.coursera;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.itcareer.app.domain.Course;
import com.itcareer.app.domain.Vendor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

/**
 * 3rd object form the test json file, presenting the response if one would search for security form Coursera API
 *
 *   {
 *       "courseType": "v2.ondemand",
 *       "domainTypes": [
 *         {
 *           "subdomainId": "security",
 *           "domainId": "information-technology"
 *         },
 *         {
 *           "domainId": "computer-science",
 *           "subdomainId": "software-development"
 *         }
 *       ],
 *       "photoUrl": "https://d3njjcbhbojbot.cloudfront.net/api/utilities/v1/imageproxy/https://coursera-course-photos.s3.amazonaws.com/4b/834f1caad14fa698f4be488b3fa65d/504473474.504473476.jpg",
 *       "id": "BrizCVfcEemQ5QpVooi_CA",
 *       "slug": "cybersecurity-roles-processes-operating-system-security",
 *       "name": "Cybersecurity Roles, Processes & Operating System Security"
 *     },
 */
public class CourseraApiClientTest {

    WireMockServer wireMockServer;

    CourseraCourseConverter courseraCourseConverter =
            new CourseraCourseConverter(new CourseraPartnerService(new DefaultResourceLoader()));

    @BeforeEach
    public void setup() {
        wireMockServer = new WireMockServer(8090);
        wireMockServer.start();
        setupStub();
    }

    public void setupStub() {
        wireMockServer.stubFor(get(urlEqualTo("/api/courses.v1?q=search&query=security&fields=partnerIds,description,domainTypes,photoUrl&limit=100"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBodyFile("json/coursera-java.json")));
    }

    @AfterEach
    public void teardown() {
        wireMockServer.stop();
    }

    @Test
    public void testCourseraSearchResponse() {

        // given
        final RestTemplate restTemplate = new RestTemplate();
        CourseraCourseApiClient courseraCourseApiClient = new CourseraCourseApiClient(restTemplate,courseraCourseConverter);
        ReflectionTestUtils.setField(courseraCourseApiClient, "courseraApiUrl", "http://localhost:8090/api/courses.v1");
        ReflectionTestUtils.setField(courseraCourseApiClient, "courseraApiQueryLimit", "100");

        // when
        Stream<Course> results = courseraCourseApiClient.findCoursesBy("security");

        // then
        List<Course> resultsInList = results.collect(Collectors.toList());
        Assertions.assertNotNull(resultsInList);
        System.out.println(resultsInList);
        Assertions.assertEquals(4, resultsInList.size());
        Course course = new Course(
                "Cybersecurity Roles, Processes & Operating System Security",
                new Vendor("Coursera", "/img/vendors/coursera/coursera-logo.png", "https://www.coursera.org/"),
                "https://d3njjcbhbojbot.cloudfront.net/api/utilities/v1/imageproxy/https://coursera-course-photos.s3.amazonaws.com/4b/834f1caad14fa698f4be488b3fa65d/504473474.504473476.jpg",
                "https://www.coursera.org/learn/cybersecurity-roles-processes-operating-system-security",
                "This course covers a wide variety of IT security concepts, tools, and best practices. It introduces threats and attacks and the many ways they can show...",
                "IBM");
        Assertions.assertEquals(course, resultsInList.get(2));
    }
}
