package com.itcareer.app.vendor.coursera;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class CourseraCourseResponseTest {

    @Test
    public void testEmptyCourseraCourseServiceToStringMethod() {

        // given
        CourseraCoursesSearchResponse courseraCoursesSearchResponse = new CourseraCoursesSearchResponse();
        List<Element> elements = new ArrayList<>();
        courseraCoursesSearchResponse.setElements(elements);

        // then
        Assertions.assertEquals("CourseraCoursesSearchResponse{elements=[]}", courseraCoursesSearchResponse.toString());
    }

    @Test
    public void testCourseraCourseServiceToStringMethod() {

        // given
        CourseraCoursesSearchResponse courseraCoursesSearchResponse = new CourseraCoursesSearchResponse();
        List<Element> elements = new ArrayList<>();
        Element element = new Element();
        element.setName("Coursera course");
        element.setId("ID");
        element.setCourseType("Course");
        element.setPhotoUrl("Photo URL");
        element.setDescription("Description");
        element.setPartnerIds(Arrays.asList("1", "2"));
        element.setSlug("slug");
        DomainType domainType = new DomainType();
        domainType.setDomainId("domainId");
        domainType.setSubdomainId("subdomainId");
        element.setDomainTypes(Set.of(domainType));
        elements.add(element);

        courseraCoursesSearchResponse.setElements(elements);

        // then
        Assertions.assertEquals("CourseraCoursesSearchResponse{elements=[Element{courseType='Course', id='ID', slug='slug', name='Coursera course', domainTypes=[DomainType{domainId='domainId', subdomainId='subdomainId'}], photoUrl='Photo URL', description='Description', partnerIds=[1, 2]}]}", courseraCoursesSearchResponse.toString());
    }
}
