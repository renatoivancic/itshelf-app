package com.itcareer.app.vendor.coursera;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.DefaultResourceLoader;

public class CourseraPartnerServiceTest {

    @Test
    public void testParsingPartnerSource() {

        // given
        CourseraPartnerService courseraPartnerService = new CourseraPartnerService(new DefaultResourceLoader());

        // when
        String partnerDisplayName = courseraPartnerService.getPartnerDisplayName("93");

        // then
        Assertions.assertNotNull(partnerDisplayName);
        Assertions.assertEquals("University of Zurich", partnerDisplayName);
    }

    @Test
    public void testParsingResourceWithoutMatch() {

        // given
        CourseraPartnerService courseraPartnerService = new CourseraPartnerService(new DefaultResourceLoader());

        // when
        String partnerDisplayName = courseraPartnerService.getPartnerDisplayName("xx");

        // then
        Assertions.assertNull(partnerDisplayName);
    }
}
