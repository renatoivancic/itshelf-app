package com.itcareer.app.vendor.coursera;

import com.itcareer.app.TestHelper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.DefaultResourceLoader;

import static org.junit.jupiter.api.Assertions.*;

public class CourseraPartnerServiceCantLoadTest {

    @Test
    public void exceptionIsThrownIfFileCanNotBeRead() {

        Exception exception = assertThrows(CourseraPartnerInitializationException.class, () -> {
            TestHelper.setFinalStaticField(
                    CourseraPartnerService.class,
                    "courseraPartnersFile",
                     "classpath:vendors/coursera/coursera-wrong-file.json"
            );
            CourseraPartnerService courseraPartnerService = new CourseraPartnerService(new DefaultResourceLoader());
        });

        assertNotNull(exception);
    }

    @AfterAll
    static void revertChanges() throws ReflectiveOperationException {
        TestHelper.setFinalStaticField(
                CourseraPartnerService.class,
                "courseraPartnersFile",
                "classpath:vendors/coursera/coursera-partners.json");
    }
}
