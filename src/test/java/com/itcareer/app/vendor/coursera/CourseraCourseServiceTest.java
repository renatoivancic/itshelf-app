package com.itcareer.app.vendor.coursera;

import com.itcareer.app.domain.Course;
import com.itcareer.app.domain.Vendor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class CourseraCourseServiceTest {

    @Mock
    CourseraCourseApiClient courseraCourseApiClient;

    @Test
    public void testCourseraCourseServiceToStringMethod() {

        // given
        CourseraCourseService courseraCourseService = new CourseraCourseService(null);

        // then
        Assertions.assertEquals("CourseraCourseService", courseraCourseService.toString());
    }

    @Test
    public void testGetAtLeastOneJavaCourse( ) {

        // given
        var searchTerm = "Java";
        Stream<Course> mockCourses = Stream.of(
                new Course(
                        "Java 11",
                        new Vendor("Coursera", "logoUrl", "vendor1Url"),
                        "imageUrl",
                        "course1Url",
                        "headline1",
                        "author1"),
                new Course(
                        "Java 14",
                        new Vendor("Coursera", "logoUrl", "vendor2Url"),
                        "imageUrl",
                        "course2Url",
                        "headline2",
                        "author2")
        );
        Mockito.when(courseraCourseApiClient.findCoursesBy(any())).thenReturn(mockCourses);
        CourseraCourseService courseraCourseService = new CourseraCourseService(courseraCourseApiClient);

        // when
        Stream<Course> courses = courseraCourseService.findCoursesBy(searchTerm);
        List<Course> coursesList = courses.collect(Collectors.toList());

        // then
        Assertions.assertNotNull(coursesList);
        Assertions.assertEquals(2, coursesList.size());
        Assertions.assertEquals(new Course(
                "Java 11",
                new Vendor("Coursera", "logoUrl", "vendor1Url"),
                "imageUrl",
                "course1Url",
                "headline1",
                "author1"), coursesList.get(0));
        Assertions.assertEquals(new Course(
                "Java 14",
                new Vendor("Coursera", "logoUrl", "vendor2Url"),
                "imageUrl",
                "course2Url",
                "headline2",
                "author2"), coursesList.get(1));
    }
}
