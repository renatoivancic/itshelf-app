package com.itcareer.app.vendor.coursera;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itcareer.app.domain.Course;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.DefaultResourceLoader;

import java.io.File;
import java.io.IOException;
import java.util.stream.Stream;

/**
 * JMH performance testing framework in action.
 *
 * There are two tests in a class.
 *
 * 1. Concatenation of strings in an old way.
 *
 * 2. Concatenation of strings with a StringBuilder
 *
 * JMH Parameters are specified as annotations on the test class.
 */
public class CourseraCourseConverterPerformanceTest {

    static CourseraCoursesSearchResponse courseraCoursesSearchResponse;
    private final CourseraCourseConverter courseraCourseConverter =
            new CourseraCourseConverter(new CourseraPartnerService(new DefaultResourceLoader()));

    @BeforeAll
    public static void init() throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        courseraCoursesSearchResponse = objectMapper.readValue(
                new File("src/test/resources/coursera-java-test-file.json"),
                CourseraCoursesSearchResponse.class);
    }

    @Test
    public void sequentialProcess() {
        System.out.println("Number of courses sequential: " + courseraCoursesSearchResponse.getElements().size());
        Stream<Course> courseStream = null;
        for(int i = 0; i< 10; i++) {
            long start = System.nanoTime();
            courseStream = courseraCourseConverter.extractCourses(courseraCoursesSearchResponse);
            long end = System.nanoTime();
            System.out.println("Time sequential: " + (end - start));
        }
        Assertions.assertNotNull(courseStream);
    }

    @Test
    public void parallelProcess() {
        System.out.println("Number of courses parallel: " + courseraCoursesSearchResponse.getElements().size());
        Stream<Course> courseStream = null;
        for(int i = 0; i< 10; i++){
            long start = System.nanoTime();
            courseStream = courseraCourseConverter.extractCoursesInParallel(courseraCoursesSearchResponse);
            long end = System.nanoTime();
            System.out.println("Time parallel: " + (end - start));
        }
        Assertions.assertNotNull(courseStream);
    }
}
