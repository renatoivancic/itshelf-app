package com.itcareer.app.vendor.itshelf;

import com.itcareer.app.domain.Course;
import com.itcareer.app.domain.Vendor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class ItShelfCourseServiceTest {

    @Mock
    ItShelfCourseApiClient itCareerCourseApiService;

    @Test
    public void testGetAtLeastOneJavaCourse( ) {

        // given
        var searchTerm = "Java";
        Stream<Course> mockCourses = Stream.of(
                new Course(
                        "Java 11",
                        new Vendor("ItCareer", "logoUrl", "vendor1Url"),
                        "imageUrl",
                        "course1Url",
                        "headline1",
                        "Renato Ivancic"),
                new Course(
                        "Java 14",
                        new Vendor("ItCareer", "logoUrl", "vendor2Url"),
                        "imageUrl",
                        "course2Url",
                        "headline2",
                        "Janez Novak")
        );
        Mockito.when(itCareerCourseApiService.findCoursesBy(any())).thenReturn(mockCourses);
        ItShelfCourseService itShelfCourseService = new ItShelfCourseService(itCareerCourseApiService);

        // when
        Stream<Course> courses = itShelfCourseService.findCoursesBy(searchTerm);
        List<Course> coursesList = courses.collect(Collectors.toList());

        // then
        Assertions.assertNotNull(coursesList);
        Assertions.assertEquals(2, coursesList.size());
        Assertions.assertEquals(new Course(
                "Java 11",
                new Vendor("ItCareer", "logoUrl", "vendor1Url"),
                "imageUrl",
                "course1Url",
                "headline1",
                "Renato Ivancic"), coursesList.get(0));
        Assertions.assertEquals(new Course(
                "Java 14",
                new Vendor("ItCareer", "logoUrl", "vendor2Url"),
                "imageUrl",
                "course2Url",
                "headline2",
                "Janez Novak"), coursesList.get(1));
    }
}
