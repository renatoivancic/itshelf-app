package com.itcareer.app.vendor.itshelf;

import com.itcareer.app.domain.Course;
import com.itcareer.app.domain.Vendor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ItShelfApiClientTest {

    @Test
    public void testItShelfApiClientStaticCourses( ) {

        // given
        ItShelfCourseApiClient itShelfCourseApiClient = new ItShelfCourseApiClient();

        // when
        List<Course> courses = itShelfCourseApiClient.findCoursesBy("whatever").collect(Collectors.toList());

        // then
        Assertions.assertNotNull(courses);
        Assertions.assertEquals(3, courses.size());
        Assertions.assertEquals("whatever Java 11", courses.get(0).getName());
        Assertions.assertEquals("http://www.itshelf.com/course/java11", courses.get(0).getUrl());
        Assertions.assertEquals("/img/logo/itshelf-logo.png", courses.get(0).getImageUrl());
        Assertions.assertEquals(new Vendor("ItShelf", "/img/logo/itshelf-logo.png", "http://www.itshelf.com"), courses.get(0).getVendor());
        Assertions.assertEquals(Objects.hash("ItShelf", "/img/logo/itshelf-logo.png", "http://www.itshelf.com"), courses.get(0).getVendor().hashCode());
        Assertions.assertEquals(Objects.hash("whatever Java 11",
                Objects.hash("ItShelf", "/img/logo/itshelf-logo.png", "http://www.itshelf.com"),
                "/img/logo/itshelf-logo.png", "http://www.itshelf.com/course/java11", "Java 11 OCP certificate", "Renato Ivancic"), courses.get(0).hashCode());
    }
}
